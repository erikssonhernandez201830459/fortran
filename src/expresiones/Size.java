package expresiones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Size extends Instruccion {
    String id;

    NodoAST nodoPadre;

    public Size(String id, int line, int column) {
        super(null, line, column);
        this.id = id;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol variable = table.getVariable(this.id);
        // Verificamos que la variable exista en la tabla
        if (variable == null) {
            String descError = "La variable ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        //Verificamos que la variable sea del tipo arreglo
        if(       variable.tipo2.tipo == Tipo.Type.ARREGLO
                ||variable.tipo2.tipo == Tipo.Type.ARREGLO2
                ||variable.tipo2.tipo == Tipo.Type.ALLOCATE
                ||variable.tipo2.tipo == Tipo.Type.ALLOCATE2
        ){
            if(variable.valor instanceof ArrayList<?> listaI) {
                if(listaI.size() > 0) {
                    if (listaI.get(0) instanceof ArrayList<?> listaJ) {
                        //Le asignamos el tipo a esta clase
                        this.type = Tipo.Type.INTEGER;

                        //Guardamos el tamanio del arreglo
                        int retorno = listaI.size() * listaJ.size();

                        //Creamos el nodo con el valor para el AST
                        nodoPadre = new NodoAST("*");
                        nodoPadre.agregarHijo(new NodoAST(String.valueOf(listaI.size())));
                        nodoPadre.agregarHijo(new NodoAST(String.valueOf(listaJ.size())));

                        //Retorno el valor de la lista
                        return retorno;
                    }
                }
                //Le asignamos el tipo a esta clase
                this.type = Tipo.Type.INTEGER;

                //Creamos el nodo con el valor para el AST
                nodoPadre = new NodoAST(String.valueOf(listaI.size()));

                //Retornamos el tamanio de la lista
                return listaI.size();
            }
        }else{
            String descError = "La variable ["+this.id+"] no es del tipo Array ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        return null;
    }


    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("SIZE");
        nodo.agregarHijo(new NodoAST(this.id));
        if(nodoPadre != null){
            nodo.agregarHijo(nodoPadre);
        }
        return nodo;
    }
}
