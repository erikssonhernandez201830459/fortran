package expresiones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Identificador extends Instruccion {
    public String id;
    //public Object valor;
    // PARA EL ARBOL AST
    NodoAST nodoPadre;
    public Identificador(String id, int line, int column) {
        super(null, line, column);
        this.id = id;
    }
    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        // Obtenemos la variable desde la Tabla de Simbolos
        Symbol variable = table.getVariable(this.id);

        if (variable == null) {
            String descError = "La variable ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }


        /**
         *Verificamos que la variable sea de tipo VARIABLE
         */
        if(variable.tipo2.tipo == Tipo.Type.VARIABLE){
            //Verificamos si posee otra variable dentro
            if (variable.valor instanceof Identificador) {
                variable.valor = ((Identificador)variable.valor).execute(table, tree);
            }
            // Asignamos tipo y valor

            java.lang.Object result = variable.valor;
            this.type = variable.tipo.tipo;
            //Creacion del nodo AST
            nodoPadre = new NodoAST("ID");
            nodoPadre.agregarHijo(this.id);
            nodoPadre.agregarHijo(String.valueOf(result));

            //Devuelvo el valor resultante
            return result;

            /**
             * VALIDACIONES PARA UN ARREGLO DE UNA DIMENSION
             */
        }else if (variable.tipo2.tipo == Tipo.Type.ARREGLO || variable.tipo2.tipo == Tipo.Type.ALLOCATE) {
            //Obtenemos el arreglo de la lista de valores
            ArrayList<Instruccion> vieja = (ArrayList<Instruccion>) variable.valor;
            // Creamos una nueva lista para copiar los valores
            ArrayList<Instruccion> nueva = new ArrayList<>();

            //Creamos el String para el AST
            String valorNodo = "[";
            for(int i = 0; i < vieja.size(); i++){
                Instruccion nodo = vieja.get(i);
                java.lang.Object val = nodo.execute(table, tree);

                if (val instanceof Excepcion e) {
                    tree.consola.add(e.toString());
                    return val;
                }

                //Creamos el nuevo primitivo
                Primitivo prim =  new Primitivo(nodo.type, val, nodo.line, nodo.column);
                nueva.add(prim);

                valorNodo += val + ", ";
            }
            valorNodo += "]";


            //Le asignamos el tipo a esta clase
            this.type = variable.tipo.tipo;

            //Creamos el nodo para el AST
            nodoPadre = new NodoAST("ID");
            nodoPadre.agregarHijo(new NodoAST(this.id));
            nodoPadre.agregarHijo(new NodoAST(valorNodo));

            //Devolvemos la lista de valores
            return nueva;

            /**
             * VALIDACIONES PARA UN ARREGLO DE 2 DIMENSIONES
             */
        }else if (variable.tipo2.tipo == Tipo.Type.ARREGLO2 || variable.tipo2.tipo == Tipo.Type.ALLOCATE2) {
            // Obtenemos la lista de Listas de Variables
            ArrayList<ArrayList<Instruccion>> viejaI = (ArrayList<ArrayList<Instruccion>>) variable.valor;
            //Creamos una nueva lista de Listas
            ArrayList<ArrayList<Instruccion>> nuevaInstruccion = new ArrayList<>();


            String valorNodo = "[";
            for(int i = 0; i<viejaI.size(); i++){
                //Obtenemos la lista de valores
                ArrayList<Instruccion> viejaJ = viejaI.get(i);

                //Creamos la nueva lista de valores
                ArrayList<Instruccion> nuevaJ = new ArrayList<>();

                valorNodo += "[";
                for(int j = 0; j<viejaJ.size(); j++) {
                    Instruccion no = viejaJ.get(j);
                    java.lang.Object val = no.execute(table, tree);

                    if (val instanceof Excepcion e) {
                        tree.consola.add(e.toString());
                        return val;
                    }

                    //Creo el nuevo nodo Primitivo
                    Primitivo prim = new Primitivo(no.type, val, no.line, no.column);
                    nuevaJ.add(prim);

                    valorNodo += val + ", ";
                }
                valorNodo += "]";
                nuevaInstruccion.add(nuevaJ);
            }
            valorNodo += "]";

            //Le asignamos el tipo a esta clase
            this.type = variable.tipo.tipo;

            //Creo el nodo para el AST
            nodoPadre = new NodoAST("ID");
            nodoPadre.agregarHijo(new NodoAST(this.id));
            nodoPadre.agregarHijo(new NodoAST(valorNodo));

            //Devuelvo la lista de valores
            return nuevaInstruccion;
        } else{
            String descError = "La variable que busca no es de los tipos admitidos \n";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        Symbol variable = controlador.actual.getVariable(this.id);
        String temp1 = controlador.getTemps();
        String temp2 = controlador.getTemps();
        //Actualizacion de posicion
        Quadrupla quadPosition = new Quadrupla("+","P", String.valueOf(variable.posicion), temp1);
        controlador.addQuad(quadPosition);
        //Asignacion de variable en el stack
        Quadrupla quadAssign = new Quadrupla("ASSIGN", controlador.tree.STACK + "[(int)"+temp1+"]", "", temp2);
        controlador.addQuad(quadAssign);
        return quadAssign;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("VARIABLE");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
