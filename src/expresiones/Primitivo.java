package expresiones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.SymbolTable;
import entorno.Tipo;
import entorno.Tree;

public class Primitivo extends Instruccion {
    Object valor;

    public Primitivo(Tipo.Type type, Object valor, int line, int column) {
        super(type, line, column);
        this.valor = valor;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        return this.valor;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        String value = this.valor.toString();
        //  String value = this.valor}` === "true" ? "1" : `${this.valor}` === "false" ? "0" : `${this.valor}`;
        if(this.valor.equals(true)) {
           //True se reemplaza por 1
           value = "1";
        } else if(this.valor.equals(false)){
            //False se reemplaza por 0
           value = "0";
       }

        //AL SER UN VALOR PRIMITIVO, NO NECESITAMOS GUARDAR TEMP, PORQUE SE RETORNA EL VALOR
        return new Quadrupla("op","arg1","arg2",value);
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo;
        if(Tipo.Type.STRING == this.type){
            String val = String.valueOf(this.valor).replace("\"", "");
            nodo = new NodoAST(val);
        }else{
            nodo = new NodoAST(String.valueOf(this.valor));
        }
        return nodo;
    }
    @Override
    public String toString(){
        return valor.toString();
    }
}
