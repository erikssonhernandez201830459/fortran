package expresiones;

import Abstract.Instruccion;

import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;


public class Logica extends Instruccion {
    Instruccion operadorIzq;
    Instruccion operadorDer;
    String operador;

    //Para el AST
    NodoAST nodoOp;
    NodoAST nodoIzq;
    NodoAST nodoDer;
    public Logica(Instruccion operadorIzq, Instruccion operadorDer, String operador, int line, int column) {
        super(null, line, column);
        this.operadorIzq = operadorIzq;
        this.operadorDer = operadorDer;
        this.operador = operador;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        if(this.operadorDer == null){
            String descError = "No existe el operador derecho";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        if (this.operadorIzq != null) {

            /**
             * VERIFICACION DE EXCEPCIONES
             */
            Object resultadoIzq = this.operadorIzq.execute(table, tree);
            if (resultadoIzq instanceof Excepcion) {
                return resultadoIzq;
            }

            java.lang.Object resultadoDerecho = this.operadorDer.execute(table, tree);
            if (resultadoDerecho instanceof Excepcion) {
                return resultadoDerecho;
            }
            /**
             * OPERACION LOGICA AND
             */
            if (this.operador.equalsIgnoreCase(".and.")) {
                String descError = "No se pueden operar logicamente los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                // LOGICAL AND LOGICAL = LOGICAL
                if(this.operadorIzq.type == Tipo.Type.LOGICAL && this.operadorDer.type == Tipo.Type.LOGICAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos para el AST
                    nodoOp = new NodoAST(".and.");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado
                    return (boolean)resultadoIzq && (boolean)resultadoDerecho;

                //OTHER AND OTHER = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * OPERACION LOGICA OR
                 */
            }else if (this.operador.equalsIgnoreCase(".or.")) {
                String descError = "No se pueden operar logicamente los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                //LOGICAL OR LOGICAL = ERROR
                if(this.operadorIzq.type == Tipo.Type.LOGICAL && this.operadorDer.type == Tipo.Type.LOGICAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos para el AST
                    nodoOp = new NodoAST(".or.");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado
                    return (boolean)resultadoIzq || (boolean)resultadoDerecho;

                //OTHER OR OTHER = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            //OPERADOR DESCONOCIDO
            }else {
                String descError = "Error, Operador desconocido ("+this.operador+") ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
        }else {
            /**
             * OPERACION LOGICA NOT
             */
            java.lang.Object resultadoDerecho = this.operadorDer.execute(table, tree);
            if (resultadoDerecho instanceof Excepcion) {
                return resultadoDerecho;
            }
            if (this.operador.equalsIgnoreCase(".not.")) {
                // NOT LOGICAL
                if (this.operadorDer.type == Tipo.Type.LOGICAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos para el AST
                    nodoOp = new NodoAST(".not.");
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado negado
                    return !(boolean)resultadoDerecho;

                //NOT OTHER = ERROR
                }else {
                    String descError = "No se puede aplicar not al tipo " + this.operadorDer.type + "";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            }else {
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Operador desconocido", this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        /**
         * OPERACIONES DE 2 EXPRESIONES RELACIONALES DE CUALQUIER TIPO
         */
        if (this.operadorIzq != null) {
            if (this.operadorDer != null) {
                //Se llaman de forma Recursiva
                Quadrupla izq = this.operadorIzq.translate(controlador);
                Quadrupla der = this.operadorDer.translate(controlador);
                String temporal = controlador.getTemps();
                System.out.println("Logica 3D - 2 Valores");
                Quadrupla quad;

                if (this.operador.equalsIgnoreCase(".and.")) {
                    quad = new Quadrupla("&&", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                } else if (this.operador.equalsIgnoreCase(".or.")) {
                    quad = new Quadrupla("||", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                }
            }
        } else{
            if(this.operadorDer != null){
                if(this.operador.equalsIgnoreCase(".not.")){
                    Quadrupla der = this.operadorDer.translate(controlador);
                    String temporal = controlador.getTemps();
                    System.out.println("Logica 3D - Negacion");
                    Quadrupla quad = new Quadrupla("!", "", der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                }
            }
        }
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("LOGICA");

        if(nodoOp != null && nodoDer != null){
            if(nodoIzq != null){
                nodoOp.agregarHijo(nodoIzq);
                nodoOp.agregarHijo(nodoDer);
            }else{
                nodoOp.agregarHijo(nodoDer);
            }
            return nodoOp;
        }
        return nodo;
    }
}
