package expresiones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

public class Aritmetica extends Instruccion {

    Instruccion operadorIzq;
    Instruccion operadorDer;
    String operador;
    NodoAST nodoOp;
    NodoAST nodoIzq;
    NodoAST nodoDer;

    public Aritmetica(Instruccion operadorIzq, Instruccion operadorDer, String operador, int line, int column) {
        super(null, line, column);
        this.operadorIzq = operadorIzq;
        this.operadorDer = operadorDer;
        this.operador = operador;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        if(this.operadorDer == null){
            String descError = "No existe el operador derecho";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        if (this.operadorIzq != null) {
            
            //Verificacion de errores
            java.lang.Object resultadoIzq = this.operadorIzq.execute(table, tree);
            if (resultadoIzq instanceof Excepcion) {
                return resultadoIzq;
            }

            java.lang.Object resultadoDerecho = this.operadorDer.execute(table, tree);
            if (resultadoDerecho instanceof Excepcion) {
                return resultadoDerecho;
            }

            /**
             * OPERACION ARITMETICA SUMA Y TODAS SUS COMBINACIONES
             */
            if (this.operador.equalsIgnoreCase("+")) {
                String descError = "No se pueden Sumar los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                /**
                 * SUMA DE INTEGER Y SUS COMBINACIONES
                 */
                if(this.operadorIzq.type == Tipo.Type.INTEGER) {

                    //INTEGER + INTEGER = INTEGER
                    if (this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.INTEGER;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("+");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (int)resultadoIzq + (int)resultadoDerecho;

                        //INTEGER + REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("+");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (int)resultadoIzq + (double)resultadoDerecho;

                        //INTEGER + OTROS = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                    /**
                     * SUMA DE REALES Y SUS COMBINACIONES
                     */
                }else if (this.operadorIzq.type == Tipo.Type.REAL) {
                    //REAL + INTEGER = REAL
                    if(this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("+");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (double)resultadoIzq + (int)resultadoDerecho;

                        //REAL + REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("+");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (double)resultadoIzq + (double)resultadoDerecho;
                    }else {
                        //REAL + OTROS = ERROR
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                }else {
                    //OTROS + OTROS = ERROR
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }


                /**
                 * OPERACION ARITMETICA RESTA
                 */
            }else if (this.operador.equalsIgnoreCase("-")) {
                String descError = "No se pueden Restar los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                /**
                 * RESTA DE INTEGER Y SUS COMBINACIONES
                 */
                if (this.operadorIzq.type == Tipo.Type.INTEGER) {

                    // INTEGER - INTEGER = INTEGER
                    if (this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.INTEGER;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("-");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (int)resultadoIzq - (int)resultadoDerecho;

                    //INTEGER - REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("-");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (int)resultadoIzq - (double)resultadoDerecho;

                    //INTEGER + OTRO = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                /**
                * RESTA DE REALES Y SUS COMBINACIONES
                */
                }else if (this.operadorIzq.type == Tipo.Type.REAL) {

                    // REAL + INTEGER = REAL
                    if (this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("-");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (double)resultadoIzq - (int)resultadoDerecho;

                    //REAL + REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("-");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (double)resultadoIzq - (double)resultadoDerecho;

                    //REAL + OTROS = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                //OTROS + OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * OPERACION ARITMETICA MULTIPLICACION
                 */
            }else if (this.operador.equalsIgnoreCase("*")) {
                String descError = "No se pueden Multiplicar los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                /**
                 * MULTIPLICACION DE ENTEROS Y SUS COMBINACIONES
                 */
                if (this.operadorIzq.type == Tipo.Type.INTEGER) {
                    //INTEGER * INTEGER = INTEGER
                    if (this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.INTEGER;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("*");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (int)resultadoIzq * (int)resultadoDerecho;

                    //INTEGER * REAL = REAL
                    } else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("*");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (int)resultadoIzq * (double)resultadoDerecho;

                    //INTEGER * OTRO = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                    /**
                     * MULTIPLICACION  DE REALES Y SUS COMBINACIONES
                     */
                } else if (this.operadorIzq.type == Tipo.Type.REAL) {
                    //REAL * INTEGER = REAL
                    if (this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.INTEGER;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("*");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (double) resultadoIzq * (int)resultadoDerecho;

                    //REAL * REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("*");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        return (double)resultadoIzq * (double)resultadoDerecho;

                    //REAL * OTRO = ERROR
                    }else{
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                //OTRO * OTRO = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * OPERACION ARITMETICA DIVISION
                 */
            } else if (this.operador.equalsIgnoreCase("/")) {
                String descError = "No se pueden Dividir los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                // DIVISION ENTRE 0 = ERROR
                if ((int)resultadoDerecho == 0) {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico,
                            "Error aritmetico, La division con cero no esta permitida",
                            this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * DIVISION DE INTEGER Y SUS COMBINACIONES
                 */
                if (this.operadorIzq.type == Tipo.Type.INTEGER) {
                    //INTEGER / INTEGER = INTEGER
                    if (this.operadorDer.type == Tipo.Type.INTEGER){
                        this.type = Tipo.Type.INTEGER;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("/");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        int resultado = (int)resultadoIzq / (int)resultadoDerecho;
                        return resultado;

                    //INTEGER / REAL = REAL
                    } else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("/");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        double resultado = (int)resultadoIzq / (double)resultadoDerecho;
                        return resultado;

                    // INTEGER / OTRO = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }

                    /**
                     * DIVISION DE REALES Y SUS COMBINACIONES
                     */
                } else if (this.operadorIzq.type == Tipo.Type.REAL) {
                    // REAL / INTEGER = REAL
                    if(this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("/");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        double resultado = (double)resultadoIzq / (int)resultadoDerecho;
                        return resultado;

                    //REAL / REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("/");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        double resultado = (double)resultadoIzq / (double)resultadoDerecho;
                        return resultado;

                    // REAL / OTRO = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                // OTRO / OTRO = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * OPERACION ARITMETICA POTENCIA
                 */
            }else if (this.operador.equalsIgnoreCase("**")) {
                String descError = "No se pueden Potenciar los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                /**
                 * POTENCIA DE INTEGER Y SUS COMBINACIONES
                 */
                if (this.operadorIzq.type == Tipo.Type.INTEGER) {
                    // INTEGER ** INTEGER = INTEGER
                    if (this.operadorDer.type == Tipo.Type.INTEGER){
                        this.type = Tipo.Type.INTEGER;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("**");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        int resultado = (int)Math.pow((int)resultadoIzq,(int)resultadoDerecho);
                        return resultado;

                    // INTEGER ** REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("**");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        double resultado = Math.pow((int)resultadoIzq,(double)resultadoDerecho);
                        return resultado;

                    // INTEGER ** OTROS = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }

                    /**
                     * POTENCIA DE REALES Y SUS COMBINACIONES
                     */
                } else if (this.operadorIzq.type == Tipo.Type.REAL) {
                    // REAL ** INTEGER = REAL
                    if(this.operadorDer.type == Tipo.Type.INTEGER) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("**");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        double resultado = Math.pow((double)resultadoIzq,(int)resultadoDerecho);
                        return resultado;

                    // REAL ** REAL = REAL
                    }else if (this.operadorDer.type == Tipo.Type.REAL) {
                        this.type = Tipo.Type.REAL;
                        //Creamos los Nodos para el AST
                        nodoOp = new NodoAST("**");
                        nodoIzq = this.operadorIzq.getAST();
                        nodoDer = this.operadorDer.getAST();
                        //Retornamos el resultado
                        double resultado = Math.pow((double)resultadoIzq,(double)resultadoDerecho);
                        return resultado;

                    // REAL ** OTROS = ERROR
                    }else {
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }

                // OTROS ** OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            }else {
                String descError = "Error, Operador desconocido ("+this.operador+") ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            /**
             * OPERACION ARITMETICA UNARIO
              */
        } else {
            java.lang.Object resultadoDerecho = this.operadorDer.execute(table, tree);
            if (resultadoDerecho instanceof Excepcion) {
                return resultadoDerecho;
            }
            /**
             * INTEGER NEGATIVO Y SUS COMBINACIONES
             */
            if (this.operador.equalsIgnoreCase("-")) {
                // - INTEGER
                if (this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.INTEGER;
                    //Creamos los Nodos para el AST
                    nodoOp = new NodoAST("-");
                    nodoDer = this.operadorDer.getAST();
                    //Retornamos el resultado
                    return -1 * (int)resultadoDerecho;

                // - REAL
                } else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.REAL;
                    //Creamos los Nodos para el AST
                    nodoOp = new NodoAST("-");
                    nodoDer = this.operadorDer.getAST();
                    //Retornamos el resultado
                    return -1 * (double)resultadoDerecho;

                // - OTROS = ERROR
                } else {
                    String descError = "No se puede aplicar negativo al tipo " + this.operadorDer.type + "";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            // ERROR GENERAL
            }else{
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Operador desconocido \n", this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        /**
         * OPERACIONES DE 2 EXPRESIONES
         */
        if (this.operadorIzq != null) {
            if(this.operadorDer != null) {
                switch (this.operador) {
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                    case "**":
                        //Se llaman de forma Recursiva
                        Quadrupla izq = this.operadorIzq.translate(controlador);
                        Quadrupla der = this.operadorDer.translate(controlador);
                        String temporal = controlador.getTemps();
                        System.out.println("Aritmetica 3D - 2 Valores");
                        Quadrupla quad = new Quadrupla(this.operador.toString(), izq.resultado, der.resultado, temporal);
                        controlador.addQuad(quad);
                        return quad;
                }
            }
            /**
             * OPERADOR UNARIO
             */
        }else{
            //Operador derecho
            if(this.operadorDer != null) {
                if(this.operador.equalsIgnoreCase("-")){
                    Quadrupla der = this.operadorDer.translate(controlador);
                    String temporal = controlador.getTemps();
                    System.out.println("Aritmetica 3D - Unario");
                    Quadrupla quad = new Quadrupla(this.operador.toString(), "", der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                }
            }
        }

        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ARITMETICA");

        if(nodoOp != null && nodoDer != null){
            if(nodoIzq != null){
                nodoOp.agregarHijo(nodoIzq);
                nodoOp.agregarHijo(nodoDer);
            }else{
                nodoOp.agregarHijo(nodoDer);
            }
            return nodoOp;
        }
        return nodo;
    }
}
