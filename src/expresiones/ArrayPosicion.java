package expresiones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.Excepcion;
import entorno.*;

import java.util.ArrayList;

public class ArrayPosicion extends Instruccion {
    String id;
    Instruccion pos1;
    Instruccion pos2;
    java.lang.Object po1;
    java.lang.Object po2;
    // PARA EL ARBOL AST
    NodoAST nodoPadre;
    NodoAST nodoId;
    NodoAST nodoPosicion;
    NodoAST nodoValor;
    public ArrayPosicion(String id, Instruccion pos1, Instruccion pos2, int line, int column) {
        super(null, line, column);
        this.id = id;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol variable = table.getVariable(this.id);
        if (variable == null) {
            String descError = "La variable ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        // Verificamos que venga una posicion
        if(this.pos1 == null){
            String descError = "Debe enviar una posicion del arreglo para acceder a su contenido ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        /**
         * VALIDACIONES PARA ARREGLOS DE 1 DIMENSION
         */
        if(variable.tipo2.tipo == Tipo.Type.ARREGLO || variable.tipo2.tipo == Tipo.Type.ALLOCATE) {
            po1 = this.pos1.execute(table, tree);
            if (po1 instanceof Excepcion) {
                return po1;
            }

            //Verificamos que la primera posicion sea un INTEGER
            if(this.pos1.type != Tipo.Type.INTEGER){
                String descError = "La posicion ingresada debe ser un INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }

            //Verificamos que solo venga una posicion
            if(this.pos2 != null){
                String descError = "Este arreglo unicamente tiene una dimension ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico,descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }

            if(po1 instanceof Integer p1) {
                ArrayList<Instruccion> listaI = (ArrayList<Instruccion>) variable.valor;
                if (p1 <= listaI.size() && p1 > 0) {
                    //Le asignamos el tipo a esta clase
                    this.type = variable.tipo.tipo;

                    //Obtenemos el nodo de la posicion buscada
                    Instruccion val = listaI.get(p1 - 1);
                    java.lang.Object result = val.execute(table, tree);

                    //Obtenemos los nodos del AST;
                    nodoPadre = new NodoAST("ARRAY");

                    nodoId = new NodoAST("ID");
                    nodoId.agregarHijo(new NodoAST(this.id));

                    nodoPosicion = new NodoAST("Posicion");
                    nodoPosicion.agregarHijo(this.pos1.getAST());

                    nodoId.agregarHijo(nodoPosicion);

                    nodoPadre.agregarHijo(nodoId);
                    nodoPadre.agregarHijo(val.getAST());

                    //Retorno el valor
                    return result;
                } else {
                    String descError = "El arreglo no puede ser accedido porque la posicion no es la adecuada";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                } //ERROR
            }
            // ERROR DE ACCESO A LA POSICION
            else{
                    String descError = "El arreglo no puede ser accedido porque la posicion no es INTEGER";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
            }

            /**
             * VALIDACIONES PARA EL ACCESO A LOS ARREGLOS DE 2 DIMENSIONES
             */
        }else if (variable.tipo2.tipo == Tipo.Type.ARREGLO2 || variable.tipo2.tipo == Tipo.Type.ALLOCATE2) {
            po1 = this.pos1.execute(table, tree);
            if (po1 instanceof Excepcion) {
                return po1;
            }

            //Verificamos que la primera posicion sea un INTEGER
            if(this.pos1.type != Tipo.Type.INTEGER){
                String descError = "La posicion ingresada debe ser un INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }

            //Verificamos que vengan 2 posiciones
            if(this.pos2 == null){
                String descError = "Debe enviar 2 posiciones, para acceder al contenido del arreglo ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }

            po2 = this.pos2.execute(table, tree);
            if (po2 instanceof Excepcion) {
                return po1;
            }

            //Verificamos que la segunda posicion sea un INTEGER
            if(this.pos1.type != Tipo.Type.INTEGER){
                String descError = "La posicion ingresada debe ser un INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }

            if(po1 instanceof Integer p1) {
                //Obtenemos la lista de listas de la variable
                ArrayList<ArrayList<Instruccion>> listaI = (ArrayList<ArrayList<Instruccion>>) variable.valor;

                if (p1 <= listaI.size() && p1 > 0) {
                    //Obtenemos la lista de nodos necesaria
                    ArrayList<Instruccion> listaJ = listaI.get(p1 - 1);

                    if(po2 instanceof Integer p2) {
                        if (p2 <= listaJ.size() && p2 > 0) {
                            //Le coloco el tipo a esta clase
                            this.type = variable.tipo.tipo;

                            //Obtenemos el nodo de la posicion buscada
                            Instruccion val = listaJ.get(p2 - 1);
                            java.lang.Object result = val.execute(table, tree);

                            //Creamos los nodos del AST;
                            nodoPadre = new NodoAST("ARRAY");

                            nodoId = new NodoAST("ID");
                            nodoId.agregarHijo(new NodoAST(this.id));

                            nodoPosicion = new NodoAST("Posicion");
                            nodoPosicion.agregarHijo(this.pos1.getAST());
                            nodoPosicion.agregarHijo(this.pos2.getAST());

                            nodoId.agregarHijo(nodoPosicion);

                            nodoPadre.agregarHijo(nodoId);
                            nodoPadre.agregarHijo(val.getAST());

                            //Retornamos el valor
                            return result;
                        }

                        //ERROR DE ASIGNACION
                        else {
                            String descError = "El arreglo no puede ser asignado porque la posicion no es la adecuada ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }
                    }

                    //ERROR DE ACCESO INTEGER
                    else{
                        String descError = "El arreglo no puede ser accedido porque la posicion no es INTEGER ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }

                }

                //ERROR DE ACCESO POR POSICION
                else {
                    String descError = "El arreglo no puede ser accedido porque la posicion no es la adecuada ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            }

            //ERROR DE ACCESO DE TIPO INTEGER
            else{
                String descError = "El arreglo no puede ser accedido porque la posicion no es INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
        }

        //ERROR GENERL
        else{
            String descError = "La variable no puede ser accedida porque no es un arreglo ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ARRAY");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
