package expresiones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

public class Relacional extends Instruccion {
    Instruccion operadorIzq;
    Instruccion operadorDer;
    String operador;
    NodoAST nodoOp;
    NodoAST nodoIzq;
    NodoAST nodoDer;

    public Relacional(Instruccion operadorIzq, Instruccion operadorDer, String operador, int line, int column) {
        super(null, line, column);
        this.operadorIzq = operadorIzq;
        this.operadorDer = operadorDer;
        this.operador = operador;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VERIFICACION DE EXCEPCIONES
         */

        if(this.operadorIzq == null){
            String descError = "No existe el operador izquierdo";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        if(this.operadorDer == null){
            String descError = "No existe el operador derecho";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }


        java.lang.Object resultadoIzq = this.operadorIzq.execute(table, tree);

        if (resultadoIzq instanceof Excepcion) {
            return resultadoIzq;
        }

        java.lang.Object resultadoDerecho = this.operadorDer.execute(table, tree);
        if (resultadoDerecho instanceof Excepcion) {
            return resultadoDerecho;
        }
        /**
         * OPERACION RELACIONAL == | IGUAL A
         */
        if (this.operador.equalsIgnoreCase("==") || this.operador.equalsIgnoreCase(".eq.")) {
            //VERIFICAMOS LOS TIPOS
            if(this.operadorIzq.type == this.operadorDer.type) {
                this.type = Tipo.Type.LOGICAL;
                //Creamos los nodos que usare en el AST
                nodoOp = new NodoAST("==");
                nodoIzq = this.operadorIzq.getAST();
                nodoDer = this.operadorDer.getAST();

                //Retornamos la igualacion de los valores
                return resultadoIzq == resultadoDerecho;

            //TIPOS DIFERENTES = ERROR
            }else {
                String descError = "No se pueden Igualar los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            /**
             * OPERACION RELACIONAL != | NO IGUAL
             */
        }else if (this.operador.equalsIgnoreCase("/=")|| this.operador.equalsIgnoreCase(".ne.")){
            //VERIFICACION DE TIPOS
            if(this.operadorIzq.type == this.operadorDer.type) {
                this.type = Tipo.Type.LOGICAL;
                //Creamos los nodos que usare en el AST
                nodoOp = new NodoAST("!=");
                nodoIzq = this.operadorIzq.getAST();
                nodoDer = this.operadorDer.getAST();

                //Retornamos el resultado de los valores
                return resultadoIzq != resultadoDerecho;

            //TIPOS DIFERENTES = ERROR
            }else {
                String descError = "No se pueden Des Igualar los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            /**
             * OPERACION RELACIONAL > | MAYOR QUE
             */
        }else if (this.operador.equalsIgnoreCase(">")|| this.operador.equalsIgnoreCase(".gt.")){
            String descError = "No se pueden usar > en los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
            /**
             * MAYOR QUE PARA INTEGER Y SUS COMBINACIONES
             */
            if(this.operadorIzq.type == Tipo.Type.INTEGER) {
                //INTEGER > INTEGER
                if (this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq > (int)resultadoDerecho;

                //INTEGER > REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq > (double)resultadoDerecho;

                //INTEGER > OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * MAYOR QUE PARA REALES Y SUS COMBINACIONES
                 */
            } else if (this.operadorIzq.type == Tipo.Type.REAL) {
                // REAL > INTEGER
                if(this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq > (int)resultadoDerecho;

                //REAL > REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq > (double)resultadoDerecho;

                //REAL > OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            //OTROS > OTROS = ERROR
            }else {
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            /**
             * OPERACION RELACIONAL >= | MAYOR IGUAL
             */
        }else if (this.operador.equalsIgnoreCase(">=")|| this.operador.equalsIgnoreCase(".ge.")){
            String descError = "No se pueden usar >=  en los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
            /**
             * MAYOR IGUAL CON INTEGER Y SUS COMBINACIONES
             */
            if(this.operadorIzq.type == Tipo.Type.INTEGER) {
                //INTEGER >= INTEGER
                if (this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq >= (int)resultadoDerecho;

                //INTEGER >= REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq >= (double)resultadoDerecho;

                // OTROS >= OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * MAYOR IGUAL CON REALES Y SUS COMBINACIONES
                 */
            }else if (this.operadorIzq.type == Tipo.Type.REAL) {
                //REAL >= INTEGER
                if(this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq >= (int)resultadoDerecho;

                //REAL >= REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST(">=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq >= (double)resultadoDerecho;

                //REAL >= OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }

            // OTROS >= OTROS = ERROR
            }else {
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            /**
             * OPERACION RELACIONAL < | MENOR QUE
             */
        }else if (this.operador.equalsIgnoreCase("<")|| this.operador.equalsIgnoreCase(".lt.")){
            String descError = "No se pueden usar < en los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
            /**
             * MENOR QUE PARA INTEGER Y SUS COMBINACIONES
             */
            if(this.operadorIzq.type == Tipo.Type.INTEGER) {
                //INTEGER < INTEGER
                if (this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq < (int)resultadoDerecho;

                //INTEGER < REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq < (double)resultadoDerecho;

                //INTEGER < OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }

                /**
                 * MENOR QUE PARA REALES Y SUS COMBINACIONES
                 */
            }else if (this.operadorIzq.type == Tipo.Type.REAL) {
                //REAL < INTEGER
                if(this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq < (int)resultadoDerecho;

                //REAL < REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq < (double)resultadoDerecho;

                //REAL < OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }

            //OTROS < OTROS = ERROR
            }else {
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            /**
             * OPERACION RELACIONAL <= | MENOR IGUAL
             */
        }else if (this.operador.equalsIgnoreCase("<=")|| this.operador.equalsIgnoreCase(".le.")){
            String descError = "No se pueden usar <= en los tipos (" + this.operadorIzq.type + ") y (" + this.operadorDer.type + ") ";
            /**
             * MENOR IGUAL CON INTEGER Y SUS COMBINACIONES
             */
            if(this.operadorIzq.type == Tipo.Type.INTEGER) {
                // INTEGER <= INTEGER
                if (this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq <= (int)resultadoDerecho;

                //INTEGER <= REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (int)resultadoIzq <= (double)resultadoDerecho;

                // INTEGER <= OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }

                /**
                 * MENOR IGUAL CON REALES Y SUS COMBINACIONES
                 */
            }else if (this.operadorIzq.type == Tipo.Type.REAL) {
                // REAL <= INTEGER
                if(this.operadorDer.type == Tipo.Type.INTEGER) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq <= (int)resultadoDerecho;

                //REAL <= REAL
                }else if (this.operadorDer.type == Tipo.Type.REAL) {
                    this.type = Tipo.Type.LOGICAL;
                    //Creamos los nodos que usare en el AST
                    nodoOp = new NodoAST("<=");
                    nodoIzq = this.operadorIzq.getAST();
                    nodoDer = this.operadorDer.getAST();

                    //Retornamos el resultado de los valores
                    return (double)resultadoIzq <= (double)resultadoDerecho;

                // REAL <= OTROS = ERROR
                }else {
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
            // OTROS <= OTROS = ERROR
            }else {
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
        // ERROR GENERAL
        }else{
            String descError = "Error, Operador desconocido ("+this.operador+") ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
    }

    public Quadrupla translate(QuadControlador controlador) {
        /**
         * OPERACIONES DE 2 EXPRESIONES RELACIONALES DE CUALQUIER TIPO
         */
        if (this.operadorIzq != null) {
            if (this.operadorDer != null) {
                //Se llaman de forma Recursiva
                Quadrupla izq = this.operadorIzq.translate(controlador);
                Quadrupla der = this.operadorDer.translate(controlador);
                String temporal = controlador.getTemps();
                System.out.println("Relacional 3D - 2 Valores");
                Quadrupla quad;
                
                if (this.operador.equalsIgnoreCase("==") || this.operador.equalsIgnoreCase(".eq.")) {
                    quad = new Quadrupla("==", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                } else if (this.operador.equalsIgnoreCase("/=") || this.operador.equalsIgnoreCase(".ne.")) {
                    quad = new Quadrupla("!=", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                } else if (this.operador.equalsIgnoreCase(">") || this.operador.equalsIgnoreCase(".gt.")) {
                    quad = new Quadrupla(">", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                } else if (this.operador.equalsIgnoreCase(">=") || this.operador.equalsIgnoreCase(".ge.")) {
                    quad = new Quadrupla(">=", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                } else if (this.operador.equalsIgnoreCase("<") || this.operador.equalsIgnoreCase(".lt.")) {
                    quad = new Quadrupla("<", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                } else if (this.operador.equalsIgnoreCase("<=") || this.operador.equalsIgnoreCase(".le.")) {
                    quad = new Quadrupla("<=", izq.resultado, der.resultado, temporal);
                    controlador.addQuad(quad);
                    return quad;
                }
            }
        } 


        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("RELACIONAL");

        if(nodoOp != null && nodoDer != null && nodoIzq != null){
            nodoOp.agregarHijo(nodoIzq);
            nodoOp.agregarHijo(nodoDer);
            return nodoOp;
        }

        return nodo;
    }
}
