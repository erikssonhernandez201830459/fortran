import Abstract.Instruccion;
import entorno.*;
import expresiones.*;
import instrucciones.*;

import grammar.GrammarBaseVisitor;
import grammar.GrammarParser;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

public class Visitor extends GrammarBaseVisitor {
    /**
     * INICIO DE LA GRAMATICA
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitStart(GrammarParser.StartContext ctx) {
        return visit(ctx.bloqueInstrucciones());
    }

    /**
     * BLOQUE DE INSTRUCCIONES
     * Contiene las ejecuciones Padre como lo es:
     * - program
     * - subrutinas
     * - function
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitBloqueInstrucciones(GrammarParser.BloqueInstruccionesContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        return instrucciones;
    }

    /**
     * INSTRUCCIONES DE EJECUCION PADRES
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitInstruccionesProgram(GrammarParser.InstruccionesProgramContext ctx) {
        //VERIFICAR ERROR
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }

        return new Program(instrucciones, ctx.id1.getText(), ctx.id2.getText(), ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }

    @Override
    public Object visitInstruccionesSubrutina(GrammarParser.InstruccionesSubrutinaContext ctx) {
        ArrayList<String> listaParametros1 = new ArrayList<>();
        for(ParseTree item: ctx.e1){
            listaParametros1.add((String)visit(item));
        }

        ArrayList<Instruccion> listaParametros2 = new ArrayList<>();
        for(ParseTree item: ctx.e2){
            listaParametros2.add((Instruccion)visit(item));
        }

        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e3){
            instrucciones.add((Instruccion)visit(item));
        }

        return new SubRutina(ctx.id1.getText(), ctx.id2.getText(), listaParametros1, listaParametros2, instrucciones, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());

    }

    @Override
    public Object visitInstruccionesFuncion(GrammarParser.InstruccionesFuncionContext ctx) {
        ArrayList<String> listaParametros1 = new ArrayList<>();
        for(ParseTree item: ctx.e1){
            listaParametros1.add((String)visit(item));
        }

        ArrayList<Instruccion> listaParametros2 = new ArrayList<>();
        for(ParseTree item: ctx.e2){
            listaParametros2.add((Instruccion)visit(item));
        }

        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e3){
            instrucciones.add((Instruccion)visit(item));
        }

        return new Funcion(ctx.id1.getText(), ctx.id2.getText(), ctx.id3.getText(), listaParametros1, listaParametros2, instrucciones, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());

    }

    /**
     * LISTA DE PARAMETROS
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitListaParametrosNormal(GrammarParser.ListaParametrosNormalContext ctx) {
        return ctx.id.getText();
    }

    @Override
    public Object visitListaParametrosComa(GrammarParser.ListaParametrosComaContext ctx) {

        return ctx.id.getText();
    }

    /**
     * LISTA DE DECLARACION DE PARAMETROS
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitListaDeclaracionParametrosNormal(GrammarParser.ListaDeclaracionParametrosNormalContext ctx) {
        Tipo type = (Tipo)visit(ctx.tipo());
        return new Declaracion(type.tipo, ctx.id.getText(), Default.value(type.tipo, ctx.id.getLine(), ctx.id.getCharPositionInLine()), ctx.id.getLine(),ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitListaDeclaracionParametrosArray2Dim(GrammarParser.ListaDeclaracionParametrosArray2DimContext ctx) {
        return new DeclaracionArray(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.dim1), (Instruccion)visit(ctx.dim2), ctx.id.getLine(), ctx.id.getCharPositionInLine());

    }

    @Override
    public Object visitListaDeclaracionParametrosArray1Dim(GrammarParser.ListaDeclaracionParametrosArray1DimContext ctx) {
        return new DeclaracionArray(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.dim1), null, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    /**
     * INSTRUCCIONES DENTRO DE LA EJECUCION DE CUALQUIER BLOQUE DE INSTRUCCIONES
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitInstruccionesDeclaracion(GrammarParser.InstruccionesDeclaracionContext ctx) {
        return visit(ctx.declaracion());
    }

    @Override
    public Object visitInstruccionesAsignacion(GrammarParser.InstruccionesAsignacionContext ctx) {
        return visit(ctx.asignacion());
    }

    @Override
    public Object visitInstruccionesAsignacionA(GrammarParser.InstruccionesAsignacionAContext ctx) {
        return visit(ctx.asignacionA());
    }

    @Override
    public Object visitInstruccionesAllocate(GrammarParser.InstruccionesAllocateContext ctx) {
        return visit(ctx.allocate());
    }

    @Override
    public Object visitInstruccionesPrint(GrammarParser.InstruccionesPrintContext ctx) {
        return visit(ctx.print());
    }

    @Override
    public Object visitInstruccionesIf(GrammarParser.InstruccionesIfContext ctx) {
        return visit(ctx.if_());
    }

    @Override
    public Object visitInstruccionesDo(GrammarParser.InstruccionesDoContext ctx) {
        return visit(ctx.do_());
    }

    @Override
    public Object visitInstruccionesExit(GrammarParser.InstruccionesExitContext ctx) {
        return visit(ctx.exit());
    }

    @Override
    public Object visitInstruccionesCycle(GrammarParser.InstruccionesCycleContext ctx) {
        return visit(ctx.cycle());
    }

    @Override
    public Object visitInstruccionesCall(GrammarParser.InstruccionesCallContext ctx) {
        return visit(ctx.call());
    }

    /**
     * DECLARACION DE VARIABLES DE CUALQUIER TIPO
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitDeclaracionArray2Dim(GrammarParser.DeclaracionArray2DimContext ctx) {
        return new DeclaracionArray(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.dim1), (Instruccion)visit(ctx.dim2), ctx.id.getLine(), ctx.id.getCharPositionInLine());

    }

    @Override
    public Object visitDeclaracionArray1Dim(GrammarParser.DeclaracionArray1DimContext ctx) {
        return new DeclaracionArray(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.dim1), null, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitDeclaracionAllocatable2Dim(GrammarParser.DeclaracionAllocatable2DimContext ctx) {
        return new DeclaracionArrayAllocatable(((Tipo)visit(ctx.tipo())).tipo,ctx.id.getText(),false, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitDeclaracionAllocatable1Dim(GrammarParser.DeclaracionAllocatable1DimContext ctx) {
        return new DeclaracionArrayAllocatable(((Tipo)visit(ctx.tipo())).tipo,ctx.id.getText(),true, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitDeclaracionArray2Dim2(GrammarParser.DeclaracionArray2Dim2Context ctx) {
        return new DeclaracionArray(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.dim1), (Instruccion)visit(ctx.dim2), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitDeclaracionArray1Dim2(GrammarParser.DeclaracionArray1Dim2Context ctx) {
        return new DeclaracionArray(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.dim1), null, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitDeclaracionAsig(GrammarParser.DeclaracionAsigContext ctx) {
        ArrayList<Instruccion> listDec = new ArrayList<Instruccion>();
        listDec.add(new Declaracion(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), (Instruccion)visit(ctx.expresion()), ctx.id.getLine(), ctx.id.getCharPositionInLine()));

        for(ParseTree item: ctx.e){
            Declaracion decla = (Declaracion)visit(item);
            decla.type = ((Tipo)visit(ctx.tipo())).tipo;
            if(decla.valor == null){
                decla.valor = Default.value(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getLine(), ctx.id.getCharPositionInLine());
            }
            listDec.add(decla);
        }

        return new ListaDeclaraciones(listDec, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitDeclaracionUniq(GrammarParser.DeclaracionUniqContext ctx) {
        ArrayList<Instruccion> listDec = new ArrayList<Instruccion>();
        listDec.add(new Declaracion(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getText(), Default.value(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getLine(), ctx.id.getCharPositionInLine()), ctx.id.getLine(), ctx.id.getCharPositionInLine()));

        for(ParseTree item: ctx.e){
            Declaracion decla = (Declaracion)visit(item);
            decla.type = ((Tipo)visit(ctx.tipo())).tipo;
            if(decla.valor == null){
                decla.valor = Default.value(((Tipo)visit(ctx.tipo())).tipo, ctx.id.getLine(), ctx.id.getCharPositionInLine());
            }
            listDec.add(decla);
        }

        return new ListaDeclaraciones(listDec, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    /**
     * LISTADO DE DECLARACIONES YA SEPARADO
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitListaDeclaracionAsig(GrammarParser.ListaDeclaracionAsigContext ctx) {
        return new Declaracion(null, ctx.id.getText(), (Instruccion)visit(ctx.expresion()), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitListaDeclaracionUniq(GrammarParser.ListaDeclaracionUniqContext ctx) {
        return new Declaracion(null, ctx.id.getText(), null, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    /**
     * ASIGNACION VARIABLES Y ARREGLOS NORMALES
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitAsignacionId(GrammarParser.AsignacionIdContext ctx) {
        return new Asignacion(ctx.id.getText(), visit(ctx.expresion()), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitAsignacionArray1(GrammarParser.AsignacionArray1Context ctx) {
        return new AsignacionArray(ctx.id.getText(), (Instruccion)visit(ctx.num), null, (Instruccion)visit(ctx.val), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitAsignacionArray2(GrammarParser.AsignacionArray2Context ctx) {
        return new AsignacionArray(ctx.id.getText(), (Instruccion)visit(ctx.num1), (Instruccion)visit(ctx.num2), (Instruccion)visit(ctx.val), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    /**
     * ASIGNACION ARREGLOS DINAMICOS
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitAsignacionArray1D(GrammarParser.AsignacionArray1DContext ctx) {
        return new AsignacionArrayDinamico(ctx.id.getText(), (Instruccion)visit(ctx.val), ctx.id.getLine(),ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitAsignacionArray2D(GrammarParser.AsignacionArray2DContext ctx) {
        return new AsignacionArrayDinamico(ctx.id.getText(), (Instruccion)visit(ctx.val), ctx.id.getLine(),ctx.id.getCharPositionInLine());
    }

    /**
     * FUNCION ALLOCATE
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitAllocate1Dim(GrammarParser.Allocate1DimContext ctx) {
        return new Allocate(ctx.id.getText(), (Instruccion) visit(ctx.izq), null, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitAllocate2Dim(GrammarParser.Allocate2DimContext ctx) {
        return new Allocate(ctx.id.getText(), (Instruccion) visit(ctx.izq), (Instruccion) visit(ctx.der), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitDeallocate(GrammarParser.DeallocateContext ctx) {
        return new Deallocate(ctx.id.getText(), ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    /**
     * FUNCIONALIDAD PRINT
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitPrint(GrammarParser.PrintContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        //instrucciones.add((Instruccion)visit(ctx.ex);
        for(ParseTree item: ctx.expresion()){
            instrucciones.add((Instruccion)visit(item));
        }

        return new Print(instrucciones, ctx.p.getLine(), ctx.p.getCharPositionInLine());
    }

//    @Override
//    public Object visitListaPrint(GrammarParser.ListaPrintContext ctx) {
//        return visit(ctx.expresion());
//    }

    /**
     * FUNCIONALIDAD IF
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitIfNormal(GrammarParser.IfNormalContext ctx) {
        ArrayList<Instruccion> instruccionesIf = new ArrayList<>();
        ArrayList<Instruccion> instruccionesElse = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instruccionesIf.add((Instruccion)visit(item));
        }

        return new If((Instruccion)visit(ctx.cond), instruccionesIf, instruccionesElse, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitIfElse(GrammarParser.IfElseContext ctx) {
        ArrayList<Instruccion> instruccionesIf = new ArrayList<>();
        ArrayList<Instruccion> instruccionesElse = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instruccionesIf.add((Instruccion)visit(item));
        }
        for(ParseTree item: ctx.e2){
            instruccionesElse.add((Instruccion)visit(item));
        }

        return new If((Instruccion)visit(ctx.cond), instruccionesIf, instruccionesElse, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitIfElseIf(GrammarParser.IfElseIfContext ctx) {
        ArrayList<Instruccion> instruccionesIf = new ArrayList<>();
        ArrayList<Instruccion> instruccionesElse = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instruccionesIf.add((Instruccion)visit(item));
        }
        for(ParseTree item: ctx.e2){
            instruccionesElse.add((Instruccion)visit(item));
        }

        return new If((Instruccion)visit(ctx.cond), instruccionesIf, instruccionesElse, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitIfElseIfElse(GrammarParser.IfElseIfElseContext ctx) {
        ArrayList<Instruccion> instruccionesIf = new ArrayList<>();
        ArrayList<Instruccion> instruccionesElse = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instruccionesIf.add((Instruccion)visit(item));
        }
        for(ParseTree item: ctx.e2){
            instruccionesElse.add((Instruccion)visit(item));
        }
        for(ParseTree item: ctx.e3){
            instruccionesElse.add((Instruccion)visit(item));
        }

        return new If((Instruccion)visit(ctx.cond), instruccionesIf, instruccionesElse, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitElseif(GrammarParser.ElseifContext ctx) {
        ArrayList<Instruccion> instruccionesIf = new ArrayList<>();
        ArrayList<Instruccion> instruccionesElse = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instruccionesIf.add((Instruccion)visit(item));
        }

        return new If((Instruccion)visit(ctx.cond), instruccionesIf, instruccionesElse, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    /**
     * FUNCIONALIDAD DO
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitDoNormal(GrammarParser.DoNormalContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        return new Do("!", "!", (Instruccion)visit(ctx.inicio), (Instruccion)visit(ctx.fin), (Instruccion)visit(ctx.paso), instrucciones, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override
    public Object visitDoNormalSinPaso(GrammarParser.DoNormalSinPasoContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        Primitivo paso = new Primitivo(Tipo.Type.INTEGER, 1, ctx.id.getLine(), ctx.id.getCharPositionInLine());
        return new Do("!", "!", (Instruccion)visit(ctx.inicio), (Instruccion)visit(ctx.fin), paso, instrucciones, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitDoEtiqueta(GrammarParser.DoEtiquetaContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        return new Do(ctx.id1.getText(), ctx.id2.getText(),(Instruccion)visit(ctx.inicio), (Instruccion)visit(ctx.fin), (Instruccion)visit(ctx.paso), instrucciones, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }
    @Override
    public Object visitDoEtiquetaSinPaso(GrammarParser.DoEtiquetaSinPasoContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        Primitivo paso = new Primitivo(Tipo.Type.INTEGER, 1, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
        return new Do(ctx.id1.getText(), ctx.id2.getText(),(Instruccion)visit(ctx.inicio), (Instruccion)visit(ctx.fin), paso, instrucciones, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }

    /**
     * FUNCIONALIDAD DO WHILE
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitDoWhile(GrammarParser.DoWhileContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        return new DoWhile("!", "!",(Instruccion)visit(ctx.condicion), instrucciones, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitDoWhileEtiqueta(GrammarParser.DoWhileEtiquetaContext ctx) {
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        for(ParseTree item: ctx.e){
            instrucciones.add((Instruccion)visit(item));
        }
        return new DoWhile(ctx.id1.getText(), ctx.id2.getText(),(Instruccion)visit(ctx.condicion), instrucciones, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }

    /**
     * FUNCIONALIDAD EXIT
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitExitNormal(GrammarParser.ExitNormalContext ctx) {
        return new Exit("!", ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitExitEtiqueta(GrammarParser.ExitEtiquetaContext ctx) {
        return new Exit(ctx.id2.getText(), ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }

    /**
     * FUNCIONALIDAD CYCLE
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitCycleNormal(GrammarParser.CycleNormalContext ctx) {
        return new Cycle("!", ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }
    @Override
    public Object visitCycleEtiqueta(GrammarParser.CycleEtiquetaContext ctx) {
        return new Cycle(ctx.id2.getText(), ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }

    /**
     * FUNCIONALIDAD CALL O LLAMADA DE SUBRUTINA
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitCall(GrammarParser.CallContext ctx) {
        ArrayList<Instruccion> parametros = new ArrayList<>();
        for(ParseTree item: ctx.e){
            parametros.add((Instruccion)visit(item));
        }
        return new LlamadaSubRutina(ctx.id.getText(), parametros, ctx.id.getLine(), ctx.id.getCharPositionInLine());
    }

    @Override public Object visitListaCallNormal(GrammarParser.ListaCallNormalContext ctx) {
        return visit(ctx.val);
    }
    @Override public Object visitListaCallComa(GrammarParser.ListaCallComaContext ctx) {
        return visit(ctx.val);
    }


    /**
     * EXPRESIONES DE TODO TIPO
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitExpresionIdentificador(GrammarParser.ExpresionIdentificadorContext ctx) {
        return new Identificador(ctx.val.getText(), ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override
    public Object visitExpresionArray2(GrammarParser.ExpresionArray2Context ctx) {
        return new ArrayPosicion(ctx.val.getText(), (Instruccion)visit(ctx.pos1), (Instruccion)visit(ctx.pos2), ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override
    public Object visitExpresionArray1(GrammarParser.ExpresionArray1Context ctx) {
        return new ArrayPosicion(ctx.val.getText(), (Instruccion)visit(ctx.pos), null, ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override
    public Instruccion visitExpresionResta(GrammarParser.ExpresionRestaContext ctx) {
        return new Aritmetica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override
    public Instruccion visitExpresionNegativo(GrammarParser.ExpresionNegativoContext ctx) {
        return new Aritmetica(null, (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionMultiplicacion(GrammarParser.ExpresionMultiplicacionContext ctx) {
        return new Aritmetica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionSuma(GrammarParser.ExpresionSumaContext ctx) {
        return new Aritmetica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());    }
    @Override public Instruccion visitExpresionDivision(GrammarParser.ExpresionDivisionContext ctx) {
        return new Aritmetica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionPotencia(GrammarParser.ExpresionPotenciaContext ctx) {
        return new Aritmetica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionLt(GrammarParser.ExpresionLtContext ctx) {
        return new Relacional((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionGe(GrammarParser.ExpresionGeContext ctx) {
        return new Relacional((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionNe(GrammarParser.ExpresionNeContext ctx) {
        return new Relacional((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionLe(GrammarParser.ExpresionLeContext ctx) {
        return new Relacional((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionEq(GrammarParser.ExpresionEqContext ctx) {
        return new Relacional((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionGt(GrammarParser.ExpresionGtContext ctx) {
        return new Relacional((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionOr(GrammarParser.ExpresionOrContext ctx) {
        return new Logica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionNot(GrammarParser.ExpresionNotContext ctx) {
        return new Logica(null, (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionAnd(GrammarParser.ExpresionAndContext ctx) {
        return new Logica((Instruccion)visit(ctx.izq), (Instruccion)visit(ctx.der), ctx.op.getText(), ctx.op.getLine(), ctx.op.getCharPositionInLine());
    }
    @Override public Object visitExpresionSize(GrammarParser.ExpresionSizeContext ctx) {
        return new Size(ctx.val.getText(), ctx.si.getLine(), ctx.si.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionReal(GrammarParser.ExpresionRealContext ctx) {
        return new Primitivo(Tipo.Type.REAL, Double.parseDouble(ctx.val.getText()), ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionInt(GrammarParser.ExpresionIntContext ctx) {
        return new Primitivo(Tipo.Type.INTEGER, Integer.parseInt(ctx.val.getText()), ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionChar(GrammarParser.ExpresionCharContext ctx) {
        return new Primitivo(Tipo.Type.CHARACTER, ctx.val.getText(), ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionString(GrammarParser.ExpresionStringContext ctx) {
        return new Primitivo(Tipo.Type.STRING, ctx.val.getText(), ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionTrue(GrammarParser.ExpresionTrueContext ctx) {
        return new Primitivo(Tipo.Type.LOGICAL, true, ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override public Instruccion visitExpresionFalse(GrammarParser.ExpresionFalseContext ctx) {
        return new Primitivo(Tipo.Type.LOGICAL, false, ctx.val.getLine(), ctx.val.getCharPositionInLine());
    }
    @Override public Object visitExpresionParentesis(GrammarParser.ExpresionParentesisContext ctx) {
        return visit(ctx.expresion());
    }
    @Override public Object visitExpresionListaExpresion(GrammarParser.ExpresionListaExpresionContext ctx) {
        ArrayList<Instruccion> listDec = new ArrayList<Instruccion>();

        listDec.add((Instruccion)visit(ctx.val));

        for(ParseTree item: ctx.e){
            Instruccion valor = (Instruccion)visit(item);
            listDec.add(valor);
        }
        return listDec;
    }
    @Override public Object visitListaExpresion(GrammarParser.ListaExpresionContext ctx) {
        return visit(ctx.expresion());
    }

    @Override
    public Object visitExpresionFuncion(GrammarParser.ExpresionFuncionContext ctx) {
        ArrayList<Instruccion> parametros = new ArrayList<>();
        for(ParseTree item: ctx.e){
            parametros.add((Instruccion)visit(item));
        }
        return new LlamadaFuncion(ctx.id1.getText(), parametros, ctx.id1.getLine(), ctx.id1.getCharPositionInLine());
    }

    /**
     * TIPOS DE DATOS
     * @param ctx the parse tree
     * @return
     */
    @Override
    public Object visitTipoInteger(GrammarParser.TipoIntegerContext ctx) {
        return new Tipo(Tipo.Type.INTEGER);
    }
    @Override
    public Object visitTipoReal(GrammarParser.TipoRealContext ctx) {
        return new Tipo(Tipo.Type.REAL);
    }
    @Override
    public Object visitTipoComplex(GrammarParser.TipoComplexContext ctx) {
        return new Tipo(Tipo.Type.COMPLEX);
    }
    @Override
    public Object visitTipoCharacter(GrammarParser.TipoCharacterContext ctx) {
        return new Tipo(Tipo.Type.CHARACTER);
    }
    @Override
    public Object visitTipoLogical(GrammarParser.TipoLogicalContext ctx) {
        return new Tipo(Tipo.Type.LOGICAL);
    }
}
/**
 * PENDIENTE DE VALIDAR CAMBIOS EN EL VISITOR
 */