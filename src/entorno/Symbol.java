package entorno;

public class Symbol {
    public Tipo tipo;
    public Tipo tipo2;
    public String id;
    public Object valor;
    public int line;
    public int column;
    public int posicion;
    public SymbolTable entorno;

    public Symbol(Tipo tipo, Tipo tipo2, String id, Object valor, int line, int column, SymbolTable entorno) {
        this.tipo = tipo;
        this.tipo2 = tipo2;
        this.id = id;
        this.valor = valor;
        this.line = line;
        this.column = column;
        this.entorno = entorno;
        this.posicion = 0;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Tipo getTipo2() {
        return tipo2;
    }

    public void setTipo2(Tipo tipo2) {
        this.tipo2 = tipo2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public SymbolTable getEntorno() {
        return entorno;
    }

    public void setEntorno(SymbolTable entorno) {
        this.entorno = entorno;
    }



}
