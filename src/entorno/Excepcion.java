package entorno;

public class Excepcion {
    ErrorTipo tipo;
    String descripcion;
    int line;
    int column;

    public Excepcion(ErrorTipo tipo, String descripcion, int line, int column) {
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.line = line;
        this.column = column;
    }
    public enum ErrorTipo{
        Lexico, 
        Sintactico,
        Semantico
    }
    /*
    @Override
    public String toString() {
        return "Error " + this.tipo + " en la linea " + this.line + " y columna " + this.column + ", " + this.descripcion;
    }
         */


    @Override
    public String toString() {
        return "<TR><TD>" + (line < 0 ? "LINEA" : line) + "</TD>" +
                "<TD>" + (column < 0 ? "COLUMNA" : column) + "</TD>" +
                "<TD>" + descripcion + "</TD>" +
                "<TD>" + (tipo == null ? "TIPO DE ERROR" : tipo) + "</TD></TR>";
    }
}
