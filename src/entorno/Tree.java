package entorno;

import Abstract.Instruccion;
import Traductor.QuadControlador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Tree {
    public ArrayList<Instruccion> instrucciones;
    public ArrayList<Excepcion> excepciones;
    public ArrayList<String> consola;
    public ArrayList<Symbol> variables;
    public Map<String,  Symbol> funciones;
    public Map<String, Symbol> metodos;
    public ArrayList<SymbolTable> tablas;
    public QuadControlador controlador;
    public int posiciones;
    public final String STACK = "STACK";
    public final String HEAP = "HEAP";

    public Tree() {
        instrucciones = new ArrayList<Instruccion>();
        excepciones = new ArrayList<Excepcion>();
        consola = new ArrayList<String>();
        variables = new ArrayList<Symbol>();
        funciones = new HashMap<>();
        metodos = new HashMap<>();
        tablas = new ArrayList<SymbolTable>();
        controlador = new QuadControlador(this);
        posiciones = 0;
    }


    public void setFuncion(Symbol symbol){
        this.funciones.put(symbol.id, symbol);
    }

    public Symbol getFuncion(String id){
        for (Map.Entry<String, Symbol> entry : funciones.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(id)){
                return funciones.get(id);
            }
        }
        return null;
    }

    public void setMetodo(Symbol symbol){
        this.metodos.put(symbol.id, symbol);
    }

    public Symbol getMetodo(String id){
        for (Map.Entry<String, Symbol> entry : metodos.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(id)){
                return metodos.get(id);
            }
        }
        return null;
    }
}
