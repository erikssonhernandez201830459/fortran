package entorno;

import expresiones.Primitivo;
import grammar.GrammarParser;

public class Default {
    public static Primitivo value(Tipo.Type tipo, int line, int column) {
        if (tipo == Tipo.Type.INTEGER) {
            return new Primitivo(tipo, 0, line, column);
        } else if (tipo == Tipo.Type.REAL) {
            return new Primitivo(tipo, 0.0, line, column);
        } else if (tipo == Tipo.Type.COMPLEX) {
            return new Primitivo(tipo, "(9.192517926E-43,0.00000000)", line, column);
        } else if (tipo == Tipo.Type.CHARACTER) {
            return new Primitivo(tipo, " ", line, column);
        } else if (tipo == Tipo.Type.LOGICAL) {
            return new Primitivo(tipo, false, line, column);
        }
        return null;
    }

    public static String value3D(Tipo.Type tipo) {
        if (tipo == Tipo.Type.INTEGER) {
            return "0";
        } else if (tipo == Tipo.Type.REAL) {
           return "0.0";
        } else if (tipo == Tipo.Type.COMPLEX) {
           return "(9.192517926E-43,0.00000000)";
        } else if (tipo == Tipo.Type.CHARACTER) {
            return "' '";
        } else if (tipo == Tipo.Type.LOGICAL) {
            return "0";
        }
        return "";
    }

    public static String value3DString(String tipo) {
        if (tipo.equalsIgnoreCase("integer")) {
            return "0";
        } else if (tipo.equalsIgnoreCase("real")) {
            return "0.0";
        } else if (tipo.equalsIgnoreCase("complex")) {
            return "(9.192517926E-43,0.00000000)";
        } else if (tipo.equalsIgnoreCase("character")) {
            return "' '";
        } else if (tipo.equalsIgnoreCase("logical")) {
            return "0";
        }
        return "";
    }
}
