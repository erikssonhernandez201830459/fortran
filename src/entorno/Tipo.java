package entorno;

public class Tipo {
    public enum Type{
        INTEGER,
        REAL,
        COMPLEX,
        CHARACTER,
        LOGICAL,
        ARREGLO,
        ARREGLO2,
        ALLOCATE,
        ALLOCATE2,
        STRING,
        VOID,
        METODO,
        FUNCION,
        VARIABLE,
        NULL;

        public Tipo.Type toType(String tipo) {
            if (tipo.equalsIgnoreCase("integer")) {
                return Type.INTEGER;
            } else if (tipo.equalsIgnoreCase("real")) {
                return Type.REAL;
            } else if (tipo.equalsIgnoreCase("complex")) {
                return Type.COMPLEX;
            } else if (tipo.equalsIgnoreCase("character")) {
                return Type.CHARACTER;
            } else if (tipo.equalsIgnoreCase("logical")) {
                return Type.LOGICAL;
            }
            return Type.STRING;
        }
    }
    public Type tipo;

    public Tipo(Type tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        if (this.tipo == Type.INTEGER) {
            return "entero";
        } else if (this.tipo == Type.REAL) {
            return "real";
        } else if (this.tipo == Type.COMPLEX) {
            return "complex";
        } else if (this.tipo == Type.CHARACTER) {
            return "caracter";
        } else if (this.tipo == Type.LOGICAL) {
            return "logico";
        }else if (this.tipo == Type.ARREGLO) {
            return "arreglo";
        }else if (this.tipo == Type.VOID) {
            return "void";
        }else if (this.tipo == Type.METODO) {
            return "metodo";
        }else if (this.tipo == Type.FUNCION) {
            return "funcion";
        }else if (this.tipo == Type.VARIABLE) {
            return "variable";
        }else if (this.tipo == Type.NULL) {
            return "Lista";
        }
        return "";
    }

}
