package entorno;

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {
    private HashMap<String, Symbol> Variables;
    private SymbolTable anterior;

    public SymbolTable(SymbolTable anterior) {
        this.anterior = anterior;
        Variables = new HashMap<String, Symbol>();
    }

    public void setVariable(Symbol symbol){
        this.Variables.put(symbol.id, symbol);
    }

    public Symbol getVariable(String id){
        SymbolTable entorno = this;
        while (entorno != null){

            for (Map.Entry<String, Symbol> entry : entorno.Variables.entrySet()) {
                if(entry.getKey().equalsIgnoreCase(id)){
                    return entorno.Variables.get(id);
                }
            }
            entorno = entorno.anterior;
        }
        return null;
    }

    /**
     * METODOS NO IMPLEMENTADOS
     */
    /*
    public boolean addSymbol(String name, Symbol newSymbol) {

        if (Variables.containsKey(name.toLowerCase())) {
            //Add list error
            return false;
        }

        Variables.put(name.toLowerCase(), newSymbol);

        return true;
    }

    public Symbol getSymbol(String name) {

        SymbolTable currentSymbolTable = this;

        while (currentSymbolTable != null) {
            if (currentSymbolTable.getSymbolTable().containsKey(name)) {
                return currentSymbolTable.getSymbolTable().get(name);
            }

            currentSymbolTable = currentSymbolTable.getPrev();
        }
        return null;
    }
    */

}