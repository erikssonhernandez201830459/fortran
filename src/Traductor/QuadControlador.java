package Traductor;

import entorno.SymbolTable;
import entorno.Tree;

import java.util.ArrayList;

public class QuadControlador {
    public ArrayList<Quadrupla> quads; // ARREGLO DE QUADRUPLAS
    public int labels;         //ETIQUETAS
    public int temps;          // TEMPORALES
    public ArrayList<String> codigo3D;
    public Tree tree;
    public SymbolTable actual;


    public QuadControlador(Tree tree) {
        this.quads = new ArrayList<Quadrupla>();
        this.labels = 0;
        this.temps = 0;
        this.codigo3D = new ArrayList<String>();;
        this.tree = tree;
        this.actual = new SymbolTable(null);
    }

    /**
     * METODO PARA AUMENTAR LOS LABELS EXISTENTES
     * @return
     */
    public String getLabel() {
        return "L"+this.labels++;
    }

    /**
     * METODO PARA AUMENTAR LOS TEMPORALES EXISTENTES
     * @return
     */
    public String getTemps() {
        return "t"+this.temps++;
    }

    public void addQuad(Quadrupla quad){
        this.quads.add(quad);
    }
}
