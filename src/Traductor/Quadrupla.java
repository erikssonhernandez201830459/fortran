package Traductor;

public class Quadrupla {
    public String operacion;
    public String arg1;
    public String arg2;
    public String resultado;

    public Quadrupla(String operacion, String arg1, String arg2, String resultado) {
        this.operacion = operacion;
        this.arg1 = arg1;
        this.arg2 = arg2;
        this.resultado = resultado;
    }
    @Override
    public String toString(){
        return "operacion: "+this.operacion +", arg1: "+this.arg1+", arg2: "+ this.arg2+ ", resultado: "+this.resultado;
    }
}
