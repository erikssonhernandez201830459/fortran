package Traductor;

public class Node {
    private String id;
    private String type;
    private String value;
    private int position;

    public Node(String id, String type, String value) {
        this.id = id;
        this.type = type;
        this.value = value;
        this.position = 0;
    }
    public Node (){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
