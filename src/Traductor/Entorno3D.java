package Traductor;

import entorno.Symbol;

import java.util.HashMap;

public class Entorno3D {
    public HashMap<String,Node> symbolTable;
    public Entorno3D father;
    private int variablePosition = 0;

    public Entorno3D(Entorno3D father){
        this.father = father;
        this.variablePosition= (father != null)? father.variablePosition: 0;
        this.symbolTable = new HashMap<String,Node>();
    }
    public int getNewVariablePosition(){
        return ++variablePosition;
    }

    public int newElement(String name, Node node){
        if (symbolTable.containsKey(name.toUpperCase()) && (searchElementByName(name)).getPosition() > 0){
            //if contains, means variable already declared
            System.out.println("La variable " + name + " ya existe.");
            return -1;
        } else{
            //position
            node.setPosition(getNewVariablePosition());//get position
            symbolTable.put(name.toUpperCase(), node);
            System.out.println("Se agrega " + (name + "= "+ node.getValue() +"<" + node.getType()).toUpperCase() + "> a la tabla de simbolos");
            return node.getPosition();//return all position
        }
    }

    public Node searchElementByName(String name){
        for (Entorno3D ent = this; ent != null; ent = ent.father)
        {
            if (ent.symbolTable.containsKey((name).toUpperCase()))
                return ent.symbolTable.get((name).toUpperCase());
        }
        return null;
    }
}
