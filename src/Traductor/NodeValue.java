package Traductor;

import entorno.Tipo;

public class NodeValue {
    public Tipo.Type type;
    public String value;

    public NodeValue(Tipo.Type type, String value) {
        this.type = type;
        this.value = value;
        System.out.println("NODO - Type: "+type + ", Value: "+value);
    }

    public Tipo.Type getType() {
        return type;
    }

    public void setType(Tipo.Type  type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
