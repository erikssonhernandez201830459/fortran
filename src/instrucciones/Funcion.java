package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Funcion extends Instruccion {

    String id1;
    String id2;
    String id3;
    ArrayList<String> listaParams1;
    ArrayList<Instruccion> listaParams2;
    ArrayList<Instruccion> retorno;
    ArrayList<Instruccion> instrucciones;

    public Funcion(String id1, String id2, String id3, ArrayList<String> listaParams1, ArrayList<Instruccion> listaParams2, ArrayList<Instruccion> instrucciones, int line, int column) {
        super(null, line, column);
        this.id1 = id1;
        this.id2 = id2;
        this.id3 = id3;
        this.listaParams1 = listaParams1;
        this.listaParams2 = listaParams2;
        this.instrucciones = instrucciones;
        this.retorno = new ArrayList<>();
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VALIDACIONES GENERALES
         */
        if(!id1.equalsIgnoreCase(id2)){
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Los nombres de apertura y cierre deben ser iguales ",
                    this.line, this.column);
           // tree.consola.add(error.toString());
            tree.excepciones.add(error);
            return error;
        }

        if(this.listaParams1.size() != this.listaParams2.size()){
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Los parametros estan incompletos ",
                    this.line, this.column);
           // tree.consola.add(error.toString());
            tree.excepciones.add(error);
            return error;
        }

        if (tree.getFuncion(this.id1) == null) {
            this.type = Tipo.Type.FUNCION;

            for(int i = 0; i<listaParams2.size(); i++){
                java.lang.Object decla = listaParams2.get(i);
                if(decla instanceof Declaracion dec){
                    if(!dec.id.equalsIgnoreCase(listaParams1.get(i))){
                        String descError = "Los parametros deben tener el mismo nombre ["+dec.id+"] ["+listaParams1.get(i)+"] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                       // tree.consola.add(error.toString());
                        tree.excepciones.add(error);
                        return error;
                    }
                }else if(decla instanceof DeclaracionArray dec){
                    if(!dec.id.equalsIgnoreCase(listaParams1.get(i))){
                        String descError = "Los parametros deben tener el mismo nombre ["+dec.id+"] ["+listaParams1.get(i)+"] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                       // tree.consola.add(error.toString());
                        tree.excepciones.add(error);
                        return error;
                    }
                }
            }

            boolean exist = false;
            boolean listaD = false;
            int elim = 0;
            int elimL = 0;
            for(int i = 0; i<instrucciones.size(); i++){
                elim = i;
                if(instrucciones.get(i) instanceof ListaDeclaraciones lisdec){
                    for(int j = 0; j<lisdec.declaraciones.size(); j++){
                        elimL = j;
                        if(lisdec.declaraciones.get(j) instanceof Declaracion dec){
                            if(this.id3.equalsIgnoreCase(dec.id)){
                                listaD = true;
                                break;
                            }
                        }
                    }
                    if(listaD){break;}
                }else if(instrucciones.get(i) instanceof DeclaracionArray decA){
                    if(this.id3.equalsIgnoreCase(decA.id)){
                        exist = true;
                        break;
                    }
                }
            }

            if(exist){
                Instruccion eli = instrucciones.remove(elim);
                this.type = eli.type;
                retorno.add(eli);
            }else if(listaD){
                ListaDeclaraciones eli = (ListaDeclaraciones) instrucciones.get(elim);
                if(eli.declaraciones.size() == 1){
                    retorno.add(eli.declaraciones.remove(elimL));
                    instrucciones.remove(elim);
                }else{
                    retorno.add(eli.declaraciones.remove(elimL));
                }
            }else{
                String descError = "La variable de retorno no ha sido encontrada ["+this.id3+"] ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
               // tree.consola.add(error.toString());
                tree.excepciones.add(error);
                return error;
            }

            ArrayList<ArrayList<Instruccion>> metodoValor = new ArrayList<>();

            metodoValor.add(listaParams2);
            metodoValor.add(instrucciones);
            metodoValor.add(retorno);

            Symbol metodo = new Symbol(new Tipo(this.type), new Tipo(Tipo.Type.FUNCION), this.id1, metodoValor, this.line, this.column, table);
            tree.setFuncion(metodo);
            return null;

        }else {
            /**
             * VALIDACION DE FUNCION CON EL MISMO NOMBRE YA CREADA EN LA TABLA DE SIMBOLOS
             */
            String descError = "La Funcion [" +this.id1+ "] ya ha sido creado con anterioridad";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
           // tree.consola.add(error.toString());
            return error;
        }
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("FUNCION");
        nodo.agregarHijo(new NodoAST(id1));
        if(listaParams1.size() != 0){
            NodoAST nodoParam = new NodoAST("PARAMETROS");
            for(String param: listaParams1){
                nodoParam.agregarHijo(param);
            }
            nodo.agregarHijo(nodoParam);
        }
        NodoAST nodoRetorno = new NodoAST("RETORNO");
        nodoRetorno.agregarHijo(new NodoAST(id3));
        nodo.agregarHijo(nodoRetorno);
        return nodo;
    }
    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
}
