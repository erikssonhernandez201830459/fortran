package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class DeclaracionArray extends Instruccion {
    String id;
    Instruccion dimension1;
    Instruccion dimension2;
    public java.lang.Object valor;

    NodoAST nodoPadre;
    public DeclaracionArray(Tipo.Type tipo, String id, Instruccion dimension1, Instruccion dimension2, int line, int column) {
        super(tipo, line, column);
        this.id = id;
        this.dimension1 = dimension1;
        this.dimension2 = dimension2;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol simbolo = table.getVariable(this.id);

        /**
         * VALIDACIONES GENERALES
         */
        if (simbolo != null) {
            String descError = "La variable [" + this.id + "] ya ha sido declarada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            return error;
        }
        //Verificamos que venga una posicion
        if (this.dimension1 == null) {
            String descError = "Debe enviar una dimension del arreglo valida ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        java.lang.Object dm1 = this.dimension1.execute(table, tree);
        if (dm1 instanceof Excepcion) {
            return dm1;
        }
        /**
         * VALIDACION ARREGLO DE 1 DIMENSION
         */
        if (this.dimension2 == null) {
            if (dm1 instanceof Integer d1) {

                //ERROR
                if (d1 <= 0) {
                    String descError = "Las dimensiones no pueden ser menores a 1 ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }

                //Creamos una lista con los valores por defecto para la variable y se los asignamos por medio del For
                ArrayList<Instruccion> result = new ArrayList<Instruccion>();
                for (int i = 0; i < (int) dm1; i++) {
                    result.add(Default.value(this.type, this.line, this.column));
                }

                /**
                 * CREAMOS LA NUEVA VARIABLE DE TIPO ARRAY 1 DIMENSION
                 */
                simbolo = new Symbol(new Tipo(this.type), new Tipo(Tipo.Type.ARREGLO), this.id, result, this.line, this.column, table);
                //Creamos el nodo para el AST
                nodoPadre = new NodoAST("DECLARACION");
                NodoAST nodoId = new NodoAST("ID");
                nodoId.agregarHijo(new NodoAST(this.type.toString()));
                nodoId.agregarHijo(new NodoAST(this.id));
                nodoPadre.agregarHijo(nodoId);
                NodoAST nodoDimension = new NodoAST("DIMENSIONES");
                nodoDimension.agregarHijo(this.dimension1.getAST());
                nodoPadre.agregarHijo(nodoDimension);

                //Agregamos la variable a la tabla de simbolos
                table.setVariable(simbolo);
                tree.variables.add(simbolo);

                //Terminamos la ejecucion
                return null;

            }
            //ERROR
            else {
                String descError = "El arreglo no puede ser declarado porque la dimension no es INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                return error;
            }
        }
        /**
         * VALIDACION PARA ARREGLOS DE 2 DIMENSIONES
         */
        else{
            if(dm1 instanceof Integer d1){

                java.lang.Object dm2 = this.dimension2.execute(table, tree);
                if (dm2 instanceof Excepcion) {
                    return dm2;
                }

                if(dm2 instanceof Integer d2){

                    //ERROR
                    if(d1 <= 0 || d2 <= 0){
                        String descError = "Las dimensiones no pueden ser menores a 1 ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                        tree.excepciones.add(error);
                        return error;
                    }

                    //Creamos la lista para asignar a la variable
                    ArrayList<ArrayList<Instruccion>> resI = new ArrayList<ArrayList<Instruccion>>();

                    for(int i = 0; i < d1; i++){

                        //Creamos una lista de nodos
                        ArrayList<Instruccion> resJ = new ArrayList<Instruccion>();

                        for(int j = 0; j < d2; j++){
                            resJ.add(Default.value(this.type, this.line, this.column));
                        }

                        //Agregamos la lista de nodos a la lista de listas
                        resI.add(resJ);
                    }

                    //Creamos la nueva variable tipo ARRAY
                    simbolo = new Symbol(new Tipo(this.type), new Tipo(Tipo.Type.ARREGLO2), this.id, resI, this.line, this.column, table);

                    //Creamos el nodo para el AST
                    nodoPadre =  new NodoAST("DECLARACION");
                    NodoAST nodoId =  new NodoAST("ID");
                    nodoId.agregarHijo(new NodoAST(this.type.toString()));
                    nodoId.agregarHijo(new NodoAST(this.id));
                    nodoPadre.agregarHijo(nodoId);
                    NodoAST nodoDimension =  new NodoAST("DIMENSIONES");
                    nodoDimension.agregarHijo(this.dimension1.getAST());
                    nodoDimension.agregarHijo(this.dimension2.getAST());
                    nodoPadre.agregarHijo(nodoDimension);

                    //Agregamos la variable a la tabla de simbolos
                    table.setVariable(simbolo);

                    //Terminamos la ejecucion
                    return null;
                }

                //ERROR
                else{
                    String descError = "El arreglo no puede ser declarado porque la dimension no es INTEGER ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }
            }

            //ERROR
            else{
                String descError = "El arreglo no puede ser declarado porque la dimension no es INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                return error;
            }
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo =  new NodoAST("DECLARACION");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
