package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;
import expresiones.Primitivo;

import java.util.ArrayList;

public class AsignacionArray extends Instruccion {
    String id;
    Instruccion valor;
    //Object resultado;
    Instruccion posicion1;
    Instruccion posicion2;

    NodoAST nodoPadre;


    public AsignacionArray(String id, Instruccion posicion1, Instruccion posicion2, Instruccion valor, int line, int column) {
        super(null, line, column);
        this.id = id;
        this.posicion1 = posicion1;
        this.posicion2 = posicion2;
        this.valor = valor;
    }

    @Override
    public Object execute(SymbolTable table, Tree tree) {

        Symbol variable = table.getVariable(this.id);

        /**
         * VALIDACION DE EXCEPCIONES
         */

        if (variable == null) {
            String descError = "La variable [" + this.id + "] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        if (this.posicion1 == null) {
            String descError = "La posicion no es valida";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        Object po1 = this.posicion1.execute(table, tree);
        if (po1 instanceof Excepcion) {
            return po1;
        }

        //ERROR
        if (this.valor == null) {
            String descError = "La expresion no es valida";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        Object result = this.valor.execute(table, tree);
        if (result instanceof Excepcion) {
            return result;
        }

        /**
         * VALIDACION DE TIPOS
         */
        if (valor.type != variable.tipo.tipo) {
            String descError = "La variable [" + this.id + "] no puede ser asignada debido a que son de diferentes tipos (" + variable.tipo.tipo + ") y (" + this.valor.type + ") ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        /**
         * VALIDACION DE VARIABLES DE TIPO ARRAY
         */
        if (variable.tipo2.tipo == Tipo.Type.ARREGLO || variable.tipo2.tipo == Tipo.Type.ALLOCATE) {

            if (po1 instanceof Integer p1) {

                //ERROR
                if (this.posicion2 != null) {
                    String descError = "La variable es unicamente de una dimension ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }

                //Verificamos que la variable sea del tipo lista de Nodos
                if (variable.valor instanceof ArrayList<?> listaI) {

                    if (p1 <= listaI.size() && p1 > 0) {
                        //Creamos el nuevo valor a agregar
                        Primitivo prim = new Primitivo(this.valor.type, result, this.line, this.column);
                        ((ArrayList<Instruccion>) variable.valor).set(p1 - 1, prim);

                        //Creamos el nodo para el AST
                        nodoPadre = new NodoAST("ASIGNACION");

                        NodoAST nodoId = new NodoAST("ID");
                        nodoId.agregarHijo(this.id);

                        NodoAST nodoPos = new NodoAST("POSICION");
                        nodoPos.agregarHijo(this.posicion1.getAST());
                        nodoId.agregarHijo(nodoPos);

                        nodoPadre.agregarHijo(nodoId);
                        nodoPadre.agregarHijo(this.valor.getAST());

                        //Salimos de la ejecucion
                        return null;
                    }

                    //ERROR
                    else {
                        String descError = "El arreglo no puede ser asignado porque la posicion no es la adecuada ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        return error;
                    }
                }

                //ERROR
                else {
                    String descError = "La variable no posee un arreglo ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }

            }

            //ERROR
            else {
                String descError = "La posicion deben ser INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }


            /**
             * VALIDACION DE VARIABLES DE TIPO ARRAY DE 2 DIMENSIONES
             */

        } else if (variable.tipo2.tipo == Tipo.Type.ARREGLO2 || variable.tipo2.tipo == Tipo.Type.ALLOCATE2) {
            if (po1 instanceof Integer p1) {

                //ERROR
                if (this.posicion2 == null) {
                    String descError = "La variable requiere 2 posiciones para ser asignada ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }

                Object po2 = this.posicion2.execute(table, tree);
                if (po2 instanceof Excepcion) {
                    return po2;
                }

                if (po2 instanceof Integer p2) {

                    //Verificamos que la variable sea del tipo lista de Nodos
                    if (variable.valor instanceof ArrayList<?> listaI) {

                        //Verificamos que el size sea el adecuado
                        if ((p1 <= listaI.size()) && p1 > 0) {

                            if (listaI.get(0) instanceof ArrayList<?> listaJ) {

                                //Obtengo la lista de listas
                                ArrayList<ArrayList<Instruccion>> listI = (ArrayList<ArrayList<Instruccion>>) variable.valor;
                                ArrayList<Instruccion> listJ = listI.get(p1 - 1);


                                if (p2 <= listJ.size() && p2 > 0) {
                                    //Creamos el valor a asignar
                                    Primitivo prim = new Primitivo(this.valor.type, result, this.line, this.column);
                                    listJ.set(p2 - 1, prim);
                                    ((ArrayList<ArrayList<Instruccion>>) variable.valor).set(p1 - 1, listJ);

                                    //Creamos el nodo para el AST
                                    nodoPadre = new NodoAST("ASIGNACION");

                                    NodoAST nodoId = new NodoAST("ID");
                                    nodoId.agregarHijo(this.id);

                                    NodoAST nodoPos = new NodoAST("POSICION");
                                    nodoPos.agregarHijo(this.posicion1.getAST());
                                    nodoPos.agregarHijo(this.posicion2.getAST());
                                    nodoId.agregarHijo(nodoPos);

                                    nodoPadre.agregarHijo(nodoId);
                                    nodoPadre.agregarHijo(this.valor.getAST());

                                    //Salimos de la ejecucion
                                    return null;
                                }

                                //ERROR
                                else {
                                    String descError = "El arreglo no puede ser asignado porque la posicion no es la adecuada ";
                                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                                    tree.excepciones.add(error);
                                    return error;
                                }
                            }

                            //ERROR
                            else {
                                String descError = "La variable no posee un arreglo ";
                                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                                tree.excepciones.add(error);
                                return error;
                            }
                        }

                        //ERROR
                        else {
                            String descError = "El arreglo no puede ser asignado porque la posicion no es la adecuada ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                            tree.excepciones.add(error);
                            return error;
                        }
                    }

                    //ERROR
                    else {
                        String descError = "La variable no posee un arreglo ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                        tree.excepciones.add(error);
                        return error;
                    }
                }

                //ERROR
                else {
                    String descError = "La posicion deben ser INTEGER ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }
            }

            //ERROR
            else {
                String descError = "La posicion deben ser INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }
        }

        //ERROR
        else {
            String descError = "La variable [" + variable.id + "] no es de un tipo valido para asignar ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ASIGNACION");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
