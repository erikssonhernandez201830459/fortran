package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;
import expresiones.Identificador;
import expresiones.Primitivo;

import java.util.ArrayList;

public class LlamadaSubRutina extends Instruccion {

    String id;
    ArrayList<Instruccion> parametrosEnt;
    ArrayList<NodoAST> parametrosEjec;
    ArrayList<NodoAST> instruccionEjec;

    public LlamadaSubRutina(String id, ArrayList<Instruccion> parametrosEnt, int line, int column) {
        super(null, line, column);
        this.id = id;
        this.parametrosEnt = parametrosEnt;
        parametrosEjec = new ArrayList<>();
        instruccionEjec = new ArrayList<>();
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * CREACION DE UN NUEVO ENTORNO Y DE UN NUEVO SIMBOLO SUBRUTINA O METODO
         */
        SymbolTable newTable = new SymbolTable(null);

        Symbol simboloMetodo = tree.getMetodo(this.id);
        /**
         * VALIDACIONES GENERALES
         */
        if (simboloMetodo == null) {
            String descError = "La subrutina ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        this.type = simboloMetodo.tipo.tipo;
        /**
         * VALIDACION DE PARAMETROS
         */
        ArrayList<Instruccion> parametros = ((ArrayList<ArrayList<Instruccion>>)simboloMetodo.valor).get(0);

        if(this.parametrosEnt.size() != parametros.size()){
            String descError = "Los parametros de entrada y parametros declarados no son los mismos ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        for (int i = 0; i < parametros.size(); i++) {
            if(parametros.get(i) instanceof Declaracion dec){
                /**
                 * VALIDACION DE DECLARACION DE PARAMETROS
                 * DENTRO DEL ENTORNO DE LA FUNCION
                 */
                if(this.parametrosEnt.get(i) instanceof Identificador ide){
                    ide.execute(table, tree);
                    if(ide.type != dec.type){
                        String descError = "La variable [" +dec.id+ "] no puede ser declarada debido a que son de diferentes tipos [" +dec.type+"] y [" +ide.type + "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, dec.line, dec.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                    dec.valor = new Primitivo(ide.type, ide.execute(table, tree), dec.line, dec.column);
                    dec.execute(newTable, tree);

                    //Agregamos los nodos para el AST
                    parametrosEjec.add(ide.getAST());
                    instruccionEjec.add(dec.getAST());
                }else {
                    this.parametrosEnt.get(i).execute(table, tree);
                    if(this.parametrosEnt.get(i).type != dec.type){
                        String descError = "La variable [" +dec.id+ "] no puede ser declarada debido a que son de diferentes tipos [" +dec.type+"] y [" +this.parametrosEnt.get(i).type+ "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, dec.line, dec.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                    dec.valor = this.parametrosEnt.get(i);
                    dec.execute(newTable, tree);

                    //Agregamos los nodos para el AST
                    parametrosEjec.add(this.parametrosEnt.get(i).getAST());
                    instruccionEjec.add(dec.getAST());
                }
                /**
                 * VALIDACIONES DE LOS PARAMETROS DE TIPO ARRAY DENTRO DE LA FUNCION
                 */
            }else if(parametros.get(i) instanceof DeclaracionArray decArray) {
                java.lang.Object dimension1 = decArray.dimension1.execute(newTable, tree);

                if(this.parametrosEnt.get(i) instanceof Identificador ide){
                    java.lang.Object result = ide.execute(table, tree);

                    if(ide.type != decArray.type){
                        String descError = "El arreglo [" +decArray.id+ "] no puede ser declarada debido a que son de diferentes tipos ["+decArray.type+"] y [" +ide.type + "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }

                    if(decArray.dimension2 == null){
                        if((int)dimension1 != ((ArrayList<Instruccion>)result).size()){
                            String descError = "El arreglo [" +decArray.id+ "] no puede ser declarado debido a que son de diferentes dimensiones ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }

                        decArray.execute(newTable, tree);
                        Asignacion newA = new Asignacion(decArray.id, result, decArray.line, decArray.column);
                        newA.execute(newTable, tree);

                        //Agregamos los nodos para el AST
                        parametrosEjec.add(ide.getAST());
                        instruccionEjec.add(decArray.getAST());
                        instruccionEjec.add(newA.getAST());

                    }else{
                        java.lang.Object dimension2 = decArray.dimension2.execute(newTable, tree);

                        ArrayList<ArrayList<Instruccion>> listI = (ArrayList<ArrayList<Instruccion>>) result;
                        ArrayList<Instruccion> listJ = listI.get(0);

                        if((int)dimension1 != listI.size() || (int)dimension2 != listJ.size()){
                            String descError = "El arreglo [" +decArray.id+ "] no puede ser declarado debido a que son de diferentes dimensiones ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }

                        decArray.execute(newTable, tree);
                        Symbol variable = newTable.getVariable(decArray.id);
                        variable.valor = listI;

                        parametrosEjec.add(ide.getAST());
                        instruccionEjec.add(decArray.getAST());
                    }
                }
            }
        }
        /**
         * VALIDACION DE SUBRUTINA, YA QUE ESTA NO DEBE RETORNAR NINGUN VALOR
         */
        ArrayList<Instruccion> instrucciones = ((ArrayList<ArrayList<Instruccion>>)simboloMetodo.valor).get(1);

        for (int i = 0; i < instrucciones.size(); i++) {
            java.lang.Object res = instrucciones.get(i).execute(newTable, tree);
            //ERROR
            if(res instanceof Excepcion e){
                tree.consola.add(e.toString());
            }
            if (res instanceof Return re) {
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "No se esperaba un retorno en esta subrutina", re.line, re.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
            instruccionEjec.add(instrucciones.get(i).getAST());
        }

        return null;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("CALL");
        nodo.agregarHijo(new NodoAST(this.id));


        if (this.parametrosEnt.size() != 0) {
            NodoAST nodo2 = new NodoAST("PARAMETROS");
            nodo2.agregarHijos(parametrosEjec);
            nodo.agregarHijo(nodo2);
        }

        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
        nodoIns.agregarHijos(instruccionEjec);

        nodo.agregarHijo(nodoIns);
        return nodo;
    }
    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
}
