package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Deallocate extends Instruccion {

    String id;
    
    NodoAST nodoPadre;
    public Deallocate(String id, int line, int column) {
        super(null, line, column);
        this.id = id;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol simbolo = table.getVariable(this.id);
        /**
         * VALIDACIONES GENERALES
         */
        if (simbolo == null) {
            String descError = "La variable [" + this.id + "] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
        }

        /**
         * VALIDACIONES PARA QUE LA VARIABLE SEA DE TIPO ARRAY
         * DE 1 DIMENSION PARA USAR ALLOCATE
         *
         */
        if (simbolo.tipo2.tipo == Tipo.Type.ALLOCATE) {

            //Verificamos que la variable posea una lista
            if(simbolo.valor instanceof ArrayList<?> listaInstrucciones){

                //ERROR
                if(listaInstrucciones.size() == 0){
                    String descError =  "La variable {" + this.id + "} no posee dimension, no puede usar Deallocate de nuevo ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }

                //Le asignamos una lista vacia a la variable
                simbolo.valor = new ArrayList<Instruccion>();

                //Creamos el nodo para el AST
                nodoPadre = new NodoAST("DEALLOCATE");
                nodoPadre.agregarHijo(new NodoAST(this.id));

                //Terminamos la Ejecucion
                return null;
            }

            //ERROR
            else{
                String descError =  "El arreglo no posee un ArrayList ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }

        }

        /**
         * VALIDACIONES PARA QUE LA VARIABLE SEA UN ARRAY DE 2 DIMENSIONES
         * Y APLICAR EL ALLOCATE
         */
        else if(simbolo.tipo2.tipo == Tipo.Type.ALLOCATE2){
            //Verificamos que la variable posea una lista
            if(simbolo.valor instanceof ArrayList<?> listaInstrucciones){

                //ERROR
                if(listaInstrucciones.size() == 0){
                    String descError =  "La variable {" + this.id + "} no posee dimensiones, no puede usar Deallocate de nuevo ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }

                //Le asignamos una lista vacia a la variable
                simbolo.valor = new ArrayList<ArrayList<Instruccion>>();

                //Creamos el nodo para el AST
                nodoPadre = new NodoAST("DEALLOCATE");
                nodoPadre.agregarHijo(new NodoAST(this.id));

                //Terminamos la Ejecucion
                return null;
            }

            //ERROR
            else{
                String descError =  "El arreglo no posee un ArrayList ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }
        }
    
        else{
                String descError = "El arreglo no es del tipo Allocatable ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
       
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo =  new NodoAST("DEALLOCATE");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
