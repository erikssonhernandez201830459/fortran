package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

public class Return extends Instruccion {
    Instruccion expresion;

    NodoAST nodoPadre;
    public Return(Instruccion expresion, int line, int column) {
        super(null, line, column);
        this.expresion = expresion;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VALIDACIONES GENERALES
         */
        if(this.expresion == null){
            String descError = "La expresion no es valida";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }
        /**
         * EJECUCION Y RETORNO DE LA EXPRESION
         */
        java.lang.Object expresion = this.expresion.execute(table, tree);
        if (expresion instanceof Excepcion) {
            return expresion;
        }

        //Creamos el nodo Main AST
        nodoPadre = new NodoAST("RETURN");
        nodoPadre.agregarHijo(this.expresion.getAST());

        return expresion;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        if(this.expresion == null){
            return null;
        }
        /**
         * ACA DEBE IR EL CODIGO DE 3D PARA EL RETURN DE EXPRESIONES
         */
        return null;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("RETURN");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
