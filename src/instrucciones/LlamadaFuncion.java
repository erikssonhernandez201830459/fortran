package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;
import expresiones.Identificador;
import expresiones.Primitivo;

import java.util.ArrayList;

public class LlamadaFuncion extends Instruccion {
    String id;
    ArrayList<Instruccion> parametrosEnt;

    ArrayList<NodoAST> parametrosEjec = new ArrayList<>();
    ArrayList<NodoAST> instruccionEjec = new ArrayList<>();
    NodoAST nodoRetorno = new NodoAST("RETORNO");

    public LlamadaFuncion(String id, ArrayList<Instruccion> parametrosEnt, int line, int column) {
        super(null, line, column);
        this.id = id;
        this.parametrosEnt = parametrosEnt;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * CREACION DE UN NUEVO ENTORNO Y DE UN NUEVO SIMBOLO FUNCION
         */
        SymbolTable newTable = new SymbolTable(null);

        Symbol simboloMetodo = tree.getFuncion(this.id);

        /**
         * VALIDACIONES GENERALES
         */
        if (simboloMetodo == null) {
            String descError = "La Funcion ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }


        this.type = ((ArrayList<ArrayList<Instruccion>>)simboloMetodo.valor).get(2).get(0).type;

        ArrayList<Instruccion> parametros = ((ArrayList<ArrayList<Instruccion>>)simboloMetodo.valor).get(0);

        /**
         * VALIDACION DE PARAMETROS
         */
        if(this.parametrosEnt.size() != parametros.size()){
            String descError = "Los parametros de entrada y parametros declarados no son los mismos ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        for (int i = 0; i < parametros.size(); i++) {
            if(parametros.get(i) instanceof Declaracion dec){
                /**
                 * VALIDACION DE DECLARACION DE PARAMETROS
                 * DENTRO DEL ENTORNO DE LA FUNCION
                 */
                if(this.parametrosEnt.get(i) instanceof Identificador ide){
                    ide.execute(table, tree);
                    if(ide.type != dec.type){
                        String descError = "La variable [" +dec.id+ "] no puede ser declarada debido a que son de diferentes tipos [" +dec.type+"] y [" +ide.type + "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, dec.line, dec.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                    dec.valor = new Primitivo(ide.type, ide.execute(table, tree), dec.line, dec.column);
                    dec.execute(newTable, tree);

                    //Agregamos los Nodos para el AST
                    parametrosEjec.add(ide.getAST());
                    instruccionEjec.add(dec.getAST());
                }else {
                    this.parametrosEnt.get(i).execute(table, tree);
                    if(this.parametrosEnt.get(i).type != dec.type){
                        String descError = "La variable [" +dec.id+ "] no puede ser declarada debido a que son de diferentes tipos [" +dec.type+"] y [" +this.parametrosEnt.get(i).type+ "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, dec.line, dec.column);
                        tree.excepciones.add(error);
                        //tree.consola.add(error.toString());
                        return error;
                    }
                    dec.valor = this.parametrosEnt.get(i);
                    dec.execute(newTable, tree);

                    //Agregamos los Nodos para el AST
                    parametrosEjec.add(this.parametrosEnt.get(i).getAST());
                    instruccionEjec.add(dec.getAST());
                }
                /**
                 * VALIDACIONES DE LOS PARAMETROS DE TIPO ARRAY DENTRO DE LA FUNCION
                 */
            }else if(parametros.get(i) instanceof DeclaracionArray decArray) {
                java.lang.Object dimension1 = decArray.dimension1.execute(newTable, tree);


                if(this.parametrosEnt.get(i) instanceof Identificador ide){
                    java.lang.Object result = ide.execute(table, tree);

                    if(decArray.dimension2 == null){
                        if(ide.type != decArray.type){
                            String descError = "El arreglo [" +decArray.id+ "] no puede ser declarada debido a que son de diferentes tipos "+ decArray.type +" y [" +ide.type + "] ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }
                        if((int)dimension1 != ((ArrayList<Instruccion>)result).size()){
                            String descError = "El arreglo [" +decArray.id+ "] no puede ser declarado debido a que son de diferentes dimensiones ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }

                        decArray.execute(newTable, tree);
                        Asignacion newA = new Asignacion(decArray.id, result, decArray.line, decArray.column);
                        newA.execute(newTable, tree);

                        //Agregamos los Nodos para el AST
                        parametrosEjec.add(ide.getAST());
                        instruccionEjec.add(decArray.getAST());
                        instruccionEjec.add(newA.getAST());
                    }else{
                        java.lang.Object dimension2 = decArray.dimension2.execute(newTable, tree);
                        if(ide.type != decArray.type){
                            String descError = "El arreglo [" +decArray.id+ "] no puede ser declarada debido a que son de diferentes tipos "+ decArray.type +" y [" +ide.type + "] ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }

                        ArrayList<ArrayList<Instruccion>> listI = (ArrayList<ArrayList<Instruccion>>) result;
                        ArrayList<Instruccion> listJ = listI.get(0);

                        if((int)dimension1 != listI.size() || (int)dimension2 != listJ.size()){
                            String descError = "El arreglo [" +decArray.id+ "] no puede ser declarado debido a que son de diferentes dimensiones ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, decArray.line, decArray.column);
                            tree.excepciones.add(error);
                            //tree.consola.add(error.toString());
                            return error;
                        }

                        decArray.execute(newTable, tree);
                        Symbol variable = newTable.getVariable(decArray.id);
                        variable.valor = listI;

                        //Agregamos los nodos para el AST
                        parametrosEjec.add(ide.getAST());
                        instruccionEjec.add(decArray.getAST());
                    }
                }
            }
        }
        /**
         * EJECUCION DE LAS INSTRUCCIONES, DECLARACION Y EJECUCION DE LA VARIABLE DE RETORNO
         * PARA TIPO VARIABLE Y ARRAY
         */
        ArrayList<Instruccion> instrucciones = ((ArrayList<ArrayList<Instruccion>>)simboloMetodo.valor).get(1);
        Instruccion declaRet = ((ArrayList<ArrayList<Instruccion>>)simboloMetodo.valor).get(2).get(0);
        declaRet.execute(newTable, tree);

        //Agregamos el Nodo para el AST
        instruccionEjec.add(declaRet.getAST());

        for (int i = 0; i < instrucciones.size(); i++) {
            java.lang.Object res = instrucciones.get(i).execute(newTable, tree);
            if(res instanceof Excepcion e){
                tree.consola.add(e.toString());
            }
            //Agregamos el Nodo para el AST
            instruccionEjec.add(instrucciones.get(i).getAST());
        }

        Identificador identificadorFuncion = new Identificador("", declaRet.line, declaRet.column);

        if(declaRet instanceof Declaracion dec){
            identificadorFuncion = new Identificador(dec.id, dec.line, dec.column);
        }else if(declaRet instanceof DeclaracionArray decA){
            identificadorFuncion = new Identificador(decA.id, decA.line, decA.column);
        }

        Return retu = new Return(identificadorFuncion, identificadorFuncion.line, identificadorFuncion.column);
        java.lang.Object ret = retu.execute(newTable, tree);

        nodoRetorno.agregarHijo(retu.getAST());

        return ret;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("LLAMAR FUNCION");
        nodo.agregarHijo(new NodoAST(this.id));

        if (this.parametrosEnt.size() != 0) {
            NodoAST nodo2 = new NodoAST("PARAMETROS");
            nodo.agregarHijos(parametrosEjec);
            nodo.agregarHijo(nodo2);
        }

        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
        nodoIns.agregarHijos(instruccionEjec);

        nodo.agregarHijo(nodoIns);

        nodo.agregarHijo(nodoRetorno);
        return nodo;
    }
    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

}
