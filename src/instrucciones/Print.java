package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;
import expresiones.Identificador;
import expresiones.Primitivo;

import java.util.ArrayList;

public class Print extends Instruccion {
    ArrayList<Instruccion> expresion;

    ArrayList<NodoAST> nodoInstruccion = new ArrayList<>();
    public Print(ArrayList<Instruccion> expresion, int line, int column) {
        super(null, line, column);
        this.expresion = expresion;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VALIDACIONES GENERALES
         */
        for(int i = 0; i<this.expresion.size(); i++){
            /**
             * BUSQUEDA EN LA TABLA DE SIMBOLOS SI ES UN IDENTIFICADOR
             */
            if(this.expresion.get(i) instanceof Identificador iden){
                Symbol ide = table.getVariable(iden.id);
                java.lang.Object val = iden.execute(table, tree);

                //ERROR
                if(val instanceof Excepcion e){
                    return val;
                }

                nodoInstruccion.add(iden.getAST());
                if(ide == null){
                    String descError = "La variable ["+iden.id+"] no ha sido encontrada";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
                    tree.excepciones.add(error);
                    //tree.consola.add(error.toString());
                    return error;
                }
                /**
                 * VALIDACION DE TIPOS DE DATOS ARRAY DE 1 DIMENSION
                 */
                if(ide.tipo2.tipo == Tipo.Type.ARREGLO || ide.tipo2.tipo == Tipo.Type.ALLOCATE){
                    String cadena = "";

                    ArrayList<Instruccion> valor = (ArrayList<Instruccion>) ide.valor;
                    for(int j = 0; j<valor.size(); j++){
                        cadena += valor.get(j).execute(table, tree) + " ";
                    }

                    tree.consola.add(cadena);

                    /**
                     * VALIDACION DE TIPO DE DATO ARRAY DE 2 DIMENSIONES
                     */
                }else if(ide.tipo2.tipo == Tipo.Type.ARREGLO2 || ide.tipo2.tipo == Tipo.Type.ALLOCATE2){
                    String cadena = "";

                    ArrayList<ArrayList<Instruccion>> valorJ = (ArrayList<ArrayList<Instruccion>>) ide.valor;
                    for(int j = 0; j<valorJ.size(); j++){
                        ArrayList<Instruccion> valorK = valorJ.get(j);
                        for(int k = 0; k<valorK.size(); k++){
                            cadena += valorK.get(k).execute(table, tree) + " ";
                        }
                        cadena += "\n";
                    }

                    tree.consola.add(cadena);
                    /**
                     * VALIDACION DE TIPO DE DATO LOGICAL
                     * QUITO COMILLAS DEL PRINT, Y REEMPLAZO TRUE/FALSE POR T/F
                     */
                }else{
                    String salida = String.valueOf(this.expresion.get(i).execute(table, tree));
                    salida = salida.replace("\"", "");
                    salida = salida.replace("'", "");

                    if(salida.equalsIgnoreCase("true")){
                        salida = "T";
                    }else if(salida.equalsIgnoreCase("false")){
                        salida = "F";
                    }

                    tree.consola.add(salida);
                }
                /**
                 * QUITO COMILLAS DEL PRINT, Y REEMPLAZO TRUE/FALSE POR T/F
                 */
            }else {
                java.lang.Object val = this.expresion.get(i).execute(table, tree);
                if(val instanceof Excepcion){
                    return val;
                }

                String salida = String.valueOf(val);

                salida = salida.replace("\"", "");
                salida = salida.replace("'", "");

                if (salida.equalsIgnoreCase("true")) {
                    salida = "T";
                } else if (salida.equalsIgnoreCase("false")) {
                    salida = "F";
                }

                tree.consola.add(salida);

                //Agregamos los nodos para el AST
                nodoInstruccion.add(this.expresion.get(i).getAST());
            }
        }
        tree.consola.add("\n");
        return null;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("PRINT");
        nodo.agregarHijos(nodoInstruccion);
        return nodo;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        for(int i = 0; i<this.expresion.size(); i++){
            Quadrupla quadTemp = expresion.get(i).translate(controlador);
            Object result = this.expresion.get(i).execute(controlador.actual, controlador.tree);
            String resultString = result.toString();
            System.out.println("QuadTemp"+quadTemp);
            if(result instanceof  Instruccion instruccion) {
                Tipo.Type typeResult = instruccion.type;
                System.out.println("Entro a la instancia de instruccion");

                //if(quadTemp) {
                    if (typeResult.equals(Tipo.Type.INTEGER) || typeResult.equals(Tipo.Type.REAL) || typeResult.equals(Tipo.Type.LOGICAL)) {
                        controlador.addQuad(new Quadrupla("PRINTF", "\"%f\"", "(double)"+quadTemp.resultado,""));
                    }
                    else if (type.equals(Tipo.Type.STRING) || type.equals(Tipo.Type.COMPLEX) || type.equals(Tipo.Type.CHARACTER)) {
                        /**
                         * PRINT NORMAL DE CADENAS Y CARACTERES PRIMITIVOS
                         */
                        if(expresion.get(i) instanceof Primitivo primitivo)
                        {
                            String temp1 = controlador.getTemps();
                            String temp2 = controlador.getTemps();
                            controlador.addQuad(new Quadrupla("ASSIGN", "H", "",temp1));
                            for (int x = 0; x < result.toString().length(); x++) {
                                //Se separa la cadena en caracteres y se asigna cada uno en el HEAP
                                controlador.addQuad(new Quadrupla("ASSIGN", String.valueOf(resultString.charAt(i)), "", controlador.tree.HEAP + "[(int)H]"));

                                //Sumamos posiciones en el HEAP
                                controlador.addQuad(new Quadrupla("+", "H", "1", "H"));
                            }
                            //Si viene vacia la declaracion, solamente declaramos la variable y la guardamos en el HEAP
                            controlador.addQuad(new Quadrupla("ASSIGN", "-1", "", controlador.tree.HEAP + "[(int)H]"));
                            //Sumamos una posicion al HEAP
                            controlador.addQuad(new Quadrupla("+", "H", "1", "H"));

                            //Guardamos la posicion del Primitivo en el Apuntador P
                            controlador.addQuad(new Quadrupla("+", "P", String.valueOf(controlador.tree.posiciones), temp2));
                            //Asignamos la variable en el STACK
                            controlador.addQuad(new Quadrupla("ASSIGN", temp1, "", controlador.tree.STACK + "[(int)" + temp2 + "]"));
                            //Guardamos la posicion del Primitivo Asignado en el STACK en el APuntador P
                            controlador.addQuad(new Quadrupla("+","P",String.valueOf(controlador.tree.posiciones),"P"));
                            boolean existe = false;
                            for (Quadrupla quadSearch: controlador.quads) {
                                if(quadSearch.arg1.equalsIgnoreCase("imprimir_cadena")){
                                    existe = true;
                                }
                            }
                            if(!existe){
                                controlador.addQuad(new Quadrupla("CALL", "imprimir_cadena",String.valueOf(controlador.temps),""));
                                controlador.temps +=2;
                            }else {
                                controlador.addQuad(new Quadrupla("CALL", "imprimir_cadena","-", ""));
                            }
                            controlador.addQuad(new Quadrupla("-","P",String.valueOf(controlador.tree.posiciones),"P"));

                        } else if(expresion.get(i) instanceof Identificador identificador)
                        {
                               Symbol simbolo = controlador.actual.getVariable(identificador.id);
                               controlador.addQuad(new Quadrupla("+","P", String.valueOf(simbolo.posicion),"P"));
                            boolean existe = false;
                            for (Quadrupla quadSearch: controlador.quads) {
                                if(quadSearch.arg1.equalsIgnoreCase("imprimir_cadena")){
                                    existe = true;
                                }
                            }
                            controlador.addQuad(new Quadrupla("CALL", "imprimir_cadena",String.valueOf(controlador.temps),""));
                            if(!existe){
                                controlador.temps +=2;
                            }
                            controlador.addQuad(new Quadrupla("-","P",String.valueOf(simbolo.posicion),"P"));


                        }
                    }

                }
            }
        //}
        /**
         * AL FINAL DE LA IMPRESION DE LA LISTA DE INSTRUCCIONES, SIEMRPRE SE DEBE HACER UN SALTO DE LINEA
         */
        controlador.addQuad(new Quadrupla("PRINTF","\"%c\"", "((char)10)",""));


        return null;
    }
}
