package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Declaracion extends Instruccion {
    String id;
    public Instruccion valor;

    NodoAST nodoPadre;

    /**
     * DECLARACION Y ASIGNACION DE VARIABLES SIMPLES
     * @param tipo
     * @param id
     * @param valor
     * @param line
     * @param column
     */
    public Declaracion(Tipo.Type tipo, String id, Instruccion valor, int line, int column) {
        super(tipo, line, column);
        this.id = id;
        this.valor = valor;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VALIDACION DE ERRORES GENERALES
         */
        if(this.valor == null){
            String descError = "La expresion no es valida ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        Object result = this.valor.execute(table, tree);

        if (result instanceof Excepcion) {
            return result;
        }
        /**
         * DECLARACION DE VARIABLE CON VALOR DE UN ARREGLO
         */
        //Verificamos que no sea una lista, osea Variable ≠ Arreglo
        if (result instanceof ArrayList<?>) {
            String descError = "No puede asignar un arreglo a una variable ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }
        /**
         * ERROR DE TIPO DE DATOS DIFERENTES
         * NO SE ADMITEN CASTEOS IMPLICITOS
         */

        if (this.valor.type != this.type) {
            String descError = "La variable [" +this.id+ "] no puede ser declarada debido a que son de diferentes Tipos (" +this.type+") y (" +valor.type + ") ";

            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        /**
         * CREAMOS EL NUEVO SIMBOLO
         */
        Symbol simbolo = new Symbol(new Tipo(this.type), new Tipo(Tipo.Type.VARIABLE), this.id, result, this.line, this.column, table);

        if(table.getVariable(this.id) == null){
            table.setVariable(simbolo);
            tree.variables.add(simbolo);

            //Creamos de nodoPadre AST
            nodoPadre = new NodoAST("DECLARACION");
            NodoAST nodoId = new NodoAST("ID");
            nodoId.agregarHijo(new NodoAST(type.toString()));
            nodoId.agregarHijo(new NodoAST(id));
            nodoPadre.agregarHijo(nodoId);
            nodoPadre.agregarHijo(this.valor.getAST());

            //Terminamos la ejecucion
            return null;
        }else{
            String descError = "La variable [" + this.id + "] ya ha sido declarada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {

        if(this.valor == null){
            return null;
        }
        Symbol variable = controlador.actual.getVariable(this.id);
        Tipo  type = variable.getTipo();
        Object result = this.valor.execute(controlador.actual, controlador.tree);
        String resultString = result.toString();
        //SI No viene una declaracion con expresion, entonces vaciamos el resultString
        if (result == null){
            resultString = "";
        }
        if(type.equals(Tipo.Type.STRING) || type.equals(Tipo.Type.COMPLEX) || type.equals(Tipo.Type.CHARACTER)){
            String temp1 = controlador.getTemps();
            String temp2 = controlador.getTemps();
            //Agregamos una Quadrupla de Asignacion
            controlador.addQuad(new Quadrupla("ASSIGN", "H", "", temp1));
            //VERIFICAMOS QUE NO VENGA VACIO Y LO RECORREMOS PARA REALIZAR LA ASIGNACION
            for (int i = 0; i < result.toString().length(); i++) {
                //Se separa la cadena en caracteres y se asigna cada uno en el HEAP
                controlador.addQuad(new Quadrupla("ASSIGN", String.valueOf(resultString.charAt(i)), "", controlador.tree.HEAP +"[(int)H]"));
                //Sumamos posiciones en el HEAP
                controlador.addQuad(new Quadrupla("+", "H", "1", "H"));
            }
            //Si viene vacia la declaracion, solamente declaramos la variable y la guardamos en el HEAP
            controlador.addQuad(new Quadrupla("ASSIGN", "-1", "", controlador.tree.HEAP +"[(int)H]"));
            //Sumamos una posicion al HEAP
            controlador.addQuad(new Quadrupla("+", "H", "1", "H"));

            //Guardamos la posicion de la variable en el Apuntador P
            controlador.addQuad(new Quadrupla("+", "P", String.valueOf(variable.posicion), temp2));
            //Asignamos la variable en el STACK
            controlador.addQuad(new Quadrupla("ASSIGN", temp1, "", controlador.tree.STACK+"[(int)" +temp2+"]"));
            System.out.println(controlador);
        } else if(type.equals(Tipo.Type.INTEGER) || type.equals(Tipo.Type.REAL) || type.equals(Tipo.Type.LOGICAL)){
            String temp1 = controlador.getTemps();
            //Guardamos la Posicion de la Variable en el Apuntador P
            controlador.addQuad(new Quadrupla("+", "P", String.valueOf(variable.posicion), temp1));

            //Si viene una Expresion (Aritmetica, Relacional o Logica), se debe Traducir primero, para solamente obtener el valor final
            Quadrupla quadExpresion = this.valor.translate(controlador);
            String resultado = "";
            //Si la Quadrupla de retorno No es nula, se almacena el valor, de lo contrario se setea como vacio, porque no tiene ningun valor asignado
            if(quadExpresion != null){
                resultado = quadExpresion.resultado;
            } else {
                resultado = "";
            }
            controlador.addQuad(new Quadrupla("ASSIGN",resultado,"",controlador.tree.STACK+"[(int)"+temp1+"]"));
        } else {
            System.out.println("Tipo de Dato no Implementado para 3D");
        }

        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("DECLARACION");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;

    }
}
