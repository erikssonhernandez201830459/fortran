package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class SubRutina extends Instruccion {

    String id1;
    String id2;
    ArrayList<String> listaParams1;
    ArrayList<Instruccion> listaParams2;
    ArrayList<Instruccion> instrucciones;
    public SubRutina(String id1, String id2, ArrayList<String> listaParams1, ArrayList<Instruccion> listaParams2, ArrayList<Instruccion> instrucciones, int line, int column) {
        super(null, line, column);
        this.id1 = id1;
        this.id2 = id2;
        this.listaParams1 = listaParams1;
        this.listaParams2 = listaParams2;
        this.instrucciones = instrucciones;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VALIDACIONES GENERALES
         */
        if(!id1.equalsIgnoreCase(id2)){
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Los nombres de apertura y cierre deben ser iguales ",
                    this.line, this.column);
            //tree.consola.add(error.toString());
            tree.excepciones.add(error);
            return error;
        }

        if(this.listaParams1.size() != this.listaParams2.size()){
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Los parametros estan incompletos ",
                    this.line, this.column);
            //tree.consola.add(error.toString());
            tree.excepciones.add(error);
            return error;
        }

        if (tree.getMetodo(this.id1) == null) {
            this.type = Tipo.Type.METODO;

            for (int i = 0; i < listaParams2.size(); i++) {
                java.lang.Object decla2 = listaParams2.get(i);
                if (decla2 instanceof Declaracion dec) {
                    if (!dec.id.equalsIgnoreCase(listaParams1.get(i))) {
                        String descError = "Los parametros deben tener el mismo nombre [" + dec.id + "] [" + listaParams1.get(i) + "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        //tree.consola.add(error.toString());
                        tree.excepciones.add(error);
                        return error;
                    }
                } else if (decla2 instanceof DeclaracionArray dec) {
                    if (!dec.id.equalsIgnoreCase(listaParams1.get(i))) {
                        String descError = "Los parametros deben tener el mismo nombre [" + dec.id + "] [" + listaParams1.get(i) + "] ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        //tree.consola.add(error.toString());
                        tree.excepciones.add(error);
                        return error;
                    }
                }
            }

            ArrayList<ArrayList<Instruccion>> metodoValor = new ArrayList<>();

            metodoValor.add(listaParams2);
            metodoValor.add(instrucciones);

            Symbol metodo = new Symbol(new Tipo(Tipo.Type.VOID), new Tipo(Tipo.Type.METODO), this.id1, metodoValor, this.line, this.column, table);
            tree.setMetodo(metodo);
            return null;
        }
            /**
             * VALIDACION DE SUBRUTINA CON EL MISMO NOMBRE YA CREADA EN LA TABLA DE SIMBOLOS
             */
        else {
            String descError = "La subrutina [" +this.id1+ "] ya ha sido creado con anterioridad";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("SUBRUTINA");
        nodo.agregarHijo(new NodoAST(id1));
        if(listaParams1.size() != 0){
            NodoAST nodoParam = new NodoAST("PARAMETROS");
            for(String param: listaParams1){
                nodoParam.agregarHijo(param);
            }
            nodo.agregarHijo(nodoParam);
        }
        return nodo;
    }
    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
}
