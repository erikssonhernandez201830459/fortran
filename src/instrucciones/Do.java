package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;
import expresiones.Primitivo;

import java.util.ArrayList;

public class Do extends Instruccion {
    Instruccion inicio;
    Instruccion fin;
    Instruccion paso;
    ArrayList<Instruccion> expresiones;
    String id;
    String id2;
    ArrayList<NodoAST> instruccionEjec = new ArrayList<>();
    NodoAST nodoPadre;

    public Do(String id, String id2, Instruccion inicio, Instruccion fin, Instruccion paso, ArrayList<Instruccion> expresiones, int line, int column) {
        super(null, line, column);
        this.inicio = inicio;
        this.fin = fin;
        this.paso = paso;
        this.expresiones = expresiones;
        this.id = id;
        this.id2 = id2;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * NUEVO AMBITO
         */
        SymbolTable newtable = new SymbolTable(table);
        /**
         * VALIDACIONES GENERALES
         */
        if(!this.id.equalsIgnoreCase(id2)){
            String descError = "Las etiquetas deben ser nombradas de igual forma";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        /**
         * VALIDACION DE INICIO DEL CICLO
         */
        if(this.inicio == null){
            String descError = "Debe enviar un Inicio de ciclo valido";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        java.lang.Object inicioCiclo = this.inicio.execute(newtable, tree);
        if (inicioCiclo instanceof Excepcion) {
            return inicioCiclo;
        }

        /**
         * VALIDACION DE FIN DE CICLO
         */
        if(this.fin == null){
            String descError = "Debe enviar un Fin de ciclo valido";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        java.lang.Object finCiclo = this.fin.execute(newtable, tree);
        if (finCiclo instanceof Excepcion) {
            return finCiclo;
        }

        /**
         * VALIDACION DE PASO DE CICLO
         */
        if(this.paso == null){
            String descError = "Debe enviar un Paso de ciclo valido";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        java.lang.Object pasoCiclo  = this.paso.execute(newtable, tree);
        if (pasoCiclo instanceof Excepcion) {
            return pasoCiclo;
        }
        /**
         * VALIDACION DE TIPO DE DATO INTEGER PARA EL INICIO, FIN Y EL PASO
         */
        if(this.inicio.type != Tipo.Type.INTEGER || this.fin.type != Tipo.Type.INTEGER || this.paso.type != Tipo.Type.INTEGER){
            String descError = "El inicio("+this.inicio.type+"), fin("+this.fin.type+") y paso("+this.paso.type+") deben ser del tipo INTEGER";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        /**
         * VALIDACION DE QUE NO SEA UNA LISTA DE INSTRUCCIONES VACIAS
         */
        if(this.expresiones.size() == 0){
            String descError = "La Instruccion DO no puede venir sin instrucciones";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }
        /**
         * RECORRIDO Y EJECUCION DE LA LISTA DE INSTRUCCIONES
         * MEDIANTE UN CICLO FOR
         */
        for(int i = (int)inicioCiclo; i <= (int)finCiclo  ; i += (int)pasoCiclo){


            java.lang.Object res;

            //Creamos el nodo para el AST
            NodoAST nodoIteracion = new NodoAST("ITERACION");

            //Recorremos y Ejecutamos todas las intrucciones
            for(int j = 0; j<this.expresiones.size(); j++){

                //Ejecutamos la instruccion
                res = this.expresiones.get(j).execute(newtable, tree);

                //ERROR
                if(res instanceof Excepcion e){
                    tree.consola.add(e.toString());
                }

                /**
                 * VALIDACION DE ETIQUETA EXIT PARA EL DO
                 */
                if (res instanceof Exit ex) {

                    //Salimos de este for y detenemos la ejecucion
                    if(ex.id.equalsIgnoreCase(this.id)){
                        Symbol in = table.getVariable(((Asignacion)this.inicio).id);

                        Asignacion nueva = new Asignacion(in.id, new Primitivo(Tipo.Type.INTEGER, inicioCiclo, this.line, this.column), this.line, this.column);
                        nueva.execute(newtable, tree);

                        //Agregamos los valores para el AST
                        nodoIteracion.agregarHijo(this.expresiones.get(j).getAST());
                        nodoIteracion.agregarHijo(nueva.getAST());

                        //Creamos los nodos para el AST
                        nodoPadre = new NodoAST("DO");

                        NodoAST nodoInicio = new NodoAST("INICIO");
                        nodoInicio.agregarHijo(this.inicio.getAST());
                        nodoPadre.agregarHijo(nodoInicio);

                        NodoAST nodoFin = new NodoAST("FIN");
                        nodoFin.agregarHijo(this.fin.getAST());
                        nodoPadre.agregarHijo(nodoFin);

                        NodoAST nodoPaso = new NodoAST("PASO");
                        nodoPaso.agregarHijo(this.paso.getAST());
                        nodoPadre.agregarHijo(nodoPaso);

                        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                        nodoIns.agregarHijos(instruccionEjec);
                        nodoPadre.agregarHijo(nodoIns);

                        //Detenemos la Ejecucion
                        return null;
                    }

                    /**
                     * CONTINUACION DEL DO DESPUES DE LA ETIQUETA EXIT
                     */
                    else{
                        //Creamos los nodos para el AST
                        nodoPadre = new NodoAST("DO");

                        NodoAST nodoInicio = new NodoAST("INICIO");
                        nodoInicio.agregarHijo(this.inicio.getAST());
                        nodoPadre.agregarHijo(nodoInicio);

                        NodoAST nodoFin = new NodoAST("FIN");
                        nodoFin.agregarHijo(this.fin.getAST());
                        nodoPadre.agregarHijo(nodoFin);

                        NodoAST nodoPaso = new NodoAST("PASO");
                        nodoPaso.agregarHijo(this.paso.getAST());
                        nodoPadre.agregarHijo(nodoPaso);

                        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                        nodoIns.agregarHijos(instruccionEjec);
                        nodoPadre.agregarHijo(nodoIns);

                        //Detenemos la ejecucion
                        return res;
                    }
                }

                /**
                 * VALIDACIONES PARA LA ETIQUETA CYCLE DENTRO DEL DO
                 */
                else if(res instanceof Cycle ex){

                    //Detenemos el for actual
                    if(ex.id.equalsIgnoreCase(this.id)){

                        //Agregamos los nodo para el AST
                        nodoIteracion.agregarHijo(this.expresiones.get(j).getAST());

                        break;
                    }

                    /**
                     * VALIDACION PARA EL DO ACTUAL Y ENVIO DE CYCLE
                     */
                    else{
                        //Creamos los nodos para el AST
                        nodoPadre = new NodoAST("DO");

                        NodoAST nodoInicio = new NodoAST("INICIO");
                        nodoInicio.agregarHijo(this.inicio.getAST());
                        nodoPadre.agregarHijo(nodoInicio);

                        NodoAST nodoFin = new NodoAST("FIN");
                        nodoFin.agregarHijo(this.fin.getAST());
                        nodoPadre.agregarHijo(nodoFin);

                        NodoAST nodoPaso = new NodoAST("PASO");
                        nodoPaso.agregarHijo(this.paso.getAST());
                        nodoPadre.agregarHijo(nodoPaso);

                        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                        nodoIns.agregarHijos(instruccionEjec);
                        nodoPadre.agregarHijo(nodoIns);

                        //Detenemos la ejecucion
                        return res;
                    }
                }

                //Agregamos los nodo para el AST
                nodoIteracion.agregarHijo(this.expresiones.get(j).getAST());
            }

            /**
             * SIMULACION DEL PASO
             */
            int k;
            if(i + (int)pasoCiclo <= (int)finCiclo){
                k = i + (int)pasoCiclo;
            }else{
                k = i;
            }
            /**
             * ACTUALIZACION DE LA VARIABLE DE PASO EN LA TABLA DE SIMBOLOS
             */
            //Obtenemos la variable de paso de la tabla de simbolos
            Symbol in = table.getVariable(((Asignacion)this.inicio).id);

            //Creamos la asignacion y la Ejecutamos
            Asignacion nueva = new Asignacion(in.id, new Primitivo(Tipo.Type.INTEGER, k, this.line, this.column), this.line, this.column);
            nueva.execute(newtable, tree);

            //Agregamos los un nodo para el AST
            nodoIteracion.agregarHijo(nueva.getAST());
            instruccionEjec.add(nodoIteracion);
        }

        /**
         * ACTUALIZACION DE LA VARIABLE DE INICIO EN LA TABLA DE SIMBOLOS
         */
        //Obtenemos la variable de paso de la tabla de simbolos
        Symbol in = table.getVariable(((Asignacion)this.inicio).id);

        //Creamos la asignacion y la Ejecutamos
        Asignacion nueva = new Asignacion(in.id, new Primitivo(Tipo.Type.INTEGER, inicioCiclo, this.line, this.column), this.line, this.column);
        nueva.execute(newtable, tree);

        //Creamos los nodos para el AST
        nodoPadre = new NodoAST("DO");

        NodoAST nodoInicio = new NodoAST("INICIO");
        nodoInicio.agregarHijo(this.inicio.getAST());
        nodoPadre.agregarHijo(nodoInicio);

        NodoAST nodoFin = new NodoAST("FIN");
        nodoFin.agregarHijo(this.fin.getAST());
        nodoPadre.agregarHijo(nodoFin);

        NodoAST nodoPaso = new NodoAST("PASO");
        nodoPaso.agregarHijo(this.paso.getAST());
        nodoPadre.agregarHijo(nodoPaso);

        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
        nodoIns.agregarHijos(instruccionEjec);
        nodoPadre.agregarHijo(nodoIns);

        //Detenemos la ejecucion
        return null;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("DO");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
}
