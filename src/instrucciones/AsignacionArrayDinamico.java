package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;
import expresiones.Primitivo;

import java.util.ArrayList;

public class AsignacionArrayDinamico extends Instruccion {
    String id;
    Instruccion expresion;

    NodoAST nodoPadre;
    public AsignacionArrayDinamico(String id, Instruccion expresion, int line, int column) {
        super(null, line, column);
        this.id = id;

        this.expresion = expresion;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol variable = table.getVariable(this.id);

        /**
         * VALIDACIONES GENERALES
         */
        if (variable == null) {
            String descError = "La variable ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        //ERROR
        if(this.expresion == null){
            String descError = "La expresion no es valida ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        java.lang.Object result = this.expresion.execute(table, tree);
        if (result instanceof Excepcion) {
            return result;
        }

        /**
         * VALIDACIONES ENTRE LA VARIABLE Y EL TIPO
         */
        if(this.expresion.type != variable.tipo.tipo){
            String descError = "El arreglo no puede ser asignado porque no poseen los mismos tipos ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        /**
         * VERIFICACION DE VARIABLES PARA LOS ARRAYS
         */
        if(variable.tipo2.tipo == Tipo.Type.ARREGLO || variable.tipo2.tipo == Tipo.Type.ALLOCATE){

            //Verificamos si el valor de la variable es una Lista
            if(variable.valor instanceof ArrayList<?> listaInstrucciones){

                //ERROR
                if(listaInstrucciones.size() == 0){
                    String descError = "El arreglo no posee dimensiones ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }

                String resultado = "[";
                for(int i = 0; i < listaInstrucciones.size(); i++){
                    Primitivo prim= new Primitivo(variable.tipo.tipo, result, this.line, this.column);
                    ((ArrayList<Instruccion>)variable.valor).set(i, prim);

                    resultado += result + ", ";
                }
                resultado += "]";

                //Creamos el nodo para el AST
                nodoPadre = new NodoAST("ASIGNACION");
                nodoPadre.agregarHijo(new NodoAST(this.id));
                nodoPadre.agregarHijo(new NodoAST(resultado));

                //Terminamos la ejecucion
                return null;
            }

            //ERROR
            else{
                String descError = "La variable no posee un arreglo ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }
            /**
             * VALIDACIONES PARA LOS ARREGLOS DE 2 DIMENSIONES
             */
        }else if(variable.tipo2.tipo == Tipo.Type.ARREGLO2 || variable.tipo2.tipo == Tipo.Type.ALLOCATE2){
            //Verificamos si el valor de la variable es una Lista
            if(variable.valor instanceof ArrayList<?> listaInstrucciones){

                //ERROR
                if(listaInstrucciones.size() == 0){
                    String descError = "El arreglo no posee dimensiones ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }

                if(listaInstrucciones.get(0) instanceof ArrayList<?> listaJ) {
                    String resultado = "[";
                    for(int i = 0; i < listaInstrucciones.size(); i++){
                        resultado += "[";

                        // Obtenemos la lista de Nodos
                        ArrayList<Instruccion> arra = ((ArrayList<ArrayList<Instruccion>>)variable.valor).get(i);

                        for(int j = 0; j < arra.size(); j++){
                            //Creamos el nuevo valor para el arreglo
                            Primitivo prim= new Primitivo(variable.tipo.tipo, result, this.line, this.column);
                            arra.set(j, prim);

                            resultado += result + ", ";
                        }
                        resultado += "], ";
                        ((ArrayList<ArrayList<Instruccion>>)variable.valor).set(i, arra);
                    }
                    resultado += "]";

                    //Creamos el nodo para el AST
                    nodoPadre = new NodoAST("ASIGNACION");
                    nodoPadre.agregarHijo(new NodoAST(this.id));
                    nodoPadre.agregarHijo(new NodoAST(resultado));

                    //Terminamos la ejecucion
                    return null;
                }

                //ERROR
                else{
                    String descError = "La variable no posee un arreglo ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }
            }

            //ERROR
            else{
                String descError = "La variable no posee un arreglo ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }
        }   //ERROR
        else{
            String descError = "La variable [" +variable.id+ "] no es de un tipo valido para asignar ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ASIGNACION");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;

    }
}
