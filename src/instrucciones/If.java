package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class If extends Instruccion {

    Instruccion condicion;
    ArrayList<Instruccion> listaIf;
    ArrayList<Instruccion> listaElse;

    NodoAST nodoPadre;
    ArrayList<NodoAST> listaIfAST = new ArrayList<>();
    ArrayList<NodoAST> listaElseAST = new ArrayList<>();

    public If(Instruccion condicion, ArrayList<Instruccion> listaIf, ArrayList<Instruccion> listaElse, int line, int column) {
        super(null, line, column);
        this.condicion = condicion;
        this.listaIf = listaIf;
        this.listaElse = listaElse;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        SymbolTable newtable = new SymbolTable(table);
        /**
         * VALIDACIONES GENERALES
         */
        //Verificamos que venga una condicion
        if(this.condicion == null){
            String descError = "Debe enviar una condicion valida";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }
        
        java.lang.Object result = this.condicion.execute(newtable, tree);

        if (result instanceof Excepcion) {
            return result;
        }

        if (this.condicion.type != Tipo.Type.LOGICAL) {
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, "Se esperaba una expresion LOGICA para la condicion", this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        /**
         * VALIDACION DE CONDICION DE ENTRADA VERDADERA
         */
        if((boolean)result) {
            for(int i = 0; i<this.listaIf.size(); i++) {
                java.lang.Object res = this.listaIf.get(i).execute(newtable, tree);

                if (res instanceof Excepcion e) {
                    tree.consola.add(e.toString());
                }

                //Agregamos los AST a una lista
                listaIfAST.add(this.listaIf.get(i).getAST());

                /**
                 * VERIFICAMOS QUE NO SEA UNA INSTANCIA DE EXIT O CYCLE
                 */
                if (res instanceof Exit || res instanceof Cycle) {
                    //Creamos el nodoPadre para el AST
                    nodoPadre = new NodoAST("IF");

                    NodoAST nodoCond = new NodoAST("CONDICION");
                    nodoCond.agregarHijo(this.condicion.getAST());
                    nodoPadre.agregarHijo(nodoCond);

                    NodoAST nodoIf = new NodoAST("INSTRUCCIONES IF");
                    if(this.listaIf.size() != 0){
                        nodoIf.agregarHijos(listaIfAST);
                    }

                    nodoPadre.agregarHijo(nodoIf);

                    //Detenemos la ejecucion
                    return res;
                }
            }
            //Creamos el nodoPadre para el AST
            nodoPadre = new NodoAST("IF");

            NodoAST nodoCond = new NodoAST("CONDICION");
            nodoCond.agregarHijo(this.condicion.getAST());
            nodoPadre.agregarHijo(nodoCond);

            NodoAST nodoIf = new NodoAST("INSTRUCCIONES IF");
            if(this.listaIf.size() != 0){
                nodoIf.agregarHijos(listaIfAST);
            }

            nodoPadre.agregarHijo(nodoIf);

            //Detenemos la ejecucion
            return true;

            /**
             * VALIDACION DE LA CONDICION DE ENTRADA FALSA
             */
        }else {
            for (int i = 0; i < this.listaElse.size(); i++) {
                java.lang.Object res = this.listaElse.get(i).execute(newtable, tree);

                if (res instanceof Excepcion e) {
                    tree.consola.add(e.toString());
                }else{
                    //Agregamos los AST a una lista
                    listaElseAST.add(this.listaElse.get(i).getAST());

                    /**
                     * VERIFICAMOS QUE NO SEA UNA INSTANCIA DE EXIT O CYCLE
                     */
                    if (res instanceof Exit || res instanceof Cycle) {
                        //Creamos el nodoPadre para el AST
                        nodoPadre = new NodoAST("IF");

                        NodoAST nodoCond = new NodoAST("CONDICION");
                        nodoCond.agregarHijo(this.condicion.getAST());
                        nodoPadre.agregarHijo(nodoCond);

                        NodoAST nodoElse = new NodoAST("INSTRUCCIONES ELSE");
                        if(this.listaIf.size() != 0){
                            nodoElse.agregarHijos(listaElseAST);
                        }

                        nodoPadre.agregarHijo(nodoElse);

                        //Detenemos la ejecucion
                        return res;
                    }

                    /**
                     * VALIDACION DE SI ES UNA CONDICIONAL ELSE IF
                     */
                    if(res != null){
                        if(res instanceof  Boolean){
                            if((boolean)res) {
                                //Creamos el nodoPadre para el AST
                                nodoPadre = new NodoAST("ELSE IF");

                                NodoAST nodoCond = new NodoAST("CONDICION");
                                nodoCond.agregarHijo(this.condicion.getAST());
                                nodoPadre.agregarHijo(nodoCond);

                                NodoAST nodoElse = new NodoAST("INSTRUCCIONES ELSE IF");
                                if (this.listaIf.size() != 0) {
                                    nodoElse.agregarHijos(listaElseAST);
                                }

                                nodoPadre.agregarHijo(nodoElse);

                                //Detenemos la ejecucion
                                return false;
                            }
                        }
                    }
                }
            }

            //Creamos el nodoPadre para el AST
            nodoPadre = new NodoAST("IF");

            NodoAST nodoCond = new NodoAST("CONDICION");
            nodoCond.agregarHijo(this.condicion.getAST());
            nodoPadre.agregarHijo(nodoCond);

            NodoAST nodoElse = new NodoAST("INSTRUCCIONES ELSE");
            if(this.listaIf.size() != 0){
                nodoElse.agregarHijos(listaElseAST);
            }

            nodoPadre.agregarHijo(nodoElse);

            //Detenemos la ejecucion
            return false;
        }
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("IF");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
}
