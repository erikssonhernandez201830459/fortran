package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class ListaDeclaraciones extends Instruccion {
    ArrayList<Instruccion> declaraciones;

    ArrayList<NodoAST> nodoDeclaraciones = new ArrayList<>();

    public ListaDeclaraciones(ArrayList<Instruccion> declaraciones,int line, int column) {
        super(null, line, column);
        this.declaraciones = declaraciones;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        //Recorremos todas las declaraciones de la lista
        for(int i = 0; i < declaraciones.size(); i++){

            //Ejecutamos todas las declaraciones de la lista
            java.lang.Object result = declaraciones.get(i).execute(table, tree);

            //ERROR
            if(result instanceof Excepcion e){
                //tree.consola.add(e.toString());
                return result;
            }

            else {
                //Guardamos los AST de las declaraciones
                nodoDeclaraciones.add(declaraciones.get(i).getAST());
            }
        }
        return null;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("LISTA DECLARACION");
        nodo.agregarHijos(nodoDeclaraciones);
        return nodo;
    }
}
