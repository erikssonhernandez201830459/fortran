package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Program extends Instruccion {
    public ArrayList<Instruccion> instrucciones;
    public String abre;
    public String cierra;

    ArrayList<NodoAST> nodoInstrucciones = new ArrayList<>();
    public Program(ArrayList<Instruccion> instrucciones, String abre, String cierra, int line, int column) {
        super(null, line, column);
        this.instrucciones = instrucciones;
        this.abre = abre;
        this.cierra = cierra;
    }

    @Override
    public Object execute(SymbolTable table, Tree tree) {
        SymbolTable newTable = new SymbolTable(table);
        /**
         * GUARDANDO LA TABLA DE SIMBOLOS PARA EL RECORRIDO EN 3D
         */
        tree.tablas.add(newTable);

        /**
         * VALIDACION DE NOMBRE DE APERTURA Y CIERRE
         */
        if(abre.equalsIgnoreCase(cierra)){
            //Recorremos todas las instrucciones obtenidas
            for(int i = 0; i<instrucciones.size(); i++){

                //Ejecutamos todas las instrucciones creadas dentro del PROGRAM
                Object value = instrucciones.get(i).execute(newTable, tree);

                //Obtenemos todos los AST de las instrucciones
                nodoInstrucciones.add(instrucciones.get(i).getAST());

                //ERROR
                if(value instanceof Excepcion e){
                    //return tree.consola.add(e.toString());
                }
            }
        }
        //ERROR GENERAL
        else{
            String descError = "Los nombres de apertura y cierre deben ser iguales";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());


        }
        return null;
    }


    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("PROGRAM");
        nodo.agregarHijos(nodoInstrucciones);
        return nodo;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        //if (!controlador.tree.tablas.isEmpty()) {
          //  controlador.actual = controlador.tree.tablas.remove(0);
        //} else {
            controlador.actual = controlador.tree.tablas.get(0);
        //}
        for (int i = 0; i < instrucciones.size(); i++) {

            //Ejecutamos todas las instrucciones creadas dentro del PROGRAM
            Object value = instrucciones.get(i).translate(controlador);


        }
        return null;
    }
}
