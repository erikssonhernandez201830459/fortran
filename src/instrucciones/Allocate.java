package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Allocate extends Instruccion {
    String id;
    Instruccion dimension1;
    Instruccion dimension2;
    NodoAST nodoPadre;

    public Allocate(String id, Instruccion dimension1, Instruccion dimension2, int line, int column) {
        super(null, line, column);
        this.id = id;
        this.dimension1 = dimension1;
        this.dimension2 = dimension2;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol simbolo = table.getVariable(this.id);

        if(simbolo == null){
            String descError = "La variable [" + this.id + "] no ha sido encontrada";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        //ERROR
        if(this.dimension1 == null){
            String descError = "No han ingresado dimensiones";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            return error;
        }

        java.lang.Object di1 = this.dimension1.execute(table, tree);
        if (di1 instanceof Excepcion e) {
            return di1;
        }

        if(((ArrayList<?>)simbolo.valor).size() != 0){
            String descError = "La variable [" + this.id + "] ya posee una dimension, no puede usar Allocate de nuevo";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        if(simbolo.tipo2.tipo == Tipo.Type.ALLOCATE){
            //ERROR
            if(this.dimension2 != null){
                String descError = "El arreglo es unicamente de 1 dimension ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }

            if(di1 instanceof Integer d1){
                //ERROR
                if(d1 <= 0){
                    String descError = "Las dimensiones no pueden ser menores a 1 ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }

                //Creo una nueva lista vacia
                ArrayList<Instruccion> res = new ArrayList<Instruccion>();

                for(int i = 0; i<d1; i++){
                    res.add(Default.value(simbolo.tipo.tipo, this.line, this.column));
                }

                //Para el AST
                nodoPadre = new NodoAST("ALLOCATE");
                nodoPadre.agregarHijo(this.dimension1.getAST());

                //Se asigna la lista a la variable
                simbolo.valor = res;
                return null;
            }

            //ERROR
            else{
                String descError = "Las dimensiones deben ser INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }


        }else if(simbolo.tipo2.tipo == Tipo.Type.ALLOCATE2){
            //ERROR
            if(this.dimension2 == null){
                String descError = "El arreglo es de 2 dimension ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }

            java.lang.Object di2 = this.dimension2.execute(table, tree);
            if (di2 instanceof Excepcion e) {
                return di2;
            }

            if(di1 instanceof Integer d1) {
                if(di2 instanceof Integer d2) {
                    if (d1 <= 0 || d2 <= 0) {
                        String descError = "Las dimensiones no pueden ser menores a 1 ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                        tree.excepciones.add(error);
                        return error;
                    }

                    //Se crea la nueva lista de listas
                    ArrayList<ArrayList<Instruccion>> resI = new ArrayList<ArrayList<Instruccion>>();

                    //Se llena la lista
                    for (int i = 0; i < d1; i++) {
                        //Se crea la lista de Instrucciones
                        ArrayList<Instruccion> resJ = new ArrayList<Instruccion>();

                        //Se llena la lista de Instrucciones
                        for (int j = 0; j < d2; j++) {
                            resJ.add(Default.value(simbolo.tipo.tipo, this.line, this.column));
                        }
                        resI.add(resJ);
                    }

                    //Para el AST
                    nodoPadre = new NodoAST("ALLOCATE");
                    nodoPadre.agregarHijo(this.dimension1.getAST());
                    nodoPadre.agregarHijo(this.dimension2.getAST());

                    //Se asigna la lista a la variable
                    simbolo.valor = resI;
                    return null;
                }

                //ERROR
                else{
                    String descError = "Las dimensiones deben ser INTEGER ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                    tree.excepciones.add(error);
                    return error;
                }
            }

            //ERROR
            else{
                String descError = "Las dimensiones deben ser INTEGER ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
                tree.excepciones.add(error);
                return error;
            }
        }

        else{
            String descError = "El arreglo no es del tipo Allocatable";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ALLOCATE");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }


}
