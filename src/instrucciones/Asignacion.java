package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class Asignacion extends Instruccion {
    public String id;
    public Object expresion;
    NodoAST nodoPadre;

    public Asignacion(String id, Object valor, int line, int column) {
        super(null, line, column);
        this.id = id;
        this.expresion = valor;
    }

    @Override
    public Object execute(SymbolTable table, Tree tree) {


        /**
         * VALIDACIONES DE QUE NO SEA UNA INSTRUCCION, SINO SE DEBE EJECUTAR
         */
        if(this.expresion == null){
            String descError = "La expresion no es valida";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        Object result = this.expresion;


        if(this.expresion instanceof Instruccion res){
            result = res.execute(table, tree);
            if (result instanceof Excepcion) {
                return result;
            }
        }

        Symbol variable = table.getVariable(this.id);

        if (variable == null) {
            String descError = "La variable ["+this.id+"] no ha sido encontrada ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError,this.line, this.column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }
        this.type = variable.tipo.tipo;
        /**
         * ERROR DE TIPOS
         * NO SE ADMITEN CASTEOS IMPLICITOS
         */
        if(this.expresion instanceof Instruccion res) {
            if (res.type != variable.tipo.tipo) {
                String descError = "La variable [" + this.id + "] no puede ser asignada debido a que son de diferentes Tipos (" + variable.tipo.tipo + ") y (" + res.type + ") ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);

                return error;

            } else {
                this.type = variable.tipo.tipo;
            }
        }

        /**
         * Verificamos si la variable es del tipo variable
         */

        if(variable.tipo2.tipo == Tipo.Type.VARIABLE){
            if(this.expresion instanceof Instruccion rest){
                Object val = rest.execute(table, tree);

                if (val instanceof Excepcion e) {
                    return val;
                }

                if(val instanceof ArrayList<?>){
                    String descError = "No es un valor valido para La variable ["+this.id+"]";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }


                //Guardamos el nuevo valor en la variable
                    variable.valor = val;



                //Creamos el nodo AST
                nodoPadre = new NodoAST("ASIGNAR");
                nodoPadre.agregarHijo(new NodoAST(this.id));
                nodoPadre.agregarHijo(rest.getAST());

                //Termino la ejecucion
                return val;
            }else{
                String descError = "No es un valor valido para La variable {"+this.id+"}  ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                return error;
            }
        }
        /**
         * VALIDACIONES PARA UNA VARIABLE DE TIPO ARRAY
         */
        else if(variable.tipo2.tipo == Tipo.Type.ARREGLO || variable.tipo2.tipo == Tipo.Type.ALLOCATE) {
            // Verificamos si la entrada es una lista
            if (result instanceof ArrayList<?> listaInstrucciones) {
                ArrayList<Instruccion> variableValor = (ArrayList<Instruccion>) variable.valor;

                //Verificamos que las listas sean de las mismas dimensiones
                if (variableValor.size() != listaInstrucciones.size()) {
                    String descError = "El arreglo no puede ser asignado porque no poseen las mismas dimensiones  ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }

                //ERROR
                if (variableValor.size() == 0) {
                    String descError = "El arreglo no puede ser asignado porque no poseen las mismas dimensiones  ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }

                String resultado = "[";
                for (int i = 0; i < listaInstrucciones.size(); i++) {
                    if (((ArrayList<Instruccion>) listaInstrucciones).get(i).type != variable.tipo.tipo) {
                        String descError = "El arreglo no puede ser asignado porque no poseen los mismos tipos  ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        return error;
                    }
                    resultado += ((ArrayList<Instruccion>) listaInstrucciones).get(i).execute(table, tree) + ", ";
                }
                resultado += "]";

                //Creamos el nodo AST
                nodoPadre = new NodoAST("ASIGNAR");
                nodoPadre.agregarHijo(new NodoAST(this.id));
                nodoPadre.agregarHijo(new NodoAST(resultado));

                //Le agregamos la lista obtenida al arreglo
                variable.valor = listaInstrucciones;
                return null;

            } else {
                String descError = "No esta asignando un arreglo a la variable [" + variable.id + "] ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }
        }
            /**
             * ASIGNACION DE ARREGLOS DE 2 DIMENSIONES
             */
        else if(variable.tipo2.tipo == Tipo.Type.ARREGLO2 || variable.tipo2.tipo == Tipo.Type.ALLOCATE2) {
            //Verificamos si la entrada es una lista
            if (result instanceof ArrayList<?> listaInstrucciones) {

                //Obtenemos la lista de listas de la variable
                ArrayList<ArrayList<Instruccion>> variableValI = (ArrayList<ArrayList<Instruccion>>) variable.valor;

                //Verificamos que la lista obtenida y la de la variable sean de la misma dimension
                if (listaInstrucciones.size() != variableValI.size()) {
                    String descError = "El arreglo no puede ser asignado porque no poseen las mismas dimensiones  ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }

                //ERROR
                if (variableValI.size() == 0) {
                    String descError = "El arreglo no puede ser asignado porque no poseen las mismas dimensiones  ";
                    Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                    tree.excepciones.add(error);
                    return error;
                }

                String resultado = "[";
                for (int i = 0; i < listaInstrucciones.size(); i++) {
                    resultado += "[";


                    if (listaInstrucciones.get(i) instanceof ArrayList<?>) {
                        ArrayList<Instruccion> variableValJ = variableValI.get(i);
                        ArrayList<Instruccion> listaJ = (ArrayList<Instruccion>) listaInstrucciones.get(i);

                        if (listaJ.size() != variableValJ.size()) {
                            String descError = "El arreglo no puede ser asignado porque no poseen las mismas dimensiones  ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                            tree.excepciones.add(error);
                            return error;
                        }

                        //ERROR
                        if (variableValJ.size() == 0) {
                            String descError = "El arreglo no puede ser asignado porque no poseen las mismas dimensiones  ";
                            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                            tree.excepciones.add(error);
                            return error;
                        }

                        for (int j = 0; j < listaJ.size(); j++) {
                            //Verificamos que los valores de la lista sean del mismo tipo que la variable
                            if (listaJ.get(j).type != variable.tipo.tipo) {
                                String descError = "El arreglo no puede ser asignado porque no poseen los mismos tipos  ";
                                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                                tree.excepciones.add(error);
                                return error;
                            }

                            resultado += listaJ.get(j).execute(table, tree) + ", ";
                        }
                        resultado += "], ";
                    }

                    //ERROR
                    else {
                        String descError = "No esta asignando un arreglo valido a la variable [" + variable.id + "]  ";
                        Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                        tree.excepciones.add(error);
                        return error;
                    }

                }
                resultado += "]";

                //Creamos el nodo AST
                nodoPadre = new NodoAST("ASIGNAR");
                nodoPadre.agregarHijo(new NodoAST(this.id));
                nodoPadre.agregarHijo(new NodoAST(resultado));

                //Le asignamos el valor a la variable
                variable.valor = listaInstrucciones;
                return null;
            } else {
                String descError = "No esta asignando un arreglo a la variable [" + variable.id + "] ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
            }

        } else{

                String descError = "La variable [" +variable.id+ "] no es del tipo array, no puede asignarla ";
                Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
                tree.excepciones.add(error);
                //tree.consola.add(error.toString());
                return error;
                    }

    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        Symbol variable = controlador.actual.getVariable(this.id);
        Tipo type = variable.getTipo();
        if(this.expresion instanceof Instruccion rest) {
            Object result = rest.execute(controlador.actual, controlador.tree);
            String resultString = result.toString();

            if (type.equals(Tipo.Type.STRING) || type.equals(Tipo.Type.COMPLEX) || type.equals(Tipo.Type.CHARACTER)) {
                String temp1 = controlador.getTemps();
                String temp2 = controlador.getTemps();
                //Agregamos una Quadrupla de Asignacion
                controlador.addQuad(new Quadrupla("ASSIGN", "H", "", temp1));
                //VERIFICAMOS QUE NO VENGA VACIO Y LO RECORREMOS PARA REALIZAR LA ASIGNACION
                for (int i = 0; i < result.toString().length(); i++) {
                    //Se separa la cadena en caracteres y se asigna cada uno en el HEAP
                    controlador.addQuad(new Quadrupla("ASSIGN", String.valueOf(resultString.charAt(i)), "", controlador.tree.HEAP + "[(int)H]"));

                    //Sumamos posiciones en el HEAP
                    controlador.addQuad(new Quadrupla("+", "H", "1", "H"));
                }
                //Si viene vacia la declaracion, solamente declaramos la variable y la guardamos en el HEAP
                controlador.addQuad(new Quadrupla("ASSIGN", "-1", "", controlador.tree.HEAP + "[(int)H]"));
                //Sumamos una posicion al HEAP
                controlador.addQuad(new Quadrupla("+", "H", "1", "H"));

                //Guardamos la posicion de la variable en el Apuntador P
                controlador.addQuad(new Quadrupla("+", "P", String.valueOf(variable.posicion), temp2));
                //Asignamos la variable en el STACK
                controlador.addQuad(new Quadrupla("ASSIGN", temp1, "", controlador.tree.STACK + "[(int)" + temp2 + "]"));
                System.out.println(controlador);
            } else if (type.equals(Tipo.Type.INTEGER) || type.equals(Tipo.Type.REAL) || type.equals(Tipo.Type.LOGICAL)) {
                String temp1 = controlador.getTemps();
                //Guardamos la Posicion de la Variable en el Apuntador P
                controlador.addQuad(new Quadrupla("+", "P", String.valueOf(variable.posicion), temp1));

                //Si viene una Expresion (Aritmetica, Relacional o Logica), se debe Traducir primero, para solamente obtener el valor final
                Quadrupla quadExpresion = rest.translate(controlador);
                String resultado = "";
                //Si la Quadrupla de retorno No es nula, se almacena el valor, de lo contrario se setea como vacio, porque no tiene ningun valor asignado
                if (quadExpresion != null) {
                    resultado = quadExpresion.resultado;
                } else {
                    resultado = "";
                }
                controlador.addQuad(new Quadrupla("ASSIGN", resultado, "", controlador.tree.STACK + "[(int)" + temp1 + "]"));
            } else {
                System.out.println("Tipo de Dato no Implementado para 3D");
            }
        } else {
            System.out.println("El valor de la asignacion no es una Expresion");
        }

        return null;
    }


    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ASIGNACION");

        if (nodoPadre != null) {
            return nodoPadre;
        }

        return nodo;
    }
}
