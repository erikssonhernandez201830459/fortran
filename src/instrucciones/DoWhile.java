package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class DoWhile extends Instruccion {

    Instruccion condicion;
    ArrayList<Instruccion> expresiones;
    String id;
    String id2;
    NodoAST nodoPadre;
    ArrayList<NodoAST> instruccionEjec = new ArrayList<>();

    public DoWhile(String id, String id2, Instruccion condicion, ArrayList<Instruccion> expresiones, int line, int column) {
        super(null, line, column);
        this.condicion = condicion;
        this.expresiones = expresiones;
        this.id = id;
        this.id2 = id2;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        /**
         * VALIDACIONES GENERALES
         */
        if(!this.id.equalsIgnoreCase(id2)){
            String descError = "Las etiquetas deben ser nombradas de igual forma  ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

        SymbolTable newTable = new SymbolTable(table);

        /**
         * VALIDACION DE CONDICIONAL
         */
        if(this.condicion == null){
            String descError = "Debe enviar una condicion valida ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }

        java.lang.Object condicionCiclo = this.condicion.execute(newTable, tree);
        if (condicionCiclo instanceof Excepcion) {
            return condicionCiclo;
        }

        /**
         * VALIDACION DE QUE NO SEA UNA LISTA DE INSTRUCCIONES VACIAS
         */
        if(this.expresiones.size() == 0){
            String descError = "La Instruccion DO While no puede venir sin instrucciones ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, this.line, this.column);
            tree.excepciones.add(error);
            return error;
        }
        /**
         * VALIDACION DEL TIPO DE DATO DE LA CONDICION BOOLEANA
         */
        if(this.condicion.type != Tipo.Type.LOGICAL ){
            String descError = "La codicion debe ser de tipo LOGICAL ("+this.condicion.type+") ";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            return error;
        }

        //Creamos los nodo para el AST
        NodoAST nodoCond = new NodoAST("CONDICION");
        nodoCond.agregarHijo(this.condicion.getAST());

        /**
         * RECORRIDO Y EJECUCION DE LA LISTA DE INSTRUCCIONES
         * MEDIANTE UN CICLO WHILE Y UN FOR
         */
        //Creamos una variable para que no se encicle
        int i = 0;

        //While para repetir las instrucciones
        while ((boolean)condicionCiclo){
            java.lang.Object res = null;

            //Creamos nodo para el AST
            NodoAST nodoIteracion = new NodoAST("ITERACION");

            //For para ejecutar las instrucciones que vienen dentro del Do while
            for(int j = 0; j < this.expresiones.size(); j++){
                res = this.expresiones.get(j).execute(newTable, tree);

                //ERROR
                if(res instanceof Excepcion e){
                    tree.consola.add(e.toString());
                }

                nodoIteracion.agregarHijo(this.expresiones.get(j).getAST());

                /**
                 * VALIDACION DE ETIQUETA EXIT PARA EL DO WHILE
                 */
                if(res instanceof Exit ex){
                    if(ex.id.equalsIgnoreCase(this.id)){
                        //Terminamos la Ejecucion

                        //Creamos el nodo para el AST
                        nodoPadre = new NodoAST("DO WHILE");
                        nodoPadre.agregarHijo(nodoCond);

                        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                        nodoIns.agregarHijos(instruccionEjec);
                        nodoPadre.agregarHijo(nodoIns);

                        //Terminamos la ejecucion
                        return null;

                    /**
                     * CONTINUACION DEL DO WHILE DESPUES DE LA ETIQUETA EXIT
                     */
                    }else{
                        //Terminamos la Ejecucion y sigo enviando el Exit

                        //Creamos el nodo para el AST
                        nodoPadre = new NodoAST("DO WHILE");
                        nodoPadre.agregarHijo(nodoCond);

                        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                        nodoIns.agregarHijos(instruccionEjec);
                        nodoPadre.agregarHijo(nodoIns);

                        //Terminamos la ejecucion
                        return res;
                    }
                }

                /**
                 * VALIDACIONES PARA LA ETIQUETA CYCLE DENTRO DEL DO WHILE
                 */
                else if (res instanceof Cycle cy) {
                    if(cy.id.equalsIgnoreCase(this.id)){
                        //Detenemos este for
                        break;
                    }else{
                        //Detenemos este for y sigo enviando el Cycle

                        //Creamos el nodo para el AST
                        nodoPadre = new NodoAST("DO WHILE");
                        nodoPadre.agregarHijo(nodoCond);

                        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                        nodoIns.agregarHijos(instruccionEjec);
                        nodoPadre.agregarHijo(nodoIns);

                        //Terminamos la ejecucion
                        return res;
                    }
                }
            }
            /**
             * VALIDACION PARA EL DO WHILE ACTUAL Y ENVIO DE CYCLE
             */
            if (res instanceof Cycle cy){
                if(cy.id.equalsIgnoreCase(this.id)){
                    //Realizamos un continue a este while
                    continue;
                }else{
                    //Detenemos este while y sigo enviando el cycle

                    //Creamos el nodo para el AST
                    nodoPadre = new NodoAST("DO WHILE");
                    nodoPadre.agregarHijo(nodoCond);

                    NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
                    nodoIns.agregarHijos(instruccionEjec);
                    nodoPadre.agregarHijo(nodoIns);

                    //Terminamos la ejecucion
                    return res;
                }
            }

            //Creamos nodo para el AST
            instruccionEjec.add(nodoIteracion);

            /**
             * VALIDACION DE LA CONDICION PARA LA FINALIZACION DEL CICLO
             */
            condicionCiclo = this.condicion.execute(newTable, tree);

            //Creamos el nodo para el AST
            NodoAST nuevaCond = new NodoAST("CONDICION");
            nuevaCond.agregarHijo(this.condicion.getAST());
            instruccionEjec.add(nuevaCond);

            //Incrementamos la variable de paso i
            i += 1;
            if(i > 400){    //Valor por defecto para evitar el desborde de memoria
                break;
            }
        }

        //Creamos el nodo para el AST
        nodoPadre = new NodoAST("DO WHILE");
        nodoPadre.agregarHijo(nodoCond);

        NodoAST nodoIns = new NodoAST("INSTRUCCIONES");
        nodoIns.agregarHijos(instruccionEjec);
        nodoPadre.agregarHijo(nodoIns);

        //Terminamos la ejecucion
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("DO WHILE");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
}
