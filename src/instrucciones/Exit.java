package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

public class Exit extends Instruccion {
    String id;
    public Exit(String id, int line, int column) {
        super(null, line, column);
        this.id = id;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        return this;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }
    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("EXIT");

        if(!id.equalsIgnoreCase("!")){
            nodo.agregarHijo(new NodoAST(id));
        }

        return nodo;
    }
}
