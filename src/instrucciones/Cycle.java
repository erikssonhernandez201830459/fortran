package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

public class Cycle extends Instruccion {
    String id;
    public Cycle(String id, int line, int column) {
        super(null, line, column);
        this.id = id;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        return this;
    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("CYCLE");

        if(!id.equalsIgnoreCase("!")){
            nodo.agregarHijo(new NodoAST(id));
        }

        return nodo;
    }
}
