package instrucciones;

import Abstract.Instruccion;
import Abstract.NodoAST;
import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.*;

import java.util.ArrayList;

public class DeclaracionArrayAllocatable extends Instruccion {
    String id;
    boolean flag;

    NodoAST nodoPadre;
    public DeclaracionArrayAllocatable(Tipo.Type tipo, String id, boolean flag, int line, int column) {
        super(tipo, line, column);
        this.id = id;
        this.flag = flag;
    }

    @Override
    public java.lang.Object execute(SymbolTable table, Tree tree) {
        Symbol simbolo;
        Tipo nuevoTipo;
        Tipo nuevoTipo2;
        java.lang.Object result;

        /**
         * VALIDACION DE ARRAYS DE 1 O 2 DIMENSIONES
         * En base a la Flag que viene desde el Visitor
         */
        if(this.flag){
            result = new ArrayList<Instruccion>();

            nuevoTipo = new Tipo(this.type);
            nuevoTipo2 = new Tipo(Tipo.Type.ALLOCATE);
        }else{
            result = new ArrayList<ArrayList<Instruccion>>();

            nuevoTipo = new Tipo(this.type);
            nuevoTipo2 = new Tipo(Tipo.Type.ALLOCATE2);
        }

        /**
         * CREAMOS LA NUEVA VARIABLE EN LA TABLA DE SIMBOLOS
         */
        simbolo = new Symbol(nuevoTipo, nuevoTipo2, this.id, result, this.line, this.column, table);

        /**
         * VALIDACION DE EXISTENCIA
         * Verificamos si existe en la tabla de simbolos
         */
        if(table.getVariable(this.id) == null){
            table.setVariable(simbolo);
            tree.variables.add(simbolo);

            nodoPadre = new NodoAST("ALLOCATABLE");
            nodoPadre.agregarHijo(new NodoAST(this.type.toString()));
            nodoPadre.agregarHijo(new NodoAST(this.id));

            //Terminamos la ejecucion
            return null;
        }
        else{
            String descError = "La variable [" + this.id + "] ya ha sido declarada";
            Excepcion error = new Excepcion(Excepcion.ErrorTipo.Semantico, descError, line, column);
            tree.excepciones.add(error);
            //tree.consola.add(error.toString());
            return error;
        }

    }

    @Override
    public Quadrupla translate(QuadControlador controlador) {
        return null;
    }

    @Override
    public NodoAST getAST() {
        NodoAST nodo = new NodoAST("ALLOCATABLE");

        if(nodoPadre != null){
            return nodoPadre;
        }

        return nodo;
    }
}
