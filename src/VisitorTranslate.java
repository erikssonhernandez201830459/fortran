import Abstract.Instruccion;
import Traductor.Entorno3D;
import Traductor.Node;
import Traductor.NodeValue;
import entorno.Default;
import entorno.Tipo;
import expresiones.Identificador;
import expresiones.Logica;
import grammar.*;
import instrucciones.Asignacion;
import instrucciones.Declaracion;
import instrucciones.If;
import instrucciones.ListaDeclaraciones;
import org.antlr.v4.codegen.model.ExceptionClause;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class VisitorTranslate extends GrammarBaseVisitor {
    int contadorTemporales = 0;
    int contadorLabels = 0;
    Stack<Entorno3D> entornos = new Stack<>();
    public String codigo3D = "";

    public String generateC3D(){
        String encabezado = ""+
                "#include <stdio.h>\n" +
                "#include <math.h>\n" +
                "double heap[30101999];\n" +
                "double stack[30101999];\n"+
                "double P;\n";
        String declaracionTemporales = "double ";
        for(int i = 1; i <= contadorTemporales; i++){
            declaracionTemporales += "t" + i + ((i == contadorTemporales)? ";\n": ", ");
        }

        return encabezado + ((contadorTemporales > 0)? declaracionTemporales: "") + codigo3D;
    }

    public void add3D(String code){
        this.codigo3D += code + "\n";
    }

    @Override
    public Object visitInstruccionesProgram(GrammarParser.InstruccionesProgramContext ctx) {
        add3D("void main() {");
        add3D("P = 0;");
        Entorno3D nuevoEntorno = new Entorno3D(null);
        entornos.push(nuevoEntorno);

        for(ParseTree item: ctx.e){
            visit(item);
        }
        entornos.pop();
        add3D("return ;\n");
        add3D("}");
        return true;
    }
    @Override
    public Object visitInstruccionesPrint(GrammarParser.InstruccionesPrintContext ctx) {
        return visit(ctx.print());
    }

    @Override
    public Object visitPrint(GrammarParser.PrintContext ctx) {
        List<GrammarParser.ExpresionContext> exprContextList = ctx.expresion();

        for(GrammarParser.ExpresionContext contextoExpresion: exprContextList){
            NodeValue nodeValue = (NodeValue) visit(contextoExpresion);
            /**
             *   VALIDACION DE TIPOS DE IMPRESION PARA TIPOS DE DATOS
             *
             */
            System.out.println(nodeValue.getType() +" - "+  nodeValue.getValue());
            if(nodeValue.getType().equals(Tipo.Type.INTEGER)) {
                add3D("\n/* PRINT - ENTEROS */");
                add3D("printf(\"%d\", (int)" + nodeValue.getValue() + ");");

            } else if(nodeValue.getType().equals(Tipo.Type.REAL)){
                add3D("\n/* PRINT - DOUBLE */");
                add3D("printf(\"%f\", (double)" + nodeValue.getValue() + ");");
            } else if(nodeValue.getType().equals(Tipo.Type.STRING)){
                add3D("\n/* PRINT - CADENAS */");
                for (char i : nodeValue.getValue().replace("\"", "").replace("\'", "").toCharArray())
                {

                     add3D("printf(\"%c\", (char)" +(int)i + ");");
                }
            } else if(nodeValue.getType().equals(Tipo.Type.CHARACTER)){
                    add3D("\n/* PRINT - CARACTERES */");
                for (int i=0; i<nodeValue.getValue().replace("\'", "").length() ; i++)
                {
                    add3D("printf(\"%c\", (char)" +(int)nodeValue.getValue().replace("\'", "").codePointAt(i) + ");");
                }
            } else if(nodeValue.getType().equals(Tipo.Type.LOGICAL)){
                    String truePrint =
                            "\tprintf(\"%c\", (char)116);\n" +//t
                                    "\tprintf(\"%c\", (char)114);\n" +//r
                                    "\tprintf(\"%c\", (char)117);\n" +//u
                                    "\tprintf(\"%c\", (char)101);\n";//e
                    String falsePrint =
                            "\tprintf(\"%c\", (char)102);\n" +//f
                                    "\tprintf(\"%c\", (char)97); \n" +//a
                                    "\tprintf(\"%c\", (char)108);\n" +//l
                                    "\tprintf(\"%c\", (char)115);\n" +//s
                                    "\tprintf(\"%c\", (char)101);\n" ;//e

                    add3D("\n/* PRINT - BOOLEANOS */");
                    add3D("L"+ ++contadorLabels + ":");     //L1
                    add3D("\tif("+nodeValue.getValue() +"== 1"+") goto L"+ ++contadorLabels+ ";");    //L2
                    add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
                    add3D("L"+ contadorLabels+":"); //L2
                    contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
                    add3D(truePrint);
                    add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
                    add3D("L"+ contadorLabels+":"); //L3
                    contadorLabels++;   //Vale 4
                    add3D(falsePrint);
                    add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
                    add3D("L"+ contadorLabels+":"); //L4


                }
            }

        add3D("printf(\"%c\", 10);");
        return true;
   }
    /**
     * CONDICIONAL IF NORMAL
     */
    @Override
    public Object visitIfNormal(GrammarParser.IfNormalContext ctx) {
        NodeValue condicion = (NodeValue) visit(ctx.cond);

        int labelSalida=0, labelTrue=0, labelFalse = 0;
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* CONDICIONAL IF */");
        add3D("L"+ ++contadorLabels + ":");     //L1
            add3D("\tif("+condicion.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
            labelSalida = contadorLabels+1;
            add3D("\tgoto L"+(labelSalida) + ";"); //goto L3 (2+1)

        add3D("L"+ contadorLabels+":"); //L2 // CONDICIONAL TRUE
            contadorLabels++; //Aumentamos del Goto anterior    //Vale 3

            //ACA VAN LAS INSTRUCCIONES A EJECUTARSE
            entornos.push(new Entorno3D(entornos.peek()));
            if(ctx.instrucciones() != null){
                for(ParseTree item: ctx.e){
                    visit(item);
                }
            }
        entornos.pop();
        add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L3

        add3D("L"+ labelSalida +":"); //L3
        contadorLabels++;
        return true;
        }

    @Override
    public Object visitIfElse(GrammarParser.IfElseContext ctx) {
        NodeValue condicion = (NodeValue) visit(ctx.cond);

        int labelSalida=0, labelIf,  labelElse = 0;
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* CONDICIONAL IF */");
        add3D("L"+ ++contadorLabels + ":");     //L1
            add3D("\tif("+condicion.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
            labelIf = contadorLabels;
            labelElse = contadorLabels+1;
            add3D("\tgoto L"+(labelElse) + ";"); //goto L3 (2+1)

        add3D("L"+ labelIf+":"); //L2 // CONDICIONAL TRUE
            contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
            labelSalida = contadorLabels+1;
            contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA
                /**
                 *  ACA VAN LAS INSTRUCCIONES IF
                 */
                entornos.push(new Entorno3D(entornos.peek()));
                if(ctx.instrucciones() != null){
                    for(ParseTree item: ctx.e){
                        visit(item);
                    }
                }
                entornos.pop();
            add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L4
            contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA

        add3D("\n/* CONDICIONAL ELSE */");
        add3D("L"+ labelElse+":"); //L3
            /**
             *  ACA VAN LAS INSTRUCCIONES ELSE
             */

            entornos.push(new Entorno3D(entornos.peek()));
            if(ctx.instrucciones() != null){
                for(ParseTree item: ctx.e2){
                    visit(item);
                }
            }
            entornos.pop();
            add3D("\tgoto L"+(labelSalida) + ";"); //L4 (Ya no es necesario sumar)

        //SALIDA DEL CICLO
        add3D("L"+ labelSalida+":"); //L4
        contadorLabels++;

        return true;    }

    @Override
    public Object visitIfElseIfElse(GrammarParser.IfElseIfElseContext ctx) {
        NodeValue condicion = (NodeValue) visit(ctx.cond);

        int labelSalida=0, labelIf,  labelElse = 0;
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* CONDICIONAL IF */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+condicion.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
        labelIf = contadorLabels;
        labelElse = contadorLabels+1;
        add3D("\tgoto L"+(labelElse) + ";"); //goto L3 (2+1)

        add3D("L"+ labelIf+":"); //L2 // CONDICIONAL TRUE
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        labelSalida = contadorLabels+1;
        contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA
        /**
         *  ACA VAN LAS INSTRUCCIONES IF
         */
        entornos.push(new Entorno3D(entornos.peek()));
        if(ctx.instrucciones() != null){
            for(ParseTree item: ctx.e){
                visit(item);
            }
        }
        entornos.pop();
        add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L4
        contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA

        add3D("\n/* CONDICIONAL IF ELSE */");
        add3D("L"+ ++contadorLabels+":"); //L3
        /**
         *  ACA VAN LAS INSTRUCCIONES ELSE
         */

        entornos.push(new Entorno3D(entornos.peek()));
        if(ctx.instrucciones() != null){
            for(ParseTree item: ctx.e2){
                visit(item);
            }
        }
        entornos.pop();
        add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L4
        contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA

        add3D("\n/* CONDICIONAL ELSE */");
        add3D("L"+ labelElse+":"); //L3
        /**
         *  ACA VAN LAS INSTRUCCIONES ELSE
         */

        entornos.push(new Entorno3D(entornos.peek()));
        if(ctx.instrucciones() != null){
            for(ParseTree item: ctx.e3){
                visit(item);
            }
        }
        entornos.pop();
        add3D("\tgoto L"+(labelSalida) + ";"); //L4 (Ya no es necesario sumar)

        //SALIDA DEL CICLO
        add3D("L"+ labelSalida+":"); //L4
        contadorLabels++;

        return true;     }
    @Override
    public Object visitIfElseIf(GrammarParser.IfElseIfContext ctx) {
            NodeValue condicion = (NodeValue) visit(ctx.cond);

            int labelSalida=0, labelIf,  labelElse = 0;
            Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
            /**
             * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
             */
            add3D("\n/* CONDICIONAL IF */");
            add3D("L"+ ++contadorLabels + ":");     //L1
            add3D("\tif("+condicion.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
            labelIf = contadorLabels;
            labelElse = contadorLabels+1;
            add3D("\tgoto L"+(labelElse) + ";"); //goto L3 (2+1)

            add3D("L"+ labelIf+":"); //L2 // CONDICIONAL TRUE
            contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
            labelSalida = contadorLabels+1;
            contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA
            /**
             *  ACA VAN LAS INSTRUCCIONES IF
             */
            entornos.push(new Entorno3D(entornos.peek()));
            if(ctx.instrucciones() != null){
                for(ParseTree item: ctx.e){
                    visit(item);
                }
            }
            entornos.pop();
            add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L4
            contadorLabels++;   //AUMENTAMOS EL VALOR PARA LA SALIDA

            add3D("L"+ labelElse+":"); //L3
            /**
             *  ACA VAN LAS INSTRUCCIONES ELSE
             */
            entornos.push(new Entorno3D(entornos.peek()));
            if(ctx.instrucciones() != null){
                for(ParseTree item: ctx.e2){
                    visit(item);
                }
            }
            entornos.pop();
            add3D("\tgoto L"+(labelSalida) + ";"); //L4 (Ya no es necesario sumar)

            //SALIDA DEL CICLO
            add3D("L"+ labelSalida+":"); //L4
            contadorLabels++;

            return true;
    }
    public Object visitElseif(GrammarParser.ElseifContext ctx) {
        NodeValue condicion = (NodeValue) visit(ctx.cond);

        int labelSalida=0, labelTrue=0, labelFalse = 0;
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* CONDICIONAL IF */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+condicion.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
        labelSalida = contadorLabels+1;
        add3D("\tgoto L"+(labelSalida) + ";"); //goto L3 (2+1)

        add3D("L"+ contadorLabels+":"); //L2 // CONDICIONAL TRUE
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3

        //ACA VAN LAS INSTRUCCIONES A EJECUTARSE
        entornos.push(new Entorno3D(entornos.peek()));
        if(ctx.instrucciones() != null){
            for(ParseTree item: ctx.e){
                visit(item);
            }
        }
        entornos.pop();
        add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L3

        add3D("L"+ labelSalida +":"); //L3
        contadorLabels++;
        return true;   }
    @Override
    public Object visitDeclaracionUniq(GrammarParser.DeclaracionUniqContext ctx) {
        List<GrammarParser.ListaDeclaracionContext> listadoContextosVariables = ctx.e;
        ArrayList<Node> variables = new ArrayList<>();
        //Nodo Principal que si viene
        Tipo.Type tipoIdentificador = Tipo.Type.INTEGER;
        tipoIdentificador = tipoIdentificador.toType(ctx.tipo().getText());
        Node node = new Node(ctx.id.getText(), ctx.tipo().getText(),Default.value3DString(ctx.tipo().getText()));
        variables.add(node);
        for(GrammarParser.ListaDeclaracionContext context: listadoContextosVariables){
            Node nodeAux = (Node) visit(context);
            variables.add(nodeAux);
        }

            String valueExpresion = "";
        for(Node nodeAux: variables){
            valueExpresion = nodeAux.getValue();
            System.out.println("**************");
            System.out.println(valueExpresion);
            if(nodeAux.getValue().equalsIgnoreCase(".true.")){
                    valueExpresion = "1";
                } else if(nodeAux.getValue().equalsIgnoreCase(".false.")){
                    valueExpresion = "0";
                } else if(nodeAux.getValue().isEmpty()){
                    valueExpresion = "'a'";
                }
            int posicion = this.entornos.peek().newElement(nodeAux.getId(), nodeAux);
            add3D("\n/* DECLARACION - "+nodeAux.getId()+" */");
            add3D("t" + ++contadorTemporales + " = P + " + posicion + ";");
            add3D("stack[(int)t" + contadorTemporales + "] = " + valueExpresion + ";");
        }

        return true;
    }

    @Override
    public Object visitDeclaracionAsig(GrammarParser.DeclaracionAsigContext ctx) {
        List<GrammarParser.ListaDeclaracionContext> listadoContextosVariables = ctx.e;
        NodeValue nodeValue = (NodeValue) visit(ctx.expresion());
        ArrayList<Node> variables = new ArrayList<>();
        //Nodo Principal que si viene
        Node node = new Node(ctx.id.getText(), ctx.tipo().getText(), nodeValue.getValue());
        variables.add(node);

        //Nodos de la Lista de declaracion
        for(GrammarParser.ListaDeclaracionContext context: listadoContextosVariables){
            Node nodeAux = (Node) visit(context);
            variables.add(nodeAux);
        }
        String valueExpresion = "";
        for(Node nodeAux: variables){
            valueExpresion = nodeAux.getValue();
            System.out.println("**************");
            System.out.println(valueExpresion);
            if(nodeAux.getValue().equalsIgnoreCase(".true.")){
                valueExpresion = "1";
            } else if(nodeAux.getValue().equalsIgnoreCase(".false.")){
                valueExpresion = "0";
            } else if(nodeAux.getValue().isEmpty()){
                valueExpresion = "'a'";
            }
            int posicion = this.entornos.peek().newElement(nodeAux.getId(), nodeAux);
            add3D("\n/* DECLARACION - "+nodeAux.getId()+" */");
            add3D("t" + ++contadorTemporales + " = P + " + posicion + ";");
            add3D("stack[(int)t" + contadorTemporales + "] = " + nodeAux.getValue() + ";");
        }

        return true;
    }

    /**
     * HIJOS DE LISTA DECLARACION
     * @param ctx the parse tree
     * @return
     */
    public Node visitListaDeclaracionAsig(GrammarParser.ListaDeclaracionAsigContext ctx) {
        String typeValue = "";


        try {
            typeValue = ((GrammarParser.DeclaracionAsigContext) ctx.parent).tipo().getText();
           } catch (Exception e){
            typeValue = ((GrammarParser.DeclaracionUniqContext) ctx.parent).tipo().getText();

        }
        System.out.println(typeValue);

        NodeValue nodeValue = (NodeValue) visit(ctx.expresion());
        Node node = new Node(ctx.id.getText(), typeValue, nodeValue.getValue());

        return node;
       }
    @Override
    public Node visitListaDeclaracionUniq(GrammarParser.ListaDeclaracionUniqContext ctx) {
        String typeValue = "";
            typeValue = ((GrammarParser.DeclaracionUniqContext) ctx.parent).tipo().getText();



        System.out.println("TIPO DECLARACION"+typeValue);

        //NodeValue nodeValue = (NodeValue) visit(ctx.expresion());
        Node node = new Node(ctx.id.getText(), typeValue, Default.value3D(Tipo.Type.INTEGER));

        return node;
    }
    /**
     * ASIGNACION DE VARIABLES
     */
    @Override
    public Object visitAsignacionId(GrammarParser.AsignacionIdContext ctx) {
        Node node = this.entornos.peek().searchElementByName(ctx.id.getText());
        NodeValue nodeValue = (NodeValue) visit(ctx.expresion());
        int posicion = node.getPosition();

        add3D("\n/* ASIGNACION - "+ctx.id.getText()+" */");
        add3D("t" + ++contadorTemporales + " = P +" + posicion + ";");
        add3D("stack[(int)t" + contadorTemporales + "] = " + nodeValue.getValue() + ";");

        return true;
    }
/**
 * ACCESO A IDENTIFICADOR
 */
@Override
public NodeValue visitExpresionIdentificador(GrammarParser.ExpresionIdentificadorContext ctx) {
   String id = ctx.IDEN().getText();
   Node node = this.entornos.peek().searchElementByName(id);
    add3D("t" + ++contadorTemporales + " = P + " + node.getPosition() + ";");
    add3D("t" + (contadorTemporales + 1) + " = stack[(int) t" + contadorTemporales + "  ];");
    contadorTemporales++;
    Tipo.Type tipoIdentificador = Tipo.Type.INTEGER;
    tipoIdentificador = tipoIdentificador.toType(node.getType());
    System.out.println("Tipo que viene"+ node.getType());
    System.out.println("Tipo Id: "+tipoIdentificador);
    return new NodeValue(tipoIdentificador, "t" + contadorTemporales);
}


/**
 * EXPRESIONES
 */

    /**
     * OPERACIONES RELACIONALES
     */

    @Override public NodeValue visitExpresionEq(GrammarParser.ExpresionEqContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* RELACIONAL - IGUAL QUE */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+valueIzq.getValue() +" == "+valueDer.getValue() + ") goto L"+ ++contadorLabels+ ";");    //L2
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
        add3D("L"+ contadorLabels+":"); //L2
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
        add3D("L"+ contadorLabels+":"); //L3
        contadorLabels++;   //Vale 4
        add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }
    @Override
    public NodeValue visitExpresionNe(GrammarParser.ExpresionNeContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* RELACIONAL - DIFERENTE QUE */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+valueIzq.getValue() +" != "+valueDer.getValue() + ") goto L"+ ++contadorLabels+ ";");    //L2
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
        add3D("L"+ contadorLabels+":"); //L2
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
        add3D("L"+ contadorLabels+":"); //L3
        contadorLabels++;   //Vale 4
        add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }

    @Override public NodeValue visitExpresionGe(GrammarParser.ExpresionGeContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* RELACIONAL - MAYOR IGUAL QUE */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+valueIzq.getValue() +" >= "+valueDer.getValue() + ") goto L"+ ++contadorLabels+ ";");    //L2
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
        add3D("L"+ contadorLabels+":"); //L2
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
        add3D("L"+ contadorLabels+":"); //L3
        contadorLabels++;   //Vale 4
        add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }
    @Override public NodeValue visitExpresionGt(GrammarParser.ExpresionGtContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* RELACIONAL - MAYOR QUE */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+valueIzq.getValue() +" > "+valueDer.getValue() + ") goto L"+ ++contadorLabels+ ";");    //L2
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
        add3D("L"+ contadorLabels+":"); //L2
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
        add3D("L"+ contadorLabels+":"); //L3
        contadorLabels++;   //Vale 4
        add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }

    @Override public NodeValue visitExpresionLe(GrammarParser.ExpresionLeContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* RELACIONAL - MENOR IGUAL QUE */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+valueIzq.getValue() +" <= "+valueDer.getValue() + ") goto L"+ ++contadorLabels+ ";");    //L2
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
        add3D("L"+ contadorLabels+":"); //L2
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
        add3D("L"+ contadorLabels+":"); //L3
        contadorLabels++;   //Vale 4
        add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);    }
    @Override
    public NodeValue visitExpresionLt(GrammarParser.ExpresionLtContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* RELACIONAL - MENOR QUE */");
        add3D("L"+ ++contadorLabels + ":");     //L1
        add3D("\tif("+valueIzq.getValue() +" < "+valueDer.getValue() + ") goto L"+ ++contadorLabels+ ";");    //L2
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L3 (2+1)
        add3D("L"+ contadorLabels+":"); //L2
        contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
        add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels+1) + ";"); //L4 (3+1)
        add3D("L"+ contadorLabels+":"); //L3
        contadorLabels++;   //Vale 4
        add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
        add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }

    /**
     * EXPRECIONES LOGICAS
     * @param ctx the parse tree
     * @return
     */
    @Override public NodeValue visitExpresionOr(GrammarParser.ExpresionOrContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        int labelSalida=0, labelTrue=0, labelFalse = 0;
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* LOGICAL - OR */");
        add3D("L"+ ++contadorLabels + ":");     //L1
            add3D("\tif("+valueIzq.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
            add3D("\tgoto L"+(contadorLabels+1) + ";"); //goto L3 (2+1)

        add3D("L"+ contadorLabels+":"); //L2
            labelTrue = contadorLabels;  //Guardamos la etiqueta L2 como LabelTrue
            add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
            contadorLabels++;   //Aumentamos el valor del contador para L3
            labelSalida = contadorLabels + 1;   //LABEL SALIDA = L4
            add3D("\tgoto L"+(labelSalida) + ";"); // goto Label Salida // L4

        add3D("L"+ contadorLabels+":"); //L3
            contadorLabels++;   //Aumentamos a L4
            labelFalse = contadorLabels + 1; //L5 o Label False L(4+1)
            add3D("\tif("+valueDer.getValue() +" == 1) goto L"+ labelTrue+ ";");    //if(t2 == 1) goto L2;
            add3D("\tgoto L"+(labelFalse) + ";"); // goto Label False // L5

        add3D("L"+ (labelFalse)+":"); //L5
            add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
            add3D("\tgoto L"+(labelSalida) + ";"); //L4 //Salida

        add3D("L"+ labelSalida+":"); //L4 //Label Salida
        contadorLabels ++; //Aumentamos el valor del Label a L5 para cuadrar con el label False
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);    }
    @Override public NodeValue visitExpresionNot(GrammarParser.ExpresionNotContext ctx) {
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        int labelSalida=0, labelTrue=0, labelFalse = 0;
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* LOGICAL - NOT */");
            add3D("L"+ ++contadorLabels + ":");     //L1
            add3D("\tif("+valueDer.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
            add3D("\tgoto L"+(contadorLabels+1) + ";"); //goto L3 (2+1)

        add3D("L"+ contadorLabels+":"); //L2 // New FALSE
            contadorLabels++; //Aumentamos del Goto anterior    //Vale 3
            add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";");   //temp +1 = t1 //NUEVO FALSE
            add3D("\tgoto L"+(contadorLabels + 1) + ";"); // goto Label Salida // L4 (3+1)

        add3D("L"+ contadorLabels+":"); //L3
            contadorLabels++;   //Vale 4
            add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";"); //temp +1 = t1 //NUEVO TRUE
            add3D("\tgoto L"+(contadorLabels) + ";"); //L4 (Ya no es necesario sumar)

        add3D("L"+ contadorLabels+":"); //L4
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }
    @Override public NodeValue visitExpresionAnd(GrammarParser.ExpresionAndContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.LOGICAL;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * CREACION DE CONDICIONALES DE VALIDACION CON ETIQUETAS
         */
        add3D("\n/* LOGICAL - AND */");
        add3D("L"+ ++contadorLabels + ":");     //L1
            add3D("\tif("+valueIzq.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t1 == 1) goto L2
            add3D("\tgoto L"+(contadorLabels+1) + ";"); //goto L3 (2+1)
            int labelFalse = contadorLabels+1;  //Guardamos la etiqueta L3 como Falsa
        add3D("L"+ contadorLabels+":"); //L2
            contadorLabels++;   //Aumentamos el valor del contador para L3
            add3D("\tif("+valueDer.getValue() +" == 1) goto L"+ ++contadorLabels+ ";");    //if(t2 == 1) goto L4;
            add3D("\tgoto L"+(labelFalse) + ";"); // goto Label False // L3

        add3D("L"+ contadorLabels+":"); //L4
            add3D("\tt"+ (contadorTemporales + 1) + " = "+1+";");   //temp +1 = t1
            add3D("\tgoto L"+(++contadorLabels) + ";"); //L5 //Salida
        add3D("L"+ labelFalse+":"); //L3 //Label False
            add3D("\tt"+ (contadorTemporales + 1) + " = "+0+";"); //temp +1 = t1
            add3D("\tgoto L"+(contadorLabels) + ";"); //L5 (Ya no es necesario sumar)
        add3D("L"+ contadorLabels+":"); //L5 // Salida
        contadorTemporales++;//Aumento el valor del Temporal
        return new NodeValue(tipoExpresion, "t"+contadorTemporales);
    }
    /**
     * EXPRESIONES ARITMETICAS
     */
    @Override
    public NodeValue visitExpresionSuma(GrammarParser.ExpresionSumaContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.INTEGER;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * VALIDACION DE CASTEOS IMPLICITOS
         */
        add3D("\n/* ARITMETICA - SUMA */");
        add3D("t"+ ++contadorTemporales + " = "+ valueIzq.getValue() + " + "+ valueDer.getValue() + ";");
        if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.INTEGER)){
            tipoExpresion = Tipo.Type.INTEGER;
        } else if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.REAL)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.INTEGER)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.REAL)){
            tipoExpresion = Tipo.Type.REAL;
        }
            return new NodeValue(tipoExpresion,"t"+contadorTemporales);
    }

    @Override
    public NodeValue visitExpresionResta(GrammarParser.ExpresionRestaContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.INTEGER;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * VALIDACION DE CASTEOS IMPLICITOS
         */
        add3D("\n/* ARITMETICA - RESTA */");
        add3D("t"+ ++contadorTemporales + " = "+ valueIzq.getValue() + " - "+ valueDer.getValue() + ";");
        if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.INTEGER)){
            tipoExpresion = Tipo.Type.INTEGER;
        } else if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.REAL)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.INTEGER)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.REAL)){
            tipoExpresion = Tipo.Type.REAL;
        }
        return new NodeValue(tipoExpresion,"t"+contadorTemporales);
    }
    @Override
    public NodeValue visitExpresionMultiplicacion(GrammarParser.ExpresionMultiplicacionContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.INTEGER;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * VALIDACION DE CASTEOS IMPLICITOS
         */
        add3D("\n/* ARITMETICA - MULTIPLICACION */");
        add3D("t"+ ++contadorTemporales + " = "+ valueIzq.getValue() + " * "+ valueDer.getValue() + ";");
        if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.INTEGER)){
            tipoExpresion = Tipo.Type.INTEGER;
        } else if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.REAL)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.INTEGER)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.REAL)){
            tipoExpresion = Tipo.Type.REAL;
        }
        return new NodeValue(tipoExpresion,"t"+contadorTemporales);
    }
    @Override
    public NodeValue visitExpresionDivision(GrammarParser.ExpresionDivisionContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.INTEGER;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * VALIDACION DE CASTEOS IMPLICITOS
         */

        add3D("\n/* ARITMETICA - DIVISION */");
        add3D("t"+ ++contadorTemporales + " = "+ valueIzq.getValue() + " / "+ valueDer.getValue() + ";");
        if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.INTEGER)){
            tipoExpresion = Tipo.Type.INTEGER;
        } else if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.REAL)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.INTEGER)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.REAL)){
            tipoExpresion = Tipo.Type.REAL;
        }
        return new NodeValue(tipoExpresion,"t"+contadorTemporales);
    }

    @Override
    public NodeValue visitExpresionPotencia(GrammarParser.ExpresionPotenciaContext ctx) {
        NodeValue valueIzq = (NodeValue)  visit(ctx.izq);
        NodeValue valueDer = (NodeValue) visit(ctx.der);
        Tipo.Type tipoExpresion = Tipo.Type.INTEGER;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * VALIDACION DE CASTEOS IMPLICITOS
         */
        add3D("\n/* ARITMETICA - POTENCIA */");
        add3D("t"+ ++contadorTemporales + " = pow("+ valueIzq.getValue() + ", "+ valueDer.getValue() + ");");
        if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.INTEGER)){
            tipoExpresion = Tipo.Type.INTEGER;
        } else if(valueIzq.getType().equals(Tipo.Type.INTEGER) && valueDer.getType().equals(Tipo.Type.REAL)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.INTEGER)) {
            tipoExpresion = Tipo.Type.REAL;
        } else if(valueIzq.getType().equals(Tipo.Type.REAL) && valueDer.getType().equals(Tipo.Type.REAL)){
            tipoExpresion = Tipo.Type.REAL;
        }
        return new NodeValue(tipoExpresion,"t"+contadorTemporales);       }
    public NodeValue visitExpresionNegativo(GrammarParser.ExpresionNegativoContext ctx) {
        NodeValue valueDer = (NodeValue)  visit(ctx.expresion());
        Tipo.Type tipoExpresion = Tipo.Type.INTEGER;//INTEGER POR DEFAULT PA QUE NO TRUENE
        /**
         * VALIDACION DE CASTEOS IMPLICITOS
         */
        add3D("\n/* ARITMETICA - MENOS UNARIO */");
        add3D("t"+ ++contadorTemporales + " =  - "+ valueDer.getValue() + ";");
        if(valueDer.getType().equals(Tipo.Type.INTEGER)){
            tipoExpresion = Tipo.Type.INTEGER;
        } else if(valueDer.getType().equals(Tipo.Type.REAL)) {
            tipoExpresion = Tipo.Type.REAL;
        }
        return new NodeValue(tipoExpresion,"t"+contadorTemporales);
    }
    /**
     * PRIMITIVOS TRADUCIDOS
     * @param ctx the parse tree
     * @return
     */
    @Override public NodeValue visitExpresionReal(GrammarParser.ExpresionRealContext ctx) {
        return new NodeValue(Tipo.Type.REAL, ctx.val.getText());
    }
    @Override public NodeValue visitExpresionInt(GrammarParser.ExpresionIntContext ctx) {
        return new NodeValue(Tipo.Type.INTEGER, ctx.val.getText());
    }
    @Override public NodeValue visitExpresionChar(GrammarParser.ExpresionCharContext ctx) {
        return new NodeValue(Tipo.Type.CHARACTER, ctx.val.getText());
    }
    @Override public NodeValue visitExpresionString(GrammarParser.ExpresionStringContext ctx) {
        return  new NodeValue(Tipo.Type.STRING, ctx.val.getText());
    }
    @Override public NodeValue visitExpresionTrue(GrammarParser.ExpresionTrueContext ctx) {
        return  new NodeValue(Tipo.Type.LOGICAL,"1");
    }
    @Override public NodeValue visitExpresionFalse(GrammarParser.ExpresionFalseContext ctx) {
        return new NodeValue(Tipo.Type.LOGICAL,"0");
    }
    @Override public Object visitExpresionParentesis(GrammarParser.ExpresionParentesisContext ctx) {
        return visit(ctx.expresion());
    }
    }
