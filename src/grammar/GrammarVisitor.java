// Generated from /home/erikssonherlo/Escritorio/Compi 2/FORTRAN/Grammar.g4 by ANTLR 4.10.1
package grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(GrammarParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#bloqueInstrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueInstrucciones(GrammarParser.BloqueInstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesProgram}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesProgram(GrammarParser.InstruccionesProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesSubrutina}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesSubrutina(GrammarParser.InstruccionesSubrutinaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesFuncion}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesFuncion(GrammarParser.InstruccionesFuncionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaParametrosNormal}
	 * labeled alternative in {@link GrammarParser#listaParametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaParametrosNormal(GrammarParser.ListaParametrosNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaParametrosComa}
	 * labeled alternative in {@link GrammarParser#listaParametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaParametrosComa(GrammarParser.ListaParametrosComaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaDeclaracionParametrosNormal}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeclaracionParametrosNormal(GrammarParser.ListaDeclaracionParametrosNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaDeclaracionParametrosArray2Dim}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeclaracionParametrosArray2Dim(GrammarParser.ListaDeclaracionParametrosArray2DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaDeclaracionParametrosArray1Dim}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeclaracionParametrosArray1Dim(GrammarParser.ListaDeclaracionParametrosArray1DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesDeclaracion}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesDeclaracion(GrammarParser.InstruccionesDeclaracionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesAsignacion}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesAsignacion(GrammarParser.InstruccionesAsignacionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesAsignacionA}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesAsignacionA(GrammarParser.InstruccionesAsignacionAContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesAllocate}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesAllocate(GrammarParser.InstruccionesAllocateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesPrint}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesPrint(GrammarParser.InstruccionesPrintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesIf}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesIf(GrammarParser.InstruccionesIfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesDo}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesDo(GrammarParser.InstruccionesDoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesExit}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesExit(GrammarParser.InstruccionesExitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesCycle}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesCycle(GrammarParser.InstruccionesCycleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code instruccionesCall}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruccionesCall(GrammarParser.InstruccionesCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionArray2Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionArray2Dim(GrammarParser.DeclaracionArray2DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionArray1Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionArray1Dim(GrammarParser.DeclaracionArray1DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionAllocatable2Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionAllocatable2Dim(GrammarParser.DeclaracionAllocatable2DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionAllocatable1Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionAllocatable1Dim(GrammarParser.DeclaracionAllocatable1DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionArray2Dim2}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionArray2Dim2(GrammarParser.DeclaracionArray2Dim2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionArray1Dim2}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionArray1Dim2(GrammarParser.DeclaracionArray1Dim2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionAsig}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionAsig(GrammarParser.DeclaracionAsigContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracionUniq}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracionUniq(GrammarParser.DeclaracionUniqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaDeclaracionAsig}
	 * labeled alternative in {@link GrammarParser#listaDeclaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeclaracionAsig(GrammarParser.ListaDeclaracionAsigContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaDeclaracionUniq}
	 * labeled alternative in {@link GrammarParser#listaDeclaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeclaracionUniq(GrammarParser.ListaDeclaracionUniqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacionId}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacionId(GrammarParser.AsignacionIdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacionArray1}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacionArray1(GrammarParser.AsignacionArray1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacionArray2}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacionArray2(GrammarParser.AsignacionArray2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacionArray1D}
	 * labeled alternative in {@link GrammarParser#asignacionA}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacionArray1D(GrammarParser.AsignacionArray1DContext ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacionArray2D}
	 * labeled alternative in {@link GrammarParser#asignacionA}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacionArray2D(GrammarParser.AsignacionArray2DContext ctx);
	/**
	 * Visit a parse tree produced by the {@code allocate2Dim}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllocate2Dim(GrammarParser.Allocate2DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code allocate1Dim}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllocate1Dim(GrammarParser.Allocate1DimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code deallocate}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeallocate(GrammarParser.DeallocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(GrammarParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifNormal}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfNormal(GrammarParser.IfNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifElse}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfElse(GrammarParser.IfElseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifElseIf}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfElseIf(GrammarParser.IfElseIfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifElseIfElse}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfElseIfElse(GrammarParser.IfElseIfElseContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#elseif}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseif(GrammarParser.ElseifContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doNormal}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoNormal(GrammarParser.DoNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doNormalSinPaso}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoNormalSinPaso(GrammarParser.DoNormalSinPasoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doEtiqueta}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoEtiqueta(GrammarParser.DoEtiquetaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doEtiquetaSinPaso}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoEtiquetaSinPaso(GrammarParser.DoEtiquetaSinPasoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doWhile}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhile(GrammarParser.DoWhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doWhileEtiqueta}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhileEtiqueta(GrammarParser.DoWhileEtiquetaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exitNormal}
	 * labeled alternative in {@link GrammarParser#exit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExitNormal(GrammarParser.ExitNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exitEtiqueta}
	 * labeled alternative in {@link GrammarParser#exit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExitEtiqueta(GrammarParser.ExitEtiquetaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cycleNormal}
	 * labeled alternative in {@link GrammarParser#cycle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCycleNormal(GrammarParser.CycleNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cycleEtiqueta}
	 * labeled alternative in {@link GrammarParser#cycle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCycleEtiqueta(GrammarParser.CycleEtiquetaContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(GrammarParser.CallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaCallNormal}
	 * labeled alternative in {@link GrammarParser#listaCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaCallNormal(GrammarParser.ListaCallNormalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaCallComa}
	 * labeled alternative in {@link GrammarParser#listaCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaCallComa(GrammarParser.ListaCallComaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionFalse}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionFalse(GrammarParser.ExpresionFalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionFuncion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionFuncion(GrammarParser.ExpresionFuncionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionLt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionLt(GrammarParser.ExpresionLtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionResta}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionResta(GrammarParser.ExpresionRestaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionParentesis}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionParentesis(GrammarParser.ExpresionParentesisContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionSize}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionSize(GrammarParser.ExpresionSizeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionInt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionInt(GrammarParser.ExpresionIntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionNegativo}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionNegativo(GrammarParser.ExpresionNegativoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionMultiplicacion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionMultiplicacion(GrammarParser.ExpresionMultiplicacionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionDivision}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionDivision(GrammarParser.ExpresionDivisionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionIdentificador}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionIdentificador(GrammarParser.ExpresionIdentificadorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionChar}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionChar(GrammarParser.ExpresionCharContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionTrue}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionTrue(GrammarParser.ExpresionTrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionOr}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionOr(GrammarParser.ExpresionOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionArray2}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionArray2(GrammarParser.ExpresionArray2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionNot}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionNot(GrammarParser.ExpresionNotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionArray1}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionArray1(GrammarParser.ExpresionArray1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionReal}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionReal(GrammarParser.ExpresionRealContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionGe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionGe(GrammarParser.ExpresionGeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionNe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionNe(GrammarParser.ExpresionNeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionLe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionLe(GrammarParser.ExpresionLeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionSuma}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionSuma(GrammarParser.ExpresionSumaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionAnd}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionAnd(GrammarParser.ExpresionAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionListaExpresion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionListaExpresion(GrammarParser.ExpresionListaExpresionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionPotencia}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionPotencia(GrammarParser.ExpresionPotenciaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionString}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionString(GrammarParser.ExpresionStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionEq}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionEq(GrammarParser.ExpresionEqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionGt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionGt(GrammarParser.ExpresionGtContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#listaExpresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaExpresion(GrammarParser.ListaExpresionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tipoInteger}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoInteger(GrammarParser.TipoIntegerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tipoReal}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoReal(GrammarParser.TipoRealContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tipoComplex}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoComplex(GrammarParser.TipoComplexContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tipoCharacter}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoCharacter(GrammarParser.TipoCharacterContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tipoLogical}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoLogical(GrammarParser.TipoLogicalContext ctx);
}