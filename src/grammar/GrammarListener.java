// Generated from /home/erikssonherlo/Escritorio/Compi 2/FORTRAN/Grammar.g4 by ANTLR 4.10.1
package grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(GrammarParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(GrammarParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#bloqueInstrucciones}.
	 * @param ctx the parse tree
	 */
	void enterBloqueInstrucciones(GrammarParser.BloqueInstruccionesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#bloqueInstrucciones}.
	 * @param ctx the parse tree
	 */
	void exitBloqueInstrucciones(GrammarParser.BloqueInstruccionesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesProgram}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesProgram(GrammarParser.InstruccionesProgramContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesProgram}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesProgram(GrammarParser.InstruccionesProgramContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesSubrutina}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesSubrutina(GrammarParser.InstruccionesSubrutinaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesSubrutina}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesSubrutina(GrammarParser.InstruccionesSubrutinaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesFuncion}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesFuncion(GrammarParser.InstruccionesFuncionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesFuncion}
	 * labeled alternative in {@link GrammarParser#instruccionesPrincipales}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesFuncion(GrammarParser.InstruccionesFuncionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaParametrosNormal}
	 * labeled alternative in {@link GrammarParser#listaParametros}.
	 * @param ctx the parse tree
	 */
	void enterListaParametrosNormal(GrammarParser.ListaParametrosNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaParametrosNormal}
	 * labeled alternative in {@link GrammarParser#listaParametros}.
	 * @param ctx the parse tree
	 */
	void exitListaParametrosNormal(GrammarParser.ListaParametrosNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaParametrosComa}
	 * labeled alternative in {@link GrammarParser#listaParametros}.
	 * @param ctx the parse tree
	 */
	void enterListaParametrosComa(GrammarParser.ListaParametrosComaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaParametrosComa}
	 * labeled alternative in {@link GrammarParser#listaParametros}.
	 * @param ctx the parse tree
	 */
	void exitListaParametrosComa(GrammarParser.ListaParametrosComaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaDeclaracionParametrosNormal}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 */
	void enterListaDeclaracionParametrosNormal(GrammarParser.ListaDeclaracionParametrosNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaDeclaracionParametrosNormal}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 */
	void exitListaDeclaracionParametrosNormal(GrammarParser.ListaDeclaracionParametrosNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaDeclaracionParametrosArray2Dim}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 */
	void enterListaDeclaracionParametrosArray2Dim(GrammarParser.ListaDeclaracionParametrosArray2DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaDeclaracionParametrosArray2Dim}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 */
	void exitListaDeclaracionParametrosArray2Dim(GrammarParser.ListaDeclaracionParametrosArray2DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaDeclaracionParametrosArray1Dim}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 */
	void enterListaDeclaracionParametrosArray1Dim(GrammarParser.ListaDeclaracionParametrosArray1DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaDeclaracionParametrosArray1Dim}
	 * labeled alternative in {@link GrammarParser#listaDeclaracionParametros}.
	 * @param ctx the parse tree
	 */
	void exitListaDeclaracionParametrosArray1Dim(GrammarParser.ListaDeclaracionParametrosArray1DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesDeclaracion}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesDeclaracion(GrammarParser.InstruccionesDeclaracionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesDeclaracion}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesDeclaracion(GrammarParser.InstruccionesDeclaracionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesAsignacion}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesAsignacion(GrammarParser.InstruccionesAsignacionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesAsignacion}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesAsignacion(GrammarParser.InstruccionesAsignacionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesAsignacionA}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesAsignacionA(GrammarParser.InstruccionesAsignacionAContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesAsignacionA}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesAsignacionA(GrammarParser.InstruccionesAsignacionAContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesAllocate}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesAllocate(GrammarParser.InstruccionesAllocateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesAllocate}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesAllocate(GrammarParser.InstruccionesAllocateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesPrint}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesPrint(GrammarParser.InstruccionesPrintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesPrint}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesPrint(GrammarParser.InstruccionesPrintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesIf}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesIf(GrammarParser.InstruccionesIfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesIf}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesIf(GrammarParser.InstruccionesIfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesDo}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesDo(GrammarParser.InstruccionesDoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesDo}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesDo(GrammarParser.InstruccionesDoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesExit}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesExit(GrammarParser.InstruccionesExitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesExit}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesExit(GrammarParser.InstruccionesExitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesCycle}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesCycle(GrammarParser.InstruccionesCycleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesCycle}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesCycle(GrammarParser.InstruccionesCycleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code instruccionesCall}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstruccionesCall(GrammarParser.InstruccionesCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code instruccionesCall}
	 * labeled alternative in {@link GrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstruccionesCall(GrammarParser.InstruccionesCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionArray2Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionArray2Dim(GrammarParser.DeclaracionArray2DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionArray2Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionArray2Dim(GrammarParser.DeclaracionArray2DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionArray1Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionArray1Dim(GrammarParser.DeclaracionArray1DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionArray1Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionArray1Dim(GrammarParser.DeclaracionArray1DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionAllocatable2Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionAllocatable2Dim(GrammarParser.DeclaracionAllocatable2DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionAllocatable2Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionAllocatable2Dim(GrammarParser.DeclaracionAllocatable2DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionAllocatable1Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionAllocatable1Dim(GrammarParser.DeclaracionAllocatable1DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionAllocatable1Dim}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionAllocatable1Dim(GrammarParser.DeclaracionAllocatable1DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionArray2Dim2}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionArray2Dim2(GrammarParser.DeclaracionArray2Dim2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionArray2Dim2}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionArray2Dim2(GrammarParser.DeclaracionArray2Dim2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionArray1Dim2}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionArray1Dim2(GrammarParser.DeclaracionArray1Dim2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionArray1Dim2}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionArray1Dim2(GrammarParser.DeclaracionArray1Dim2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionAsig}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionAsig(GrammarParser.DeclaracionAsigContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionAsig}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionAsig(GrammarParser.DeclaracionAsigContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracionUniq}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracionUniq(GrammarParser.DeclaracionUniqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracionUniq}
	 * labeled alternative in {@link GrammarParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracionUniq(GrammarParser.DeclaracionUniqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaDeclaracionAsig}
	 * labeled alternative in {@link GrammarParser#listaDeclaracion}.
	 * @param ctx the parse tree
	 */
	void enterListaDeclaracionAsig(GrammarParser.ListaDeclaracionAsigContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaDeclaracionAsig}
	 * labeled alternative in {@link GrammarParser#listaDeclaracion}.
	 * @param ctx the parse tree
	 */
	void exitListaDeclaracionAsig(GrammarParser.ListaDeclaracionAsigContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaDeclaracionUniq}
	 * labeled alternative in {@link GrammarParser#listaDeclaracion}.
	 * @param ctx the parse tree
	 */
	void enterListaDeclaracionUniq(GrammarParser.ListaDeclaracionUniqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaDeclaracionUniq}
	 * labeled alternative in {@link GrammarParser#listaDeclaracion}.
	 * @param ctx the parse tree
	 */
	void exitListaDeclaracionUniq(GrammarParser.ListaDeclaracionUniqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacionId}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacionId(GrammarParser.AsignacionIdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacionId}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacionId(GrammarParser.AsignacionIdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacionArray1}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacionArray1(GrammarParser.AsignacionArray1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacionArray1}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacionArray1(GrammarParser.AsignacionArray1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacionArray2}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacionArray2(GrammarParser.AsignacionArray2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacionArray2}
	 * labeled alternative in {@link GrammarParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacionArray2(GrammarParser.AsignacionArray2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacionArray1D}
	 * labeled alternative in {@link GrammarParser#asignacionA}.
	 * @param ctx the parse tree
	 */
	void enterAsignacionArray1D(GrammarParser.AsignacionArray1DContext ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacionArray1D}
	 * labeled alternative in {@link GrammarParser#asignacionA}.
	 * @param ctx the parse tree
	 */
	void exitAsignacionArray1D(GrammarParser.AsignacionArray1DContext ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacionArray2D}
	 * labeled alternative in {@link GrammarParser#asignacionA}.
	 * @param ctx the parse tree
	 */
	void enterAsignacionArray2D(GrammarParser.AsignacionArray2DContext ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacionArray2D}
	 * labeled alternative in {@link GrammarParser#asignacionA}.
	 * @param ctx the parse tree
	 */
	void exitAsignacionArray2D(GrammarParser.AsignacionArray2DContext ctx);
	/**
	 * Enter a parse tree produced by the {@code allocate2Dim}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 */
	void enterAllocate2Dim(GrammarParser.Allocate2DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code allocate2Dim}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 */
	void exitAllocate2Dim(GrammarParser.Allocate2DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code allocate1Dim}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 */
	void enterAllocate1Dim(GrammarParser.Allocate1DimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code allocate1Dim}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 */
	void exitAllocate1Dim(GrammarParser.Allocate1DimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code deallocate}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 */
	void enterDeallocate(GrammarParser.DeallocateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code deallocate}
	 * labeled alternative in {@link GrammarParser#allocate}.
	 * @param ctx the parse tree
	 */
	void exitDeallocate(GrammarParser.DeallocateContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(GrammarParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(GrammarParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifNormal}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void enterIfNormal(GrammarParser.IfNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifNormal}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void exitIfNormal(GrammarParser.IfNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifElse}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void enterIfElse(GrammarParser.IfElseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifElse}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void exitIfElse(GrammarParser.IfElseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifElseIf}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void enterIfElseIf(GrammarParser.IfElseIfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifElseIf}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void exitIfElseIf(GrammarParser.IfElseIfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifElseIfElse}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void enterIfElseIfElse(GrammarParser.IfElseIfElseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifElseIfElse}
	 * labeled alternative in {@link GrammarParser#if}.
	 * @param ctx the parse tree
	 */
	void exitIfElseIfElse(GrammarParser.IfElseIfElseContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#elseif}.
	 * @param ctx the parse tree
	 */
	void enterElseif(GrammarParser.ElseifContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#elseif}.
	 * @param ctx the parse tree
	 */
	void exitElseif(GrammarParser.ElseifContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doNormal}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void enterDoNormal(GrammarParser.DoNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doNormal}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void exitDoNormal(GrammarParser.DoNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doNormalSinPaso}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void enterDoNormalSinPaso(GrammarParser.DoNormalSinPasoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doNormalSinPaso}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void exitDoNormalSinPaso(GrammarParser.DoNormalSinPasoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doEtiqueta}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void enterDoEtiqueta(GrammarParser.DoEtiquetaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doEtiqueta}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void exitDoEtiqueta(GrammarParser.DoEtiquetaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doEtiquetaSinPaso}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void enterDoEtiquetaSinPaso(GrammarParser.DoEtiquetaSinPasoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doEtiquetaSinPaso}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void exitDoEtiquetaSinPaso(GrammarParser.DoEtiquetaSinPasoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doWhile}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void enterDoWhile(GrammarParser.DoWhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doWhile}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void exitDoWhile(GrammarParser.DoWhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doWhileEtiqueta}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileEtiqueta(GrammarParser.DoWhileEtiquetaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doWhileEtiqueta}
	 * labeled alternative in {@link GrammarParser#do}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileEtiqueta(GrammarParser.DoWhileEtiquetaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exitNormal}
	 * labeled alternative in {@link GrammarParser#exit}.
	 * @param ctx the parse tree
	 */
	void enterExitNormal(GrammarParser.ExitNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exitNormal}
	 * labeled alternative in {@link GrammarParser#exit}.
	 * @param ctx the parse tree
	 */
	void exitExitNormal(GrammarParser.ExitNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exitEtiqueta}
	 * labeled alternative in {@link GrammarParser#exit}.
	 * @param ctx the parse tree
	 */
	void enterExitEtiqueta(GrammarParser.ExitEtiquetaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exitEtiqueta}
	 * labeled alternative in {@link GrammarParser#exit}.
	 * @param ctx the parse tree
	 */
	void exitExitEtiqueta(GrammarParser.ExitEtiquetaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code cycleNormal}
	 * labeled alternative in {@link GrammarParser#cycle}.
	 * @param ctx the parse tree
	 */
	void enterCycleNormal(GrammarParser.CycleNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code cycleNormal}
	 * labeled alternative in {@link GrammarParser#cycle}.
	 * @param ctx the parse tree
	 */
	void exitCycleNormal(GrammarParser.CycleNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code cycleEtiqueta}
	 * labeled alternative in {@link GrammarParser#cycle}.
	 * @param ctx the parse tree
	 */
	void enterCycleEtiqueta(GrammarParser.CycleEtiquetaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code cycleEtiqueta}
	 * labeled alternative in {@link GrammarParser#cycle}.
	 * @param ctx the parse tree
	 */
	void exitCycleEtiqueta(GrammarParser.CycleEtiquetaContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#call}.
	 * @param ctx the parse tree
	 */
	void enterCall(GrammarParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#call}.
	 * @param ctx the parse tree
	 */
	void exitCall(GrammarParser.CallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaCallNormal}
	 * labeled alternative in {@link GrammarParser#listaCall}.
	 * @param ctx the parse tree
	 */
	void enterListaCallNormal(GrammarParser.ListaCallNormalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaCallNormal}
	 * labeled alternative in {@link GrammarParser#listaCall}.
	 * @param ctx the parse tree
	 */
	void exitListaCallNormal(GrammarParser.ListaCallNormalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listaCallComa}
	 * labeled alternative in {@link GrammarParser#listaCall}.
	 * @param ctx the parse tree
	 */
	void enterListaCallComa(GrammarParser.ListaCallComaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listaCallComa}
	 * labeled alternative in {@link GrammarParser#listaCall}.
	 * @param ctx the parse tree
	 */
	void exitListaCallComa(GrammarParser.ListaCallComaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionFalse}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionFalse(GrammarParser.ExpresionFalseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionFalse}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionFalse(GrammarParser.ExpresionFalseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionFuncion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionFuncion(GrammarParser.ExpresionFuncionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionFuncion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionFuncion(GrammarParser.ExpresionFuncionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionLt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionLt(GrammarParser.ExpresionLtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionLt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionLt(GrammarParser.ExpresionLtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionResta}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionResta(GrammarParser.ExpresionRestaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionResta}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionResta(GrammarParser.ExpresionRestaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionParentesis}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionParentesis(GrammarParser.ExpresionParentesisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionParentesis}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionParentesis(GrammarParser.ExpresionParentesisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionSize}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionSize(GrammarParser.ExpresionSizeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionSize}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionSize(GrammarParser.ExpresionSizeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionInt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionInt(GrammarParser.ExpresionIntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionInt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionInt(GrammarParser.ExpresionIntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionNegativo}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionNegativo(GrammarParser.ExpresionNegativoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionNegativo}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionNegativo(GrammarParser.ExpresionNegativoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionMultiplicacion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionMultiplicacion(GrammarParser.ExpresionMultiplicacionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionMultiplicacion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionMultiplicacion(GrammarParser.ExpresionMultiplicacionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionDivision}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionDivision(GrammarParser.ExpresionDivisionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionDivision}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionDivision(GrammarParser.ExpresionDivisionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionIdentificador}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionIdentificador(GrammarParser.ExpresionIdentificadorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionIdentificador}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionIdentificador(GrammarParser.ExpresionIdentificadorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionChar}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionChar(GrammarParser.ExpresionCharContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionChar}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionChar(GrammarParser.ExpresionCharContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionTrue}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionTrue(GrammarParser.ExpresionTrueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionTrue}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionTrue(GrammarParser.ExpresionTrueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionOr}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionOr(GrammarParser.ExpresionOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionOr}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionOr(GrammarParser.ExpresionOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionArray2}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionArray2(GrammarParser.ExpresionArray2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionArray2}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionArray2(GrammarParser.ExpresionArray2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionNot}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionNot(GrammarParser.ExpresionNotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionNot}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionNot(GrammarParser.ExpresionNotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionArray1}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionArray1(GrammarParser.ExpresionArray1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionArray1}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionArray1(GrammarParser.ExpresionArray1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionReal}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionReal(GrammarParser.ExpresionRealContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionReal}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionReal(GrammarParser.ExpresionRealContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionGe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionGe(GrammarParser.ExpresionGeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionGe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionGe(GrammarParser.ExpresionGeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionNe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionNe(GrammarParser.ExpresionNeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionNe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionNe(GrammarParser.ExpresionNeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionLe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionLe(GrammarParser.ExpresionLeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionLe}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionLe(GrammarParser.ExpresionLeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionSuma}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionSuma(GrammarParser.ExpresionSumaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionSuma}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionSuma(GrammarParser.ExpresionSumaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionAnd}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionAnd(GrammarParser.ExpresionAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionAnd}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionAnd(GrammarParser.ExpresionAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionListaExpresion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionListaExpresion(GrammarParser.ExpresionListaExpresionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionListaExpresion}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionListaExpresion(GrammarParser.ExpresionListaExpresionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionPotencia}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionPotencia(GrammarParser.ExpresionPotenciaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionPotencia}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionPotencia(GrammarParser.ExpresionPotenciaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionString}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionString(GrammarParser.ExpresionStringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionString}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionString(GrammarParser.ExpresionStringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionEq}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionEq(GrammarParser.ExpresionEqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionEq}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionEq(GrammarParser.ExpresionEqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresionGt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionGt(GrammarParser.ExpresionGtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresionGt}
	 * labeled alternative in {@link GrammarParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionGt(GrammarParser.ExpresionGtContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#listaExpresion}.
	 * @param ctx the parse tree
	 */
	void enterListaExpresion(GrammarParser.ListaExpresionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#listaExpresion}.
	 * @param ctx the parse tree
	 */
	void exitListaExpresion(GrammarParser.ListaExpresionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tipoInteger}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipoInteger(GrammarParser.TipoIntegerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tipoInteger}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipoInteger(GrammarParser.TipoIntegerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tipoReal}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipoReal(GrammarParser.TipoRealContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tipoReal}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipoReal(GrammarParser.TipoRealContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tipoComplex}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipoComplex(GrammarParser.TipoComplexContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tipoComplex}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipoComplex(GrammarParser.TipoComplexContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tipoCharacter}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipoCharacter(GrammarParser.TipoCharacterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tipoCharacter}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipoCharacter(GrammarParser.TipoCharacterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tipoLogical}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipoLogical(GrammarParser.TipoLogicalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tipoLogical}
	 * labeled alternative in {@link GrammarParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipoLogical(GrammarParser.TipoLogicalContext ctx);
}