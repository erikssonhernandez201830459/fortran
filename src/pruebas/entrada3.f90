function Contains(tam, arreglo, elemento) result (flag)
    implicit none
    integer, intent(in) :: tam
    integer, intent(in) :: arreglo(tam)
    integer, intent(in) :: elemento
    logical :: flag

    integer :: i

    do i = 0, tam, 1
        if (arreglo[i] .eq. elemento) then
            flag = .true.
            exit
        end if
    end do

    if (flag .eq. .true.) then
        print *, 'En el arreglo ', arreglo, ' si existe el elemento ', elemento
    end if 
end function Contains

function encontrarHoraInicioVariable (horaIngreso, horaActual, frecuencia) result (horaInicio)
    implicit none
    integer, intent(in) :: horaIngreso
    integer, intent(in) :: horaActual
    integer, intent(in) :: frecuencia
    integer :: horaInicio, inicio9 = 8

    integer, dimension(4) :: horarios8
    integer :: horarios6(5) = (/ 10,16,22,4,10 /), horarios4(7)
    integer, dimension(9) :: horarios3 = (/9, 12, 15, 18, 21, 0, 3, 6, 9/)
    integer :: horarios2(12) = (/10, 12, 14, 16, 18, 20, 22, 0, 2, 4, 6, 10/)
    integer, dimension(6) :: inicio10

    horarios8 = (/10, 18, 2, 10/)
    horarios4 = (/10, 14, 18, 22, 2, 6, 10/)

    inicio10 = (/24, 12, 6, 4, 3, 2/)

    if (hora < 6 .and. hora > 0) then
        horaInicio = horaIngreso

    else if (hora <= 10 .and. hora >= 7) then 
        logical :: contiene = Contains(inicio10, frecuencia)
        if (contiene .eq. .true.) then
            horaInicio = 10
        else if (inicio9 == frecuencia) then
            horaInicio = 9
        else 
            horaInicio = 8
        end if
    else 
        if (frecuencia == 2) then
            logical :: contiene = Contains(horarios2, horaIngreso)
            if (contiene .eq. .true.) then 
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1, h <= size(horarios2), 1
                    if (horarios2[h] > horaIngreso) then
                        horaInicio = horarios2[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 4) then
            logical :: contiene = Contains(horarios4, horaIngreso)
            if (contiene .eq. .true.) then 
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1, h <= size(horarios4), 1
                    if (horarios4[h] > horaIngreso) then
                        horaInicio = horarios4[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 3) then
            logical :: contiene = Contains(horarios3, horaIngreso)
            if (contiene .eq. .true.) then 
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1, h <= size(horarios3), 1
                    if (horarios3[h] > horaIngreso) then
                        horaInicio = horarios3[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 6) then
            logical :: contiene = Contains(horarios6, horaIngreso)
            if (contiene .eq. .true.) then 
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1, h <= size(horarios6), 1
                    if (horarios6[h] > horaIngreso) then
                        horaInicio = horarios6[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 8) then
            if (Contains(horarios8, horaIngreso)) then
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1, h <= size(horarios8), 1
                    if (horarios8[h] > horaIngreso) then
                        horaInicio = horarios8[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 12) then
            if (horaIngreso > 10) then
                horaInicio = 22
            else
                horaInicio = 10
            endif
        else if (frecuencia == 24) then
            horaInicio = 10
        end if
    end if
  
end function encontrarHoraInicioVariable

subroutine sumarMatrices() 
    implicit none
    integer :: a(3,3)
    integer, dimension(3,3) :: B, c

    a[1][1] = 1
    a[1][2] = 3
    a[1][3] = 4

    a[2][1] = 2
    a[2][2] = 4
    a[2][3] = 3

    a[3][1] = 3
    a[3][2] = 4
    a[3][3] = 5

    b[1][1] = 1
    b[1][2] = 3
    B[1][3] = 4

    B[2][1] = 2
    b[2][2] = 4
    b[2][3] = 3

    b[3][1] = 1
    b[3][2] = 2
    b[3][3] = 4

    integer :: i, j

    do i = 1, i < 4, 1
        do j = 1, j < 4, 1
            c[i][j] = a[i][j] + b[i][j]
        end do
    end do

    print *, c

end subroutine    

function factorial() result (fact)
    integer :: i = 1,fact=1
    integer :: number=17
    
    
    do while (i <= number)
        fact=fact*i; 
        i = i + 1
    end do

    ! -288522240
    print *, "Factorial de ", number, " es: ", fact   
end function factorial


program calificacion
    implicit none
    
    
    print *, EncontrarHorainicioVariable(14, 6, 4)
    print *, EncontrarHorainicioVariable(7, 23, 6)
    print *, EncontrarHorainicioVariable(11, 17, 12)
    
    if ((EncontrarHorainicioVariable(14, 6, 4) + EncontrarHorainicioVariable(7, 23, 6) + EncontrarHorainicioVariable(11, 17, 12)) == 46) then
        print *, "100azo en funciones"
    end if

    integer :: x, y, z

    x = 9
    y = 12
    z = 3

    integer :: exp1 = x - y / 3 + z * 2 - 1
    print *, "Evaluacion 1 = " , exp1

    integer :: exp2 = (x - y)/3 + ((z * 2) - 1)
    print *, "Evaluacion 2 = " , exp2

    x = 30
    y = 50.5
    z = 60
    
    if (y > x) then
        print *, "y es mayor que x"
    else
        print *, "y es menor que x"
    end if 

    if (y < z) then
        print *, "y is menor que z"
    else  
        print *, "y es mayor que z"
    end if 
  
    integer :: a = 12
    integer :: b = 10
    print *, "a == b  : " , a , "==" , b , " : " , (a == b)
    print *, "a != b  : " , a , "!=" , b , " : " , (a /= b)
    print *, "a > b   :  " , a , ">" , b , " : " , (a > b)
    print *, "a < b   :  " , a , "<" , b , " : " , (a < b)
    print *, "a >=b   : " , a , ">=" , b , " : " , (a >= b)
    print *, "a <= b  : " , a , "<=" , b , " : " , (a <= b)

    print *, 'CON ETIQUETAS'
    print *, "a == b  : " , a , "==" , b , " : " , (a .eq. b)
    print *, "a != b  : " , a , "!=" , b , " : " , (a .ne. b)
    print *, "a > b   :  " , a , ">" , b , " : " , (a .gt. b)
    print *, "a < b   :  " , a , "<" , b , " : " , (a .lt. b)
    print *, "a >=b   : " , a , ">=" , b , " : " , (a .ge. b)
    print *, "a <= b  : " , a , "<=" , b , " : " , (a .le. b)

    x = 10
    y = 5

    logical :: result1 = (x == 10 .and. y == 5)
    print *, "Resultado 1: " , result1
    logical :: result2 = (x == 10 .and. y > x)
    print *, "Resultado 2: " , result2
    logical :: result3 = (x < y .and. y > x)
    print *, "Resultado 3: " , result3

    print *, (2 == 2) .or. (3 > 5) 
    print *, (5 > 18) .or. (3 /= 9)
    print *, (4 == 4) .or. (5 < 9) 
    print *, (4 < 2) .or. (2 == 1) 
    print *, (3 /= 3) .or. (3 >= 9)

    x = 1
    y = 2
    z = 5
    print *, "x: " , (.not. ((x+2)==(1+2)))
    print *, "y: " , (.not.(y==z))
    print *, "z>x: " , (.not.(z > x))

    integer :: f = factorial()
    print *, f          

end program calificacion