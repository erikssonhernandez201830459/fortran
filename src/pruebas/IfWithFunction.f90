function fun(a, b, c) result(res)
    implicit none

    integer, intent(in) :: a
    integer, intent(in) :: b
    integer, intent(in) :: c
    integer :: res

    print *, a
    if (a > b .and. a > c) then
        res = a
    else if (b > a .and. b > c) then
        res = b
    else
        res = c
    end if

end function fun

program prog
implicit none
integer :: a = fun(10, 20, 30)
! , b = fun(30, 20, 10), c = fun(20, 30, 10)

print *, a

end program prog