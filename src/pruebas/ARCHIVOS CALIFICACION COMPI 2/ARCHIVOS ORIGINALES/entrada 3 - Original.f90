function check(horaIngreso, horaActual) result (horaInicio)
    implicit none
    integer, intent(in) :: horaIngreso
    integer, intent(in) :: horaActual
    integer :: horaInicio, frecuencia, inicio9

    if (horaingreso < 6 .and. horaactual < 7) then
        horaInicio = horaIngreso

    else if (horaingreso >= 6 .and. horaactual >= 7) then 
        if (inicio9 == frecuencia) then
            horaInicio = 9
        else 
            horaInicio = 8
        end if
    end if

    
    call sumarMatrices()

end function check


subroutine sumarMatrices() 
    implicit none
    integer :: a(3,3)
    integer, dimension(3,3) :: B, c

    a[1,1] = 1
    a[1,2] = 3
    a[1,3] = 4

    a[2,1] = 2
    a[2,2] = 4
    a[2,3] = 3

    a[3,1] = 3
    a[3,2] = 4
    a[3,3] = 5

    b[1,1] = 1
    b[1,2] = 3
    B[1,3] = 4

    B[2,1] = 2
    b[2,2] = 4
    b[2,3] = 3

    b[3,1] = 1
    b[3,2] = 2
    b[3,3] = 4

    integer :: i, j

    do i = 1, 3, 1
        do j = 1, 3, 1
            c[i,j] = a[i,j] + b[i,j]
        end do
    end do

    print *, c

end subroutine sumarMatrices

function factorial() result (fact)
    implicit none
    integer :: i = 1,fact=1
    integer :: number=17
    
    
    do while (i <= number)
        fact=fact*i
        i = i + 1
    end do

    ! -288522240
    print *, "Factorial de ", number, " es: ", fact   
end function factorial


program calificacion
    implicit none
    

    integer :: x, y, z

    x = 9
    y = 12
    z = 3

    integer :: exp1 = x - y / 3 + z * 2 - 1
    print *, "Evaluacion 1 = " , exp1

    integer :: exp2 = (x - y)/3 + ((z * 2) - 1)
    print *, "Evaluacion 2 = " , exp2

    x = 30
    y = 50.5
    z = 60
    
    if (y > x) then
        print *, "y es mayor que x"
    else
        print *, "y es menor que x"
    end if 

    if (y < z) then
        print *, "y is menor que z"
    else  
        print *, "y es mayor que z"
    end if 
  
    integer :: a = 12
    integer :: b = 10
    print *, "a == b  : " , a , "==" , b , " : " , (a == b)
    print *, "a != b  : " , a , "!=" , b , " : " , (a /= b)
    print *, "a > b   :  " , a , ">" , b , " : " , (a > b)
    print *, "a < b   :  " , a , "<" , b , " : " , (a < b)
    print *, "a >=b   : " , a , ">=" , b , " : " , (a >= b)
    print *, "a <= b  : " , a , "<=" , b , " : " , (a <= b)

    print *, 'CON ETIQUETAS'
    print *, "a == b  : " , a , "==" , b , " : " , (a .eq. b)
    print *, "a != b  : " , a , "!=" , b , " : " , (a .ne. b)
    print *, "a > b   :  " , a , ">" , b , " : " , (a .gt. b)
    print *, "a < b   :  " , a , "<" , b , " : " , (a .lt. b)
    print *, "a >=b   : " , a , ">=" , b , " : " , (a .ge. b)
    print *, "a <= b  : " , a , "<=" , b , " : " , (a .le. b)

    x = 10
    y = 5

    logical :: result1 = (x == 10 .and. y == 5)
    print *, "Resultado 1: " , result1
    logical :: result2 = (x == 10 .and. y > x)
    print *, "Resultado 2: " , result2
    logical :: result3 = (x < y .and. y > x)
    print *, "Resultado 3: " , result3

    print *, (2 == 2) .or. (3 > 5) 
    print *, (5 > 18) .or. (3 /= 9)
    print *, (4 == 4) .or. (5 < 9) 
    print *, (4 < 2) .or. (2 == 1) 
    print *, (3 /= 3) .or. (3 >= 9)

    x = 1
    y = 2
    z = 5
    print *, "x: " , (.not. ((x+2)==(1+2)))
    print *, "y: " , (.not.(y==z))
    print *, "z>x: " , (.not.(z > x))

    integer :: f = factorial()
    print *, f          

    print *, check(4, 3)
    print *, check(17, 23)
    print *, check(78, 91)

end program calificacion