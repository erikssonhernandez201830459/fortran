package GUI;

import javax.swing.*;
import javax.swing.text.BadLocationException;

public class UpdatePosition {
    public String lineAndColumnInfo(JTextArea component) {
        String info = "";
        int linea = 1;
        int columna = 1;

        int caretPos = component.getCaretPosition();
        try {
            linea = component.getLineOfOffset(caretPos);
            columna = caretPos - component.getLineStartOffset(linea);
            linea++;
        } catch (BadLocationException ex) {
            ex.printStackTrace(System.out);
        }

        info = "Linea: " + linea + ", Columna: " + columna;
        return info;
    }
}
