package Abstract;

public enum Operador {
    Suma,
    Resta,
    Multiplicacion,
    Division,
    Potencia,
}
