package Abstract;

import entorno.Symbol;
import entorno.Tree;
import org.antlr.v4.gui.TreeViewer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Reportes {
    public static Tree tree;
    public static NodoAST nodoRaiz;
    public static TreeViewer treeViewer;
    public static String systemDirection = System.getProperty("user.dir").toString();



    public static void graphAST(NodoAST raiz){

        try
        {
            String reporteAST = getDot(raiz);

            FileWriter file = new FileWriter("AST.dot");
            file.write(reporteAST);
            file.close();
            Runtime.getRuntime().exec("dot -Tpdf AST.dot -o AST.pdf");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Se construyo el AST");
        }




    }

    static int c = 1;
    static String grafo = "";

    static public String getDot(NodoAST raiz){
        grafo = "";
        grafo += "digraph {\n";
        grafo += "bgcolor=\"#1a1a1a\"\n" +
                "edge [fontcolor=\"black\", color=\"#28B463\"]\n" +
                "node [style=filled, fillcolor=\"#313638\", fontcolor=\"white\" , color=transparent]";
        grafo += "label=\"AST (Abstract Syntax Tree)\" fontsize=35 fontcolor=white;\n" +
                "    labelloc=\"t\"";
        grafo += "n0[label=\"" + raiz.getValor() + "\"];\n";
        c = 1;
        recorrerAST("n0", raiz);
        grafo += "}";
        return grafo;
    }

    static public void recorrerAST(String padre, NodoAST nPadre){
        for(NodoAST hijo: nPadre.getHijos()){
            String nombreHijo = "n" + c;
            grafo += nombreHijo + "[label=\"" + hijo.getValor()+ "\"];\n";
            grafo += padre + "->" + nombreHijo + ";\n";
            c++;
            recorrerAST(nombreHijo, hijo);
        }
    }

    /**
     * GRAFICAR SIMBOLOS
     */

    public static void graphSimbolos(Tree tree){
        String reportTableSymbol = "digraph E { ";
                reportTableSymbol += "    bgcolor=\"#1a1a1a\"\n" +
                        "edge [fontcolor=\"black\", color=\"#28B463\"]\n" +
                        "node [style=filled, fillcolor=\"#313638\", fontcolor=\"white\" , color=transparent]\n" +
                        "label=\"Reporte Tabla de Simbolos\" fontsize=35 fontcolor=white;\n" +
                        "    labelloc=\"t\"";
                reportTableSymbol += "node[shape = plain] tabla [label = <<TABLE>";
        reportTableSymbol += "<TR><TD> TIPO1 </TD>" +
                "<TD> TIPO2 </TD>" +
                "<TD> NOMBRE </TD>"+
                "<TD> VALOR </TD>"+
                "<TD> LINEA </TD>"+
                "<TD> COLUMNA </TD></TR>";

        for(Symbol sim: tree.variables) {

            reportTableSymbol += "<TR><TD>" + sim.tipo.toString() + "</TD>" +
                    "<TD>" + sim.tipo2.toString() + "</TD>" +
                    "<TD>" + sim.id + "</TD>" +
                    "<TD>" + sim.valor.toString() + "</TD>" +
                    "<TD>" + sim.line + "</TD>" +
                    "<TD>" + sim.column + "</TD></TR>";
        }

        for (String clave : tree.funciones.keySet()) {
            Symbol sim = tree.funciones.get(clave);

            reportTableSymbol += "<TR><TD>" + sim.tipo.toString() + "</TD>" +
                    "<TD>" + sim.tipo2.toString() + "</TD>" +
                    "<TD>" + sim.id + "</TD>" +
                    "<TD>"  + "</TD>" +
                    "<TD>" + sim.line + "</TD>" +
                    "<TD>" + sim.column + "</TD></TR>";
        }

        for (String clave : tree.metodos.keySet()) {
            Symbol sim = tree.metodos.get(clave);

            reportTableSymbol += "<TR><TD>" + sim.tipo.toString() + "</TD>" +
                    "<TD>" + sim.tipo2.toString() + "</TD>" +
                    "<TD>" + sim.id + "</TD>" +
                    "<TD>"  + "</TD>" +
                    "<TD>" + sim.line + "</TD>" +
                    "<TD>" + sim.column + "</TD></TR>";
        }

        reportTableSymbol += "</TABLE>>]; }";


        try
        {


            FileWriter file = new FileWriter("SymbolTable.dot");
            file.write(reportTableSymbol);
            file.close();
            Runtime.getRuntime().exec("dot -Tpdf SymbolTable.dot -o SymbolTable.pdf");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Se construyo el Reporte de Tabla de Simbolos");
        }
    }


        }
