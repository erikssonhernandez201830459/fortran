package Abstract;



import Traductor.QuadControlador;
import Traductor.Quadrupla;
import entorno.SymbolTable;
import entorno.Tipo;
import entorno.Tree;

public abstract class Instruccion {
    public int line;
    public int column;
    public Tipo.Type type;

    public Instruccion(Tipo.Type type, int line, int column) {
        this.line = line;
        this.column = column;
        this.type = type;
    }

    public abstract Object execute(SymbolTable table, Tree tree);
    public abstract Quadrupla translate(QuadControlador controlador);
    public abstract NodoAST getAST();

}
