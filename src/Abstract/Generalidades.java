package Abstract;

public class Generalidades {
    /***
     * Todas las lineas comentadas:
     * //tree.consola.add(error.toString());
     * se utilizaban para mostrar los errores en la consola en tiempo de ejecucion
     * pero se restringio unicamente hacia el reporte generado en graphviz
     */

    /**
     * Se puede mejorar en las operaciones aritmeticas el uso de un Enum para guardar los
     * operadores y evitar errores, asi como se implemento en los tipos de datos y los errores
     */

    /**
     * Cuando recorremos los arreglos, se hace referencia a las posiciones con las letras i,j
     * siendo:
     * ARRAY DE 1 DIMENSION =       i -> array[i]
     * ARRAY DE 2 DIMENSIONES =     i,j -> array2[i,j]
     */

    /**
     * EN EL CICLO DO WHILE (WHILE)
     * Tenemos un limite para evitar el desborde de memoria si no se cumple la condicion
     *  //Incrementamos la variable i
     *             i += 1;
     *             if(i > 400){    //Valor por defecto para evitar el desborde de memoria
     *                 break;
     *             }
     */

}
