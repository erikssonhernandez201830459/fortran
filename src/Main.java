import Abstract.Instruccion;
import Abstract.NodoAST;
import Abstract.Reportes;
import entorno.SymbolTable;
import entorno.*;
import grammar.*;
import grammar.GrammarParser;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;

import static org.antlr.v4.runtime.CharStreams.fromString;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    /*public static void main(String[] args) {
        System.out.println("Hello World!");
        IDE_Template editor = new IDE_Template();
        editor.setLocationRelativeTo(null);
        editor.setVisible(true);
    }*/
    public void ejecutar() {
        CharStream charStream = CharStreams.fromString("""
                program myProgram
                                implicit none

                                integer :: x=4

                                if ( x .eq. 4 ) then
                                    print *, "Primer If"
                                    logical :: y = .true.

                                    if ( .not. y ) then
                                        print *, "Segundo If"
                                    else if ( x .eq. 4 ) then
                                        print*, "Segundo ElseIf"
                                    else
                                        print *, "Segundo Else"
                                    end if

                                else
                                    print *, "Primer Else"
                                end if

                                end program myProgram
                """);

        /**
         * INSTANCIAS PRINCIPALES
         */
        Reportes.tree = new Tree();

        /**
         * ANALISIS LEXICO
         */

        GrammarLexer grammarLexer = new GrammarLexer(charStream);
        /**
         * MANEJO DE ERRORES LEXICOS
         */
        Reportes.tree.excepciones.add(new Excepcion(null, "DESCRIPCION", -1, -1));
        //MANEJO DE ERRORES LEXICOS
        grammarLexer.removeErrorListeners();
        grammarLexer.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                Reportes.tree.excepciones.add(new Excepcion(Excepcion.ErrorTipo.Lexico, msg, line, charPositionInLine));
            }
        });

        /**
         * ANALISIS SINTACTICO
         */
        CommonTokenStream commonTokenStream = new CommonTokenStream(grammarLexer);
        GrammarParser grammarParser = new GrammarParser(commonTokenStream);

        /**
         * MANEJO DE ERRORES SINTACTICOS
         */
        grammarParser.removeErrorListeners();
        grammarParser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                Reportes.tree.excepciones.add(new Excepcion(Excepcion.ErrorTipo.Sintactico, msg, line, charPositionInLine));
            }
        });

        GrammarParser.StartContext startContext = grammarParser.start();
        /**
         * INICIO DEL INTERPRETE
         */
        Visitor visitor = new Visitor();
        Object treeAST = visitor.visit(startContext);

        /**
         * CREAMOS UNA NUEVA TABLA DE SIMBOLOS GENERAL
         */
        SymbolTable globalTable = new SymbolTable(null);


        /**
         * CONSTRUCCION DEL CST
         */
        java.util.List<String> ruleNames = Arrays.asList(grammarParser.getRuleNames());
        Reportes.treeViewer = new TreeViewer(ruleNames, startContext);

        /**
         * CONSTRUCCION DEL AST
         */
        Reportes.tree.instrucciones = (ArrayList<Instruccion>) treeAST;
        Reportes.nodoRaiz = new NodoAST("RAIZ");

        /**
         * SALIDA DE CONSOLA
         */
        String salida = "";
        for(int i = 0; i<Reportes.tree.instrucciones.size(); i++){
            Object res = Reportes.tree.instrucciones.get(i).execute(globalTable,Reportes.tree);
            //ERROR
            if(res instanceof Excepcion er){
                Reportes.tree.consola.add(er.toString());
            }
            Reportes.nodoRaiz.agregarHijo(Reportes.tree.instrucciones.get(i).getAST());
        }

        for(int i = 0; i < Reportes.tree.consola.size(); i++){
            salida += Reportes.tree.consola.get(i);
        }


        System.out.println(salida);

    }
}
