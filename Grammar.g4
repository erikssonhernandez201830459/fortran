grammar Grammar;
//Indicamos que la gramática será case insensitive
options { caseInsensitive = true; }

    /*Declaración de Tokens*/
//Palabras reservadas
PROGRAM         : 'program';
IF              : 'if';
ELSE            : 'else';
WHILE           : 'while';
DO              : 'do';
THEN            : 'then';
EXIT            : 'exit';
CYCLE           : 'cycle';
END             : 'end';
SUBROUNTINE     : 'subrountine';
IMPLICIT        : 'implicit';
NONE            : 'none';
INTENT          : 'intent';
IN              : 'in';
FUNCTION        : 'function';
RESULT          : 'result';

//Funcionalidades
PRINT           : 'print';
DIMENSION       : 'dimension';
SIZE            : 'size';
ALLOCATABLE     : 'allocatable';
ALLOCATE        : 'allocate';
DEALLOCATE      : 'deallocate';
CALL            : 'call';

//Tipos de Datos
/*
INTEGER         : 'integer';
REAL            : 'real';
COMPLEX         : 'complex';
CHARACTER       : 'character';
LOGICAL         : 'logical';
*/
//Caracteres Especiales
PTS             : ':';
PTS_DECLARACION : '::';
COMA            : ',';
IGUAL           : '=';
COMILLAS_D      : '"';
COMILLAS_S      : '\'';
PA              : '(';
PC              : ')';

//Operadores Aritmeticos
ASTERISCO_DOBLE : '**';
ASTERISCO       : '*';
DIAGONAL        : '/';
MAS             : '+';
MENOS           : '-';

//Operadores Relacionales
IGUAL_IGUAL     : '==';
DIFERENTE_IGUAL : '/=';
MAYOR_QUE       : '>';
MAYOR_IGUAL     : '>=';
MENOR_QUE       : '<';
MENOR_IGUAL     : '<=';
EQ              : '.eq.';
NE              : '.ne.';
GT              : '.gt.';
LT              : '.lt.';
GE              : '.ge.';
LE              : '.le.';

//Operadores Lógicos
AND             : '.and.';
OR              :  '.or.';
NOT             : '.not.';

// Tokens Especificos
INT             : [0-9]+ ;
REAL            : [0-9]+'.'[0-9]+;
COMPLEX         : '('[0-9]+','[0-9]+')';
IDEN            : [a-z][a-z0-9_]* ;
CHAR            : ["|'].["|'];
STRING          : ["|'] (~["\r\n] | '""')+ ["|'];
WS              : [ \t\r\n]+ -> skip ;
COMENTARIO      : '!' ~( '\r' | '\n' )* -> skip;

start : bloqueInstrucciones EOF;

bloqueInstrucciones: e+=instruccionesPrincipales*
;

instruccionesPrincipales
        :'program' id1=IDEN 'implicit' 'none' e+=instrucciones* 'end' 'program' id2=IDEN                       #instruccionesProgram
        |'subroutine' id1=IDEN '(' e1+=listaParametros* ')'
         'implicit' 'none' e2+=listaDeclaracionParametros* e3+=instrucciones* 'end' 'subroutine' id2=IDEN          #instruccionesSubrutina
        |'function' id1=IDEN '(' e1+=listaParametros* ')' 'result' '(' id3=IDEN ')'
                 'implicit' 'none' e2+=listaDeclaracionParametros* e3+=instrucciones* 'end' 'function' id2=IDEN    #instruccionesFuncion
;

listaParametros
        :id=IDEN        #listaParametrosNormal
        |',' id=IDEN    #listaParametrosComa
;

listaDeclaracionParametros
        :type=tipo ',' 'intent' '(' 'in' ')' '::' id=IDEN                                           #listaDeclaracionParametrosNormal
        |type=tipo ',' 'intent' '(' 'in' ')' '::' id=IDEN '(' dim1=expresion ',' dim2=expresion ')' #listaDeclaracionParametrosArray2Dim
        |type=tipo ',' 'intent' '(' 'in' ')' '::' id=IDEN '(' dim1=expresion ')'                    #listaDeclaracionParametrosArray1Dim
;

instrucciones
        :declaracion #instruccionesDeclaracion
        |asignacion  #instruccionesAsignacion
        |asignacionA #instruccionesAsignacionA
        |allocate    #instruccionesAllocate
        |print       #instruccionesPrint
        |if          #instruccionesIf
        |do          #instruccionesDo
        |exit        #instruccionesExit
        |cycle       #instruccionesCycle
        |call        #instruccionesCall
;

declaracion
        :tipo ',' 'dimension' '(' dim1=expresion ',' dim2=expresion ')' '::' id=IDEN    #declaracionArray2Dim
        |tipo ',' 'dimension' '(' dim1=expresion ')' '::' id=IDEN                       #declaracionArray1Dim
        |tipo ',' 'allocatable' '::' id=IDEN '(' ':' ',' ':' ')'                        #declaracionAllocatable2Dim
        |tipo ',' 'allocatable' '::' id=IDEN '(' ':' ')'                                #declaracionAllocatable1Dim
        |tipo '::' id=IDEN '(' dim1=expresion ',' dim2=expresion ')'                    #declaracionArray2Dim2
        |tipo '::' id=IDEN '(' dim1=expresion ')'                                       #declaracionArray1Dim2
        |tipo '::' id=IDEN '=' expresion e+=listaDeclaracion* #declaracionAsig
        |tipo '::' id=IDEN e+=listaDeclaracion*               #declaracionUniq
;

listaDeclaracion
        :',' id=IDEN '=' expresion #listaDeclaracionAsig
        |',' id=IDEN               #listaDeclaracionUniq
;

asignacion
        :id=IDEN '=' expresion                                                  #asignacionId
        |id=IDEN '[' num=expresion ']' '=' val=expresion                        #asignacionArray1
        |id=IDEN '[' num1=expresion ',' num2=expresion ']' '=' val=expresion    #asignacionArray2
;

asignacionA
        :id=IDEN '[' ':' ']' '=' val=expresion                                  #asignacionArray1D
        |id=IDEN '[' ':' ',' ':' ']' '=' val=expresion                          #asignacionArray2D
;

allocate
        :'allocate' '(' id=IDEN '(' izq=expresion ',' der=expresion ')' ')'   #allocate2Dim
        |'allocate' '(' id=IDEN '(' izq=expresion ')' ')'                      #allocate1Dim
        |'deallocate' '(' id=IDEN ')'                                           #deallocate
;
print
        :p='print' '*' ',' expresion (',' expresion)*
;



if
        :id='if' '(' cond=expresion ')' 'then' e+=instrucciones* 'end' 'if'                                        #ifNormal
        |id='if' '(' cond=expresion ')' 'then' e+=instrucciones* 'else' e2+=instrucciones* 'end' 'if'             #ifElse
        |id='if' '(' cond=expresion ')' 'then' e+=instrucciones* e2+=elseif* 'end' 'if'                            #ifElseIf
        |id='if' '(' cond=expresion ')' 'then' e+=instrucciones* e2+=elseif* 'else' e3+=instrucciones* 'end' 'if' #ifElseIfElse
;

elseif
        :id='else' 'if' '(' cond=expresion ')' 'then' e+=instrucciones*
;

do
        :id='do' inicio=asignacion ',' fin=expresion ',' paso=expresion e+=instrucciones* 'end' 'do'                       #doNormal
        |id='do' inicio=asignacion ',' fin=expresion e+=instrucciones* 'end' 'do'                                          #doNormalSinPaso
        |id1=IDEN ':' 'do' inicio=asignacion ',' fin=expresion ',' paso=expresion e+=instrucciones* 'end' 'do' id2=IDEN    #doEtiqueta
        |id1=IDEN ':' 'do' inicio=asignacion ',' fin=expresion e+=instrucciones* 'end' 'do' id2=IDEN                       #doEtiquetaSinPaso
        |id='do' 'while' '(' condicion=expresion ')' e+=instrucciones* 'end' 'do'                                          #doWhile
        |id1=IDEN ':' 'do' 'while' '(' condicion=expresion ')' e+=instrucciones* 'end' 'do' id2=IDEN                       #doWhileEtiqueta
;

exit
        :id='exit'                              #exitNormal
        |id1='exit' id2=IDEN                    #exitEtiqueta
;

cycle
        :id='cycle'                             #cycleNormal
        |id1='cycle' id2=IDEN                   #cycleEtiqueta
;

call
        :'call' id=IDEN '(' e+=listaCall* ')'
;

listaCall
        :val=expresion                          #listaCallNormal
        |',' val=expresion                      #listaCallComa
;

expresion
        :op='-' der=expresion                                #expresionNegativo
        |izq=expresion op='**' der=expresion                #expresionPotencia
        |izq=expresion op='/' der=expresion                 #expresionDivision
        |izq=expresion op='*' der=expresion                 #expresionMultiplicacion
        |izq=expresion op='-' der=expresion                 #expresionResta
        |izq=expresion op='+' der=expresion                 #expresionSuma
        |izq=expresion op=('=='|'.eq.') der=expresion       #expresionEq
        |izq=expresion op=('/='|'.ne.') der=expresion       #expresionNe
        |izq=expresion op=('>='|'.ge.') der=expresion       #expresionGe
        |izq=expresion op=('>' |'.gt.') der=expresion       #expresionGt
        |izq=expresion op=('<='|'.le.') der=expresion       #expresionLe
        |izq=expresion op=('<' |'.lt.') der=expresion       #expresionLt
        |op='.not.' der=expresion                               #expresionNot
        |izq=expresion op='.and.' der=expresion             #expresionAnd
        |izq=expresion op='.or.'  der=expresion             #expresionOr
        |si='size' '(' val=IDEN ')'                         #expresionSize
        |val=IDEN '[' pos=expresion ']'                     #expresionArray1
        |val=IDEN '[' pos1=expresion ',' pos2=expresion ']' #expresionArray2
        |val=IDEN                                           #expresionIdentificador
        |val=REAL                                           #expresionReal
        |val=INT                                            #expresionInt
        |val=CHAR                                           #expresionChar
        |val=STRING                                         #expresionString
        |val='.true.'                                       #expresionTrue
        |val='.false.'                                      #expresionFalse
        |'(/' val=expresion e+=listaExpresion* '/)'         #expresionListaExpresion
        |'(' val=expresion ')'                              #expresionParentesis
        |id1=IDEN '(' e+=listaCall* ')'                     #expresionFuncion
;

listaExpresion
        :',' expresion
;

tipo
        :'integer'      #tipoInteger
        |'real'         #tipoReal
        |'complex'      #tipoComplex
        |'character'    #tipoCharacter
        |'logical'      #tipoLogical
;