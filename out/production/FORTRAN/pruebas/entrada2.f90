function contains(tam , arreglo , elemento) result (flag)
    implicit none
    integer , intent(in) :: tam
    integer , intent(in) :: arreglo ( tam )
    integer , intent(in) :: elemento
    logical :: flag
    !arreglo = (/ 1  , 2  , 3 , 4 , 5 , 6 , 7 , 8 , 9  /)
    integer :: i

    do i = 1  , tam , 1
        if (arreglo[ i ] .eq. elemento) then
            flag = .true.
            exit
        end if
    end do

    if (flag .eq. .true.) then
        print * , 'En el arreglo ' , arreglo , ' si existe el elemento ' , elemento
    end if 
end function contains

function encontrarhorainiciovariable (horaIngreso , horaActual , frecuencia) result (horaInicio)
    implicit none
    integer , intent(in) :: horaIngreso
    integer , intent(in) :: horaActual
    integer , intent(in) :: frecuencia
    integer :: horaInicio , inicio9 = 8

    integer , dimension(4) :: horarios8
    integer :: horarios6( 5)
    horarios6 = (/ 10 ,16 ,22 ,4 ,10 /)

    integer :: horarios4( 7)
    horarios4 = (/ 10 , 14 , 18 , 22 , 2 , 6 , 10 /)
    integer , allocatable :: horarios3_2(:)
    integer , dimension(6) :: inicio10

    horarios8 = (/ 10 , 18 , 2 , 10 /)


    inicio10 = (/24 , 12 , 6 , 4 , 3 , 2/)

    print * , "PARAMETROS: " , size( inicio10 ) , "  , " , inicio10 , "  , " , frecuencia

    if (horaIngreso < 6 .and. horaActual > 0) then
        horaInicio = horaIngreso

    else if (horaIngreso <= 10 .and. horaActual >= 7) then 
        ! logical :: contiene = contains(size( inicio10 ) , inicio10 , frecuencia)
        logical :: contiene = .true.
        if (contiene .eq. .true.) then
            horaInicio = 10
        else if (inicio9 == frecuencia) then
            horaInicio = 9
        else 
            horaInicio = 8
        end if
    else
        allocate(horarios3_2(12))
             if (frecuencia == 2) then
            horarios3_2 = (/10 , 12 , 14 , 16 , 18 , 20 , 22 , 0 , 2 , 4 , 6 , 10/)
            !logical :: contiene = contains(size(horarios3_2) ,horarios3_2 , horaIngreso)
logical :: contiene = .true.

            if (contiene .eq. .true.) then 
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1 , h <= size(horarios3_2) , 1
                    if (horarios3_2[h] > horaIngreso) then
                        horaInicio = horarios3_2[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 4) then
               print * , "size(horarios4): " , size(horarios4)
                                print * , "horarios4: " , horarios4
                                print * , "horaIngreso: " , horaIngreso

            !logical :: contiene = contains(size(horarios4) , horarios4 , horaIngreso)

            logical :: contiene = .true.
            if (contiene .eq. .true.) then
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1 , h <= size(horarios4) , 1
                    if (horarios4[h] > horaIngreso) then
                        horaInicio = horarios4[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 3) then
            deallocate(horarios3_2)
            allocate(horarios3_2(9))
            !logical :: contiene = contains(size(horarios3_2) ,horarios3_2 , horaIngreso)
            logical :: contiene = .true.

            if (contiene .eq. .true.) then
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1 , h <= size(horarios3_2) , 1
                    if (horarios3_2[h] > horaIngreso) then
                        horaInicio = horarios3_2[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 6) then
            logical :: contiene = contains(size(horarios6) ,horarios6 , horaIngreso)
            if (contiene .eq. .true.) then 
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1 , h <= size(horarios6) , 1
                    if (horarios6[h] > horaIngreso) then
                        horaInicio = horarios6[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 8) then
            if (contains(size(horarios8) ,horarios8 , horaIngreso)) then
                horaInicio = horaIngreso
            else 
                integer :: h 
                do i=1 , h <= size(horarios8) , 1
                    if (horarios8[h] > horaIngreso) then
                        horaInicio = horarios8[h]
                        exit
                    end if
                end do
            end if
        else if (frecuencia == 12) then
            if (horaIngreso > 10) then
                horaInicio = 22
            else
                horaInicio = 10
            end if
        else if (frecuencia == 24) then
            horaInicio = 10
        end if
    end if
  
end function encontrarhorainiciovariable

subroutine sumarMatrices() 
    implicit none
    integer :: a( 3 , 3) !Separacion de los parametros
    ! Separacion de Declaraciones
    integer , dimension( 3 , 3) :: B
    integer , dimension( 3 , 3) :: c

    a[1 ,1] = 1
    a[1 ,2] = 3
    a[1 ,3] = 4

    a[2 ,1] = 2
    a[2 ,2] = 4
    a[2 ,3] = 3

    a[3 ,1] = 3
    a[3 ,2] = 4
    a[3 ,3] = 5

    B[1 ,1] = 1
    B[1 ,2] = 3
    B[1 ,3] = 4

    B[2 ,1] = 2
    B[2 ,2] = 4
    B[2 ,3] = 3

    B[3 ,1] = 1
    B[3 ,2] = 2
    B[3 ,3] = 4

    integer :: i = 1 , j = 1

    !El inicio(INTEGER) , fin(LOGICAL) y paso(INTEGER) deben ser del tipo INTEGER
    ! do i = 1 , i < 4 , 1
    !    do j = 1 , j < 4 , 1
    do i=1 , 3 , 1
            do j=1 , 3 , 1
            c[ i , j] = a[ i , j] + B[ i , j]

        end do
    end do

    print * , c

end subroutine  sumarMatrices  


program calificacion
    implicit none
    
    
    print * , encontrarhorainiciovariable(14 , 6 , 4)
    print * , encontrarhorainiciovariable(7 , 23 , 6)
    print * , encontrarhorainiciovariable(11 , 17 , 12)
    
   if ((encontrarhorainiciovariable(14 , 6 , 4) + encontrarhorainiciovariable(7 , 23 , 6) + encontrarhorainiciovariable(11 , 17 , 12)) == 46) then
       print * , "100azo en funciones"
    end if
    print * , "*******PROBANDO SUMA DE MATRICES******* "
    call sumarMatrices()

end program calificacion

! Listado de Cambios
! funcion encontrar Hora Inicio Variable , a minusculas todas
! Variable horaingreso cambiada por horaIngreso
! Variable horaactual cambiada por horaActual
! funcion Contains cambiada a minusculas
! Variable b[ cambiada por B[ porque asi esta declarada al inicio
! Linea 170: El inicio(INTEGER) , fin(LOGICAL) y paso(INTEGER) deben ser del tipo INTEGER