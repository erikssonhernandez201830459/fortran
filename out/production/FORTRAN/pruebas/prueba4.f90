program myProgram
implicit none

integer :: x=4

if ( x .eq. 4 ) then
    print *, "Primer If"
    logical :: y = .true.

    if ( .not. y ) then
        print *, "Segundo If"
    else if ( x .eq. 4 ) then
        print*, "Segundo ElseIf"
    else
        print *, "Segundo Else"
    end if

else
    print *, "Primer Else"
end if

end program myProgram