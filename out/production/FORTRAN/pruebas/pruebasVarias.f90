program myProgram
implicit none

print *, 211
real:: si, sa
integer:: x = 4

x = 2+2*4
integer, dimension (4):: arr

print*, x

end program myProgram


program myProgram
implicit none

integer :: x ( 3  )
integer :: y ( 3  )

x = [/ 1 , 2 , 3 /]
print*, x

x = [/ 4 , 5 , 6 /]
print*, y

x = y
print*, x

end program myProgram


program myProgram
implicit none

integer :: x = 4
integer:: si( x, x )
integer :: no(4)

si[1,1] = 1
si[1,2] = x
si[2,2] = 1
si[2,3] = x
si[3,3] = 1
si[3,4] = x
si[4,4] = 1
si[4,1] = x
si[1,4] = 200

no[2] = 100

print*, si[1,4]
print*, no[2]
end program myProgram





program myProgram
implicit none

integer, allocatable :: array1(:)

allocate(array1(10))
allocate(array1(11))

array1[11] = 10

print*, array1

end program myProgram




program myProgram
implicit none

integer :: x=4

if ( x .eq. 4 ) then
    print *, "Primer If"
    logical :: y = .true.

    if ( .not. y ) then
        print *, "Segundo If"
    else if ( x .eq. 4 ) then 
        print*, "Segundo ElseIf"
    else
        print *, "Segundo Else"
    end if

else
    print *, "Primer Else"
end if

end program myProgram




program myProgram
implicit none

integer :: i, j
integer :: arr ( 6 , 6 )


do i = 1 , 6 , 1
    do j = 1 , 6 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            arr [ i , j ] = 4
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            arr [ i , j ] = 5
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            arr [ i , j ] = 6
       end if
    end do
end do

print *, arr
end program myProgram





program myProgram
implicit none

integer :: i
do i = 1, 100, 1
  if (i > 10) then
      exit
  end if
  print *, i
end do


end program myProgram




program myProgram
implicit none

integer :: i
do i = 1, 10, 1
  if (i .eq. 4) then
      cycle
  end if
  print *, i
end do


end program myProgram


program myProgram
implicit none

    integer :: i , j
    outer_loop : do i = 1 , 10 , 1
        inner_loop : do j = 1 , 10 , 1
            if ((j + i) > 10) then  !imprime solo la pareja de números i, j
					                !que suman más de 10
                cycle inner_loop        !va a la siguiete iteración del ciclo “outer_loop”
            end if
            print *, "I=" , i , "J=" , j , "Sum=" , j + i
        end do inner_loop
    end do outer_loop
end program myProgram










subroutine print_matrix ( n , m , A )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  real, intent(in) :: A( n, m)
 
  print*, "SUBRUTINA"
end subroutine print_matrix



program myProgram
implicit none

print *, "Si jalo2"

end program myProgram



subroutine print_matrix ( n , m , a )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  integer, intent (in) :: a ( n , m )



  print*, a
  print*, n, " SUBRUTINA ", m
end subroutine print_matrix



program myProgram
implicit none
    integer :: arr ( 2 , 2 )

call print_matrix ( 2 , 2 , arr )

end program myProgram








subroutine print_matrix ( n , m , a )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  integer, intent (in) :: a ( n )



  print*, a
  print*, n, " SUBRUTINA ", m
end subroutine print_matrix



program myProgram
implicit none
    integer :: arr ( 2 )

call print_matrix ( 2 , 2 , arr )

end program myProgram





subroutine print_matrix ( n , m , a )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  integer, intent (in) :: a ( n , m )
  
  integer :: i, j

do i = 1 , 6 , 1
    do j = 1 , 6 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            a [ i , j ] = 1
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            a [ i , j ] = 2
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            a [ i , j ] = 3
       end if
    end do
end do


  print*, a
  print*, n, " SUBRUTINA ", m
end subroutine print_matrix



program myProgram
implicit none

integer :: i, j
integer :: arr ( 6 , 6 )


do i = 1 , 6 , 1
    do j = 1 , 6 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            arr [ i , j ] = 4
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            arr [ i , j ] = 5
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            arr [ i , j ] = 6
       end if
    end do
end do

print *, arr
call print_matrix ( 6 , 6 , arr )
print *, arr

end program myProgram





program compi
    implicit none

    integer :: i , j
    integer, dimension( 10 ) :: array
    array = [/ 2 , 5 , 7 , 6 , 8 , 1 , 9 , 6 , 8 , 3 /]

    do i = 1, size ( array ) - 1 , 1
        do j = i + 1, size ( array ) , 1
            if (array[ i ] > array [ j ] ) then
                integer :: aux = array [ i ]
                array [ i ] = array [ j ] 
                array [ j ] = aux
            end if
        end do
    end do

    print *, array

end program compi




program compi
    implicit none

    integer :: i , j
    integer, dimension( 5 ) :: array
    array = [/ 7 , 3 , 2  , 1 , 2/]

    do i = 1, size ( array ) - 1  , 1
        do j = i + 1, size ( array ), 1
            if (array[ i ] > array [ j ] ) then
                integer :: aux = array [ i ]
                array [ i ] = array [ j ] 
                array [ j ] = aux
            end if
        end do
    end do

    print *, array

end program compi


program compi
    implicit none

    integer :: i , j
    integer, dimension( 3 ) :: array
    array = [/ 7 , 3 , 2 /]

    do i = 1, size ( array ) - 1  , 1
        do j = i + 1, size ( array ), 1
            if (array[ i ] > array [ j ] ) then
                integer :: aux = array [ i ]
                array [ i ] = array [ j ] 
                array [ j ] = aux
            end if
        end do
    end do

    print *, array

end program compi



function print_matrix ( n , m ) result ( x )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
   
  integer :: x = 2
  
  
  print*, " funcion "

end function print_matrix



program myProgram
implicit none

  print*, "Antes"
  print*, print_matrix ( 2 , 4 )
  print*, "Despues"


end program myProgram





function print_matrix ( n , m ) result ( x )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
   
  real :: x = 4.0
  
  print*, "funcion "
  
end function print_matrix



program myProgram
implicit none
  integer :: x
  print*, "Antes"
  x = print_matrix ( 2 , 4 )
  print*, print_matrix ( 2 , 4 )
  print*, "Despues"


end program myProgram








function ret (  ) result ( x )
  implicit none
   
  integer :: x ( 2 )
  
end function ret



program myProgram
implicit none
  integer :: x
  x = ret ( )
  print*, ret ( )



end program myProgram



program call_sub
  implicit none
  integer :: mat(4)
 
   mat [ : ] = 1
   print*, mat
 
end program call_sub











function ret (  ) result ( x )
  implicit none
   
  integer :: x ( 2 )
  x[ 1 ] = 1
  x[ 2 ] = 2
  
end function ret



program myProgram
implicit none
  integer :: x ( 2 )
  integer :: y ( 2 )
   x = ret ( )
  print *, x
   x = [/ 3, 4 /]
   y = [/ 5, 6  /]
  print *, x
  print *, y
  y = x
  print *, y

end program myProgram




program myProgram
implicit none
  integer :: x ( 2 )
  integer :: y ( 2 )
   x = [/ 3, 4 /]
   y = [/ 5, 6 /]

   y = x
   print *, y

end program myProgram










program myProgram
implicit none

   print *, 2 + 2

end program myProgram



















