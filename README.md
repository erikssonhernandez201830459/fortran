# Fortran Analyzer
## Laboratorio de Organizacion de Lenguajes y Compiladores 2
Auxiliar: Mariana Sic
## Ingenieria CUNOC - USAC
 Junio 2022

## Descripción del Proyecto
El Proyecto No. 1 consiste en la creación de un Interprete y Traductor del Lenguaje de Programacion *Fortran*, 
utilizando la herramienta de ANTLR e implementando el Patron Visitor y Patron Interprete combinados. 
El proyecto se desarrollo en el lenguaje de Java 
### Interprete:
La función del Interprete será realizar: 
- Análisis Léxico
- Análisis Sintáctico
- Análisis Semántico
- Ejecución de las Instrucciones
- Reporte de Errores 
- Reporte de Tabla de Simbolos
- Reporte de AST (Abstract Syntax Tree)
- Reporte de CST (Concret Syntax Tree)

### Traductor
#### Generacion de Codigo Intermedio
La generación de código intermedio corresponde a una de las fases del compilador, por lo que para este proyecto se solicita que el estudiante genere código en tres direcciones que sea equivalente al código de alto nivel.
El código de tres direcciones será en C, debe ser ejecutado en la misma aplicación mostrando su resultado en una caja de texto el resultado de consola.

La función del Traductor será realizar:
- Traducción del Codigo en 3 Direcciones

## Herramientas Utilizadas
- Java _Version 11.0.15_
- ANTLR4 _Version 4.10.1_